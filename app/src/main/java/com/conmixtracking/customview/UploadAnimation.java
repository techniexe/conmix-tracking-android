package com.conmixtracking.customview;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatImageView;

import com.conmixtracking.R;

/**
 * Created by Hitesh Patel on 10,May,2021
 */
public class UploadAnimation extends AppCompatImageView {
    AnimationDrawable frameAnimation;

    public UploadAnimation(Context context) {
        super(context);
        initView(context);
    }

    public UploadAnimation(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public UploadAnimation(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        setImageResource(R.drawable.uploading_animation);
        frameAnimation = (AnimationDrawable) getDrawable();
    }

    public void start() {
        frameAnimation.start();
    }

    public void stop() {
        frameAnimation.stop();
    }
}