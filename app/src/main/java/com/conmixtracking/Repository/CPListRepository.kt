package com.conmixtracking.Repository

import com.conmixtracking.services.api.ApiInterface


/**
 * Created by Hitesh Patel on 27,July,2021
 */
class CPListRepository(private val apiInterface: ApiInterface) {
    suspend fun getCPListData() = apiInterface.getCPListData()
}