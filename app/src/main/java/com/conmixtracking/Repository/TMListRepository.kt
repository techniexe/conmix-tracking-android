package com.conmixtracking.Repository

import com.conmixtracking.services.api.ApiInterface



/**
 * Created by Hitesh Patel on 06,May,2021
 */
class TMListRepository(private val apiInterface: ApiInterface) {

    suspend fun getTMListData(is_active:Boolean?,
                              TM_category_id:String?,
                              TM_sub_category_id:String?,
                              is_gps_enabled:Boolean?,
                              is_insurance_active:Boolean?,
                              before: String?,
                              after: String?,
                              limit_count:Int?) = apiInterface.getTMListData(is_active,TM_category_id,TM_sub_category_id,is_gps_enabled,is_insurance_active,before,after,limit_count)


suspend fun getSpecificTMData(id: String?) = apiInterface.getSpecificTMData(id)

}