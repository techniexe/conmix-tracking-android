package com.conmixtracking.Repository

import com.conmixtracking.data.DelayReasonObj
import com.conmixtracking.data.UpdateQtyOBJ
import com.conmixtracking.data.UploadTmInvoiceObj
import com.conmixtracking.services.api.ApiInterface
import okhttp3.MultipartBody
import retrofit2.http.Part
import retrofit2.http.Path


/**
 * Created by Hitesh Patel on 06,May,2021
 */
class OrderRepository(private val apiInterface: ApiInterface) {

    suspend fun getOrderDetailListData(id: String?,current_date:String?) = apiInterface.getOrderDetailListData(id,current_date)

    suspend fun getOrderDetailCPListData(id: String?,current_date:String?) = apiInterface.getOrderDetailCPListData(id,current_date)

    suspend fun getOrdeIdDetailData(order_id:String?,track_id:String?) = apiInterface.getOrdeIdDetailData(order_id,track_id)

    suspend fun getOrdeIdCPDetailData(order_id:String?,track_id:String?) = apiInterface.getOrdeIdCPDetailData(order_id,track_id)

    suspend fun updateQtyData(track_id:String?,tmId:String?,order_id:String?,uQb: UpdateQtyOBJ?) = apiInterface.updateQtyData(track_id,tmId,order_id,uQb)

    suspend fun updateQtyStatusCPData(track_id:String?,cpId:String?,order_id:String?,uQb: UpdateQtyOBJ?) = apiInterface.updateQtyStatusCPData(track_id,cpId,order_id,uQb)

    suspend fun confirmPickupOrder(track_id:String?,tmId:String?,order_id:String?) = apiInterface.confirmPickupOrder(track_id,tmId,order_id)

    suspend fun confirmPickupCPOrder(track_id:String?,cpId:String?,order_id:String?) = apiInterface.confirmPickupCPOrder(track_id,cpId,order_id)

    suspend fun uploadDocumentOrder(track_id:String?, tmId:String?, order_id:String?,royalty_pass_image: MultipartBody.Part?,
                                     way_slip_image: MultipartBody.Part?,
                                     challan_image: MultipartBody.Part?,
                                     invoice_image: MultipartBody.Part?) = apiInterface.uploadDocumentOrder(track_id, tmId, order_id,royalty_pass_image,way_slip_image,challan_image,invoice_image)


    suspend fun uploadDocumentCPOrder(track_id:String?, cpId:String?, order_id:String?,royalty_pass_image: MultipartBody.Part?,
                                    way_slip_image: MultipartBody.Part?,
                                    challan_image: MultipartBody.Part?,
                                    invoice_image: MultipartBody.Part?) = apiInterface.uploadDocumentCPOrder(track_id, cpId, order_id,royalty_pass_image,way_slip_image,challan_image,invoice_image)



    suspend fun uploadSignSupplierOrder(track_id:String?, tmId:String?, order_id:String?,supplier_pickup_signature: MultipartBody.Part?,
                                        driver_pickup_signature: MultipartBody.Part?,
                                        buyer_drop_signature: MultipartBody.Part?,
                                        driver_drop_signature: MultipartBody.Part?) = apiInterface.uploadSignSupplierOrder(track_id, tmId, order_id,supplier_pickup_signature,driver_pickup_signature,buyer_drop_signature,driver_drop_signature)

    suspend fun uploadSignSupplierCPOrder(track_id:String?, cpId:String?, order_id:String?,supplier_pickup_signature: MultipartBody.Part?,
                                        driver_pickup_signature: MultipartBody.Part?,
                                        buyer_drop_signature: MultipartBody.Part?,
                                        driver_drop_signature: MultipartBody.Part?) = apiInterface.uploadSignSupplierCPOrder(track_id, cpId, order_id,supplier_pickup_signature,driver_pickup_signature,buyer_drop_signature,driver_drop_signature)

    suspend fun postDelayReasonOrder(tmId:String?,orderId:String?,track_id:String?,dROBJ: DelayReasonObj?) = apiInterface.postDelayReasonOrder(tmId,orderId,track_id,dROBJ)

    suspend fun postDelayReasonCPOrder(cpId:String?,orderId:String?,track_id:String?,dROBJ: DelayReasonObj?) = apiInterface.postDelayReasonCPOrder(cpId,orderId,track_id,dROBJ)

    suspend fun deliverSuceessCall( tmId: String?,
                                   orderId: String?, track_id: String?) = apiInterface.deliverSuceessCall(tmId,orderId,track_id)


    suspend fun deliverSuceessCPCall( cpId: String?,
                                    orderId: String?, track_id: String?) = apiInterface.deliverSuceessCPCall(cpId,orderId,track_id)

    suspend fun getCPDeliverCheck(orderId: String?) = apiInterface.getCPDeliverCheck(orderId)

    suspend fun uploadTmInvoice(uploadTmInvoiceObj: UploadTmInvoiceObj?) = apiInterface.uploadTmInvoice(uploadTmInvoiceObj)

    suspend fun uploadVendorInvoice(uploadTmInvoiceObj: UploadTmInvoiceObj?) = apiInterface.uploadVendorInvoice(uploadTmInvoiceObj)
}