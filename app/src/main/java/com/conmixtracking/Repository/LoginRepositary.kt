package com.conmixtracking.Repository

import com.conmixtracking.data.LoginReqModel
import com.conmixtracking.services.api.ApiInterface


/**
 * Created by Hitesh Patel on 06,May,2021
 */
class LoginRepositary (private val apiInterface: ApiInterface) {

    suspend fun loginWithIdentifier(userName:String,loginReqModel: LoginReqModel) = apiInterface.loginWithIdentifier(userName,loginReqModel = loginReqModel)

    suspend fun getOtpCode(mobilenumber: String?) = apiInterface.getOtpCode(mobilenumber)

    suspend fun postMobileCode(mobile_number: String?,code: String?) = apiInterface.postMobileCode(mobile_number,code)
}