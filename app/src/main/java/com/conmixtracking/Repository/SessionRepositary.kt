package com.conmixtracking.Repository

import com.conmixtracking.data.SessionRequest
import com.conmixtracking.services.api.ApiInterface


/**
 * Created by Hitesh Patel on 06,May,2021
 */
class SessionRepositary(val apiInterface: ApiInterface){
    suspend fun getSessionToken(sessionRequest: SessionRequest) = apiInterface.getSessionToken(sessionRequest)
}