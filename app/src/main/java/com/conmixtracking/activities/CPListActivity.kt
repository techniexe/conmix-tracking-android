package com.conmixtracking.activities

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.PopupMenu
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.conmixtracking.ConmixTrackingApp
import com.conmixtracking.R
import com.conmixtracking.ViewModel.CPListViewModel
import com.conmixtracking.ViewModel.TMListViewModel
import com.conmixtracking.adapter.CPListAdapter
import com.conmixtracking.adapter.VehicleListAdapter
import com.conmixtracking.data.CPListObjRes
import com.conmixtracking.data.TMListObjRes
import com.conmixtracking.databinding.VehicleListBinding
import com.conmixtracking.dialog.SelectLanguageDialog
import com.conmixtracking.services.api.ApiClient
import com.conmixtracking.services.api.Status
import com.conmixtracking.utils.SharedPrefUtils
import com.conmixtracking.utils.Utils
import spencerstudios.com.bungeelib.Bungee
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by Hitesh Patel on 27,July,2021
 */
class CPListActivity : AppCompatActivity(), CPListAdapter.cplistInterface{


    private lateinit var binding: VehicleListBinding
    var cpList:ArrayList<CPListObjRes>? = ArrayList<CPListObjRes>()
    var mAdapter: CPListAdapter? = null
    private lateinit var cpListViewModel: CPListViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = VehicleListBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.userFollowerCountLabel.text = getString(R.string.cplisttitletxt)
        setupViewModel()
        binding.loaderView.visibility = View.VISIBLE
        binding.loaderView.show()
        binding.navBackFollowersList.setOnClickListener {

            finish()
            Bungee.slideRight(this)
        }

        if (!Utils.checkConnectivity(this)) {
            Utils.showToast(this, getString(R.string.no_internet_msg), Toast.LENGTH_SHORT)
        } else {
            getVehicleListData()
        }

    }

    override fun onBackPressed() {
        binding.navBackFollowersList?.performClick()
    }
    private fun setupViewModel() {

        cpListViewModel = ViewModelProvider(
            this,
            CPListViewModel.ViewModelFactory(ApiClient().apiService)
        )
            .get(CPListViewModel::class.java)


    }

    fun getVehicleListData() {
        ApiClient.setToken()
        cpListViewModel.getCPListData()
            .observe(this, androidx.lifecycle.Observer {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            if (resource.data?.isSuccessful!!) {
                                resource.data.let { response ->
                                    setResposerData(response.body()?.data)
                                }
                            } else {
                                emptyView()
                                Utils.setErrorData(this, resource.data.errorBody())
                            }

                        }
                        Status.ERROR -> {
                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                        }
                        Status.LOADING -> {

                        }

                    }
                }
            })

    }

    private fun setResposerData(response: ArrayList<CPListObjRes>?) {

        try {
            if (mAdapter == null) {


                cpList?.clear()
                if (response != null) {
                    cpList?.addAll(response)
                    mAdapter = cpList?.let { CPListAdapter(this@CPListActivity, it) }
                    val mLayoutManager = LinearLayoutManager(this@CPListActivity)
                    binding.vehicleListRecycler.setLayoutManager(mLayoutManager)
                    mAdapter?.setListener(this@CPListActivity)
                    binding.vehicleListRecycler.setAdapter(mAdapter)
                }

                // myOrderRV?.setAdapter(adapter)
                // recyclerView?.smoothSnapToPosition(0)
                // mAdapter?.loadDone()
                // progressBar?.visibility = View.GONE


            } else if (cpList != null) {

                cpList?.clear()
                response?.forEach {
                    if (cpList!!.contains(it)) {
                        val index = cpList!!.indexOf(it)
                        cpList!!.removeAt(index)
                        cpList!!.add(it)
                    } else {
                        cpList!!.add(it)
                    }

                }
                try {
                    mAdapter?.notifyDataSetChanged()
                } catch (e: Exception) {

                }
                binding.progressBar.visibility = View.GONE
                //mAdapter?.loadDone()
                //(spottfeedListRv.getItemAnimator() as SimpleItemAnimator).supportsChangeAnimations = false
            }


            binding.loaderView.visibility = View.GONE
            binding.loaderView.hide()
            binding.progressBar.visibility = View.GONE
            emptyView()

        } catch (e: Exception) {
            binding.loaderView.visibility = View.GONE
            binding.loaderView.hide()
            emptyView()
            binding.progressBar.visibility = View.GONE
        }


        emptyView()

    }

    override fun onCPListRowClick(cpo: CPListObjRes) {
        val intent = Intent(this, CPDetailActivity::class.java)
        intent.putExtra("id", cpo?._id)
        intent.putExtra("cpObj", cpo)
        startActivity(intent)
        Bungee.slideLeft(this)
    }





    fun emptyView() {

        if (cpList?.isEmpty() == true) {
            binding.emptyRecylerview.visibility = View.VISIBLE
            binding.emptyTextFollowFragment.text = getString(R.string.no_data_empty_screen)
            binding.vehicleListRecycler.visibility = View.GONE

        } else {
            binding.emptyRecylerview.visibility = View.GONE
            binding.vehicleListRecycler.visibility = View.VISIBLE
        }
    }

}