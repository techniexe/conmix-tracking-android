package com.conmixtracking.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.PopupMenu
import androidx.lifecycle.ViewModelProvider
import com.conmixtracking.ConmixTrackingApp
import com.conmixtracking.R
import com.conmixtracking.ViewModel.LoginViewModel
import com.conmixtracking.data.OrderDetailObj
import com.conmixtracking.databinding.AcceptOrderInfoBinding
import com.conmixtracking.dialog.BreakAlertDialog
import com.conmixtracking.services.api.ApiClient
import com.conmixtracking.services.api.Status
import com.conmixtracking.utils.Utils
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 10,May,2021
 */
class OrderInfoAcceptActivity:AppCompatActivity(),BreakAlertDialog.ClickListener {
    private lateinit var binding:AcceptOrderInfoBinding
    private lateinit var loginViewModel: LoginViewModel
    var  ODRes: OrderDetailObj?= null
    var cancelOrd :Boolean = false
    private var vehicleId:String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = AcceptOrderInfoBinding.inflate(layoutInflater)

        setContentView(binding.root)
        setupViewModel()


        ODRes = intent.getParcelableExtra("ODRes")
      //  vehicleId = SharedPrefrence.getTMID(this)

        vehicleId = ConmixTrackingApp.instance?.globalVehicle


      //  binding.orderInfoComNameTxt.text = ODRes?.vendor_info?.full_name

        binding.orderIdTxt.text = "#"+ODRes?.vendor_order_info?._id
        binding.orderQtyTxt.text = (ODRes?.pickup_quantity?:0.0).toInt().toString()+" Cu.Mtr"
        binding.pickupQtyTxt.text = (ODRes?.pickup_quantity?:0.0).toInt().toString()+" Cu.Mtr"
        binding.royaltyPassTxt.text = (ODRes?.royality_quantity?:0.0).toInt().toString()+" Cu.Mtr"

        // val diffrenceQty = ODRes?.quantity?.minus(ODRes?.pickup_quantity?:0.0)

        binding.remainingQtyTxt.text = (ODRes?.remaining_quantity?:0.0).toInt().toString()+" Cu.Mtr"
        binding.supplierNameTxt.text = ODRes?.vendor_info?.company_name?:" "
        binding.buyerNameTxt.text = ODRes?.buyer_info?.company_name?:" "

        binding.navBackFollowersList.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }

        binding.submitBtn.setOnClickListener {
            cancelOrd = false
            val fragment: BreakAlertDialog = BreakAlertDialog(this, getString(R.string.accept_order_alert_txt), getString(R.string.yes_btn_txt), getString(R.string.no_btn_txt))
            fragment.show(supportFragmentManager, "alert")
            fragment.isCancelable = false
            fragment.setDialogListener(this)

        }

        binding.cancelMenu.setOnClickListener {
            cancelOrd = true
            showPopupMenuCancel(binding.cancelMenu)
        }

    }

    private fun setupViewModel() {

        loginViewModel = ViewModelProvider(
            this,
            LoginViewModel.ViewModelFactory(ApiClient().apiService)
        )
            .get(LoginViewModel::class.java)


    }

    override fun onBackPressed() {

        binding.navBackFollowersList.performClick()
    }

    override fun onDoneClicked() {

        if(cancelOrd){

            // UpdateQty("REJECTED")

            val intent = Intent(this@OrderInfoAcceptActivity, RejectReasonActivity::class.java)
            intent.putExtra("fromEvnt","REJECTED")
            intent.putExtra("orderDtl",ODRes)
            intent.putExtra("cancelOrd",cancelOrd)
            intent.putExtra("vehicleId",vehicleId)
            intent.putExtra("isFromCP",false)
            startActivity(intent)
            Bungee.slideLeft(this@OrderInfoAcceptActivity)

        }else {


            getOtpCode(ODRes?.buyer_info?.mobile_number)


            //UpdateQty("ACCEPTED")
        }
    }

    override fun onCancelClicked() {

    }
    fun showPopupMenuCancel(v: View) {
        val popup = PopupMenu(this@OrderInfoAcceptActivity, v)
        val inflater = popup.menuInflater

        inflater.inflate(R.menu.cancel_order_menu, popup.menu)
        popup.show()
        popup.setOnMenuItemClickListener { item ->

            when (item.itemId) {
                R.id.cnacel_order_Menu -> {

                    val fragment: BreakAlertDialog = BreakAlertDialog(this, getString(R.string.cancel_order_alert_txt), getString(R.string.yes_btn_txt), getString(R.string.no_btn_txt))
                    fragment.show(supportFragmentManager, "alert")
                    fragment.isCancelable = false
                    fragment.setDialogListener(this)


                    true


                }
                else -> false
            }

        }
    }


    fun getOtpCode(mobileNumber:String?){

        if(mobileNumber != null) {


            ApiClient.setToken()
            loginViewModel.getOtpCode(mobileNumber)
                .observe(this, androidx.lifecycle.Observer {
                    it?.let { resource ->
                        when (resource.status) {
                            Status.SUCCESS -> {

                                if (resource.data?.isSuccessful == true) {
                                    resource.data.let { response ->

                                        if(response.code() ==200){

                                            val intent = Intent(this@OrderInfoAcceptActivity, OTPActivity::class.java)
                                            intent.putExtra("fromEvnt","ACCEPTED")
                                            intent.putExtra("orderDtl",ODRes)
                                            intent.putExtra("cancelOrd",cancelOrd)
                                            intent.putExtra("vehicleId",vehicleId)
                                            startActivity(intent)
                                            Bungee.slideLeft(this@OrderInfoAcceptActivity)



                                        } else if (response.code() == 422) {

                                            Utils.showToast(
                                                this@OrderInfoAcceptActivity,
                                                response?.message(),
                                                Toast.LENGTH_SHORT
                                            )

                                        }

                                    }
                                } else {


                                    Utils.setErrorData(this, resource.data?.errorBody())
                                }

                            }
                            Status.ERROR -> {

                                Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)

                            }
                            Status.LOADING -> {

                            }



                        }
                    }
                })

        }else{
            Utils.showToast(this,getString(R.string.error_mobile_number), Toast.LENGTH_SHORT)
        }

    }

}