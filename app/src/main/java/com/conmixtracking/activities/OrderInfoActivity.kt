package com.conmixtracking.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.conmixtracking.ConmixTrackingApp
import com.conmixtracking.ViewModel.LoginViewModel
import com.conmixtracking.data.OrderDetailObj
import com.conmixtracking.databinding.OrderInfoBinding
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 10,May,2021
 */
class OrderInfoActivity:AppCompatActivity() {
    private lateinit var binding: OrderInfoBinding
    private lateinit var loginViewModel: LoginViewModel
    var  ODRes: OrderDetailObj?= null
    var vehicleID:String?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = OrderInfoBinding.inflate(layoutInflater)

        setContentView(binding.root)

        ODRes = intent.getParcelableExtra("orderDtl")
       // vehicleID = intent?.getStringExtra("vehicle_Id")?:""

        vehicleID = ConmixTrackingApp.instance?.globalVehicle


        binding.orderInfoComNameTxt.text = ODRes?.vendor_info?.full_name

        binding.orderIdTxt.text = "#"+ODRes?.vendor_order_info?._id
        binding.orderQtyTxt.text = ODRes?.pickup_quantity?.toInt().toString()+" Cu.Mtr"
        binding.pickupQtyTxt.text = ODRes?.pickup_quantity?.toInt().toString()+" Cu.Mtr"
        binding.royaltyPassTxt.text = ODRes?.royality_quantity?.toInt().toString()+" Cu.Mtr"

        //val diffrenceQty = ODRes?.quantity?.minus(ODRes?.pickup_quantity?:0.0)

        binding.remainingQtyTxt.text = ODRes?.remaining_quantity?.toInt().toString()+" Cu.Mtr"

        binding.navBackOrderInfo.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }

        binding.proceedBtn?.setOnClickListener {

            val intent = Intent(this@OrderInfoActivity, UploadDocumentActivity::class.java)
            intent.putExtra("order_id",ODRes?.vendor_order_info?._id)
            intent.putExtra("vehicle_Id",vehicleID)
            intent.putExtra("ODRes",ODRes)
            startActivity(intent)
            Bungee.slideLeft(this@OrderInfoActivity)
        }

    }

    override fun onBackPressed() {

        binding.navBackOrderInfo.performClick()
    }

}