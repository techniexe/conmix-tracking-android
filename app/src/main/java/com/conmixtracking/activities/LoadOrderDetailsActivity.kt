package com.conmixtracking.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.PopupMenu
import androidx.lifecycle.ViewModelProvider
import com.conmixtracking.ConmixTrackingApp
import com.conmixtracking.R
import com.conmixtracking.ViewModel.LoginViewModel
import com.conmixtracking.ViewModel.OrderViewModel
import com.conmixtracking.data.OrderDetailObj
import com.conmixtracking.databinding.EmptyLayoutBinding
import com.conmixtracking.databinding.GoForOrderDetailBinding
import com.conmixtracking.dialog.BreakAlertDialog
import com.conmixtracking.dialog.SelectLanguageDialog
import com.conmixtracking.services.api.ApiClient
import com.conmixtracking.services.api.Status
import com.conmixtracking.utils.SharedPrefUtils
import com.conmixtracking.utils.Utils
import com.google.gson.internal.bind.util.ISO8601Utils
import spencerstudios.com.bungeelib.Bungee
import java.text.ParsePosition
import java.text.SimpleDateFormat


/**
 * Created by Hitesh Patel on 10,May,2021
 */
class LoadOrderDetailsActivity:AppCompatActivity(),BreakAlertDialog.ClickListener  {
    private lateinit var binding: GoForOrderDetailBinding
    private lateinit var bindingNoOrder: EmptyLayoutBinding
    private lateinit var orderViewModel: OrderViewModel
    private lateinit var loginViewModel: LoginViewModel
    var ODRes: OrderDetailObj?= null
    var pickUpAddrees:String? = null
    var deliveryAddress:String? = null
    var order_Id:String?= null
    private var vehicleId:String? = null
    var isFromCP:Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = GoForOrderDetailBinding.inflate(layoutInflater)
        bindingNoOrder = binding.emptyOrder
        setContentView(binding.root)

        setupViewModel()
        order_Id = SharedPrefUtils.getInstance(this).getStringFromSharedPref(SharedPrefUtils.ORDER_ID)
       // vehicleId = SharedPrefrence.getTMID(this)

        vehicleId = ConmixTrackingApp.instance?.globalVehicle
        isFromCP = intent.getBooleanExtra("isFromCP",false)

        ODRes = intent.getParcelableExtra("ODRes")


        if(isFromCP){
            getOrderDetailCPData(ODRes?.vendor_order_info?._id)
        }else{
            getOrderDetailData(ODRes?.vendor_order_info?._id)
        }


        binding.navBackOrderDeatilBtn.setOnClickListener {

            val intent = Intent(this@LoadOrderDetailsActivity, OrderListActivity::class.java)
            intent.putExtra("vehicleID",vehicleId)
            intent.putExtra("isFromCP",isFromCP)
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
            Bungee.slideRight(this@LoadOrderDetailsActivity)
            finish()


        }

        binding.pickUpLnly.setOnClickListener {

            val intent = Intent(this, MapMarkerActivity::class.java)
            intent.putExtra("latitude", ODRes?.pickup_address_info?.pickup_location?.coordinates?.get(1) ?:0.0)
            intent.putExtra("longitude",ODRes?.pickup_address_info?.pickup_location?.coordinates?.get(0) ?:0.0)
            intent.putExtra("address",pickUpAddrees)
            startActivity(intent)
            Bungee.slideLeft(this)

        }

        binding.deliverLnly.setOnClickListener {

            val intent = Intent(this, MapMarkerActivity::class.java)
            intent.putExtra("latitude", ODRes?.delivery_address_info?.delivery_location?.coordinates?.get(1) ?:0.0)
            intent.putExtra("longitude",ODRes?.delivery_address_info?.delivery_location?.coordinates?.get(0) ?:0.0)
            intent.putExtra("address",deliveryAddress)
            startActivity(intent)
            Bungee.slideLeft(this)
        }

        binding.mapnavicn.setOnClickListener {
            val gmmIntentUri: Uri = Uri.parse("google.navigation:q=${ODRes?.delivery_address_info!!.delivery_location!!.coordinates!![1]},${ODRes?.delivery_address_info!!.delivery_location!!.coordinates!![0]}")
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            startActivity(mapIntent)
            Bungee.slideLeft(this)
        }

        binding.pickUpBtn.setOnClickListener {
            // Utils().showToast(this,"PickUp Button Clicked",Toast.LENGTH_SHORT)
        }

        binding.menuIcon.setOnClickListener {

            showPopupMenu(binding.menuIcon)

        }

        binding.orderDeliverBtn.setOnClickListener {

            if(isFromCP){
                getOtpCode(ODRes?.buyer_info?.mobile_number)
            }else{
                Intent(this, OrderInfoAcceptActivity::class.java).apply {
                    this.putExtra("ODRes",ODRes)
                    startActivity(this)
                    Bungee.slideLeft(this@LoadOrderDetailsActivity)
                }
            }

        }

    }

    fun showPopupMenu(v: View) {
        val popup = PopupMenu(this@LoadOrderDetailsActivity, v)
        val inflater = popup.menuInflater

        inflater.inflate(R.menu.delay_menu, popup.menu)

        val cancelorderMn: MenuItem = popup.menu.findItem(R.id.rejectMenu)

        if (isFromCP){
            cancelorderMn.isVisible = true
        }else{
            cancelorderMn.isVisible = false
        }

        popup.show()
        popup.setOnMenuItemClickListener { item ->

            when (item.itemId) {



                R.id.deleyMenu -> {
                    Intent(this, DelayReasonActivity::class.java).apply {
                        this.putExtra("ODRes",ODRes)
                        this.putExtra("isFromCP",isFromCP)
                        startActivity(this)
                    }


                    true
                }
                R.id.rejectMenu -> {
                    val fragment: BreakAlertDialog = BreakAlertDialog(this@LoadOrderDetailsActivity, getString(R.string.cancel_order_alert_txt), getString(R.string.yes_btn_txt), getString(R.string.no_btn_txt))
                    fragment.show(supportFragmentManager, "alert")
                    fragment.isCancelable = false
                    fragment.setDialogListener(this@LoadOrderDetailsActivity)


                    true
                }

                else -> false
            }

        }
    }

    private fun setupViewModel() {

        orderViewModel = ViewModelProvider(
            this,
            OrderViewModel.ViewModelFactory(ApiClient().apiService)
        )
            .get(OrderViewModel::class.java)
        loginViewModel = ViewModelProvider(
            this,
            LoginViewModel.ViewModelFactory(ApiClient().apiService)
        )
            .get(LoginViewModel::class.java)


    }

    private fun getOrderDetailData(id:String?){
        showLoader()
        ApiClient.setToken()
        orderViewModel.getOrdeIdDetailData(ODRes?.vendor_order_info?._id,ODRes?._id)
            .observe(this, androidx.lifecycle.Observer {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            hideLoader()
                            if (resource.data?.isSuccessful!!) {
                                resource.data.let { response ->
                                    ODRes = response?.body()?.order
                                    hideLoader()

                                    if(ODRes != null){
                                        binding.orderDetailLnly?.visibility = View.VISIBLE
                                        bindingNoOrder.emptyFrame?.visibility = View.GONE


                                        SharedPrefUtils.getInstance(this).storeStringInSharedPref(SharedPrefUtils.ORDER_ID,ODRes?.vendor_order_info?._id)
                                        upDateUi(ODRes)
                                    }else{
                                        binding.orderDetailLnly?.visibility = View.GONE
                                        bindingNoOrder.emptyFrame?.visibility = View.VISIBLE
                                        if(isFromCP){
                                            bindingNoOrder.emptyText.text = getString(R.string.no_data_cp_empty_screen)
                                        }else{
                                            bindingNoOrder.emptyText.text = getString(R.string.no_data_empty_screen)
                                        }
                                    }
                                }
                            } else {

                                binding.orderDetailLnly?.visibility = View.GONE
                                bindingNoOrder.emptyFrame?.visibility = View.VISIBLE
                                if(isFromCP){
                                    bindingNoOrder.emptyText.text = getString(R.string.no_data_cp_empty_screen)
                                }else{
                                    bindingNoOrder.emptyText.text = getString(R.string.no_data_empty_screen)
                                }
                                Utils.setErrorData(this, resource.data.errorBody())
                            }

                        }
                        Status.ERROR -> {
                            hideLoader()
                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                            binding.orderDetailLnly?.visibility = View.GONE
                            bindingNoOrder.emptyFrame?.visibility = View.VISIBLE
                            if(isFromCP){
                                bindingNoOrder.emptyText.text = getString(R.string.no_data_cp_empty_screen)
                            }else{
                                bindingNoOrder.emptyText.text = getString(R.string.no_data_empty_screen)
                            }
                        }
                        Status.LOADING -> {

                        }
                    }
                }
            })
    }

    private fun getOrderDetailCPData(id:String?){
        showLoader()
        ApiClient.setToken()
        orderViewModel.getOrdeIdCPDetailData(ODRes?.vendor_order_info?._id,ODRes?._id)
            .observe(this, androidx.lifecycle.Observer {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            hideLoader()
                            if (resource.data?.isSuccessful!!) {
                                resource.data.let { response ->
                                    ODRes = response?.body()?.order
                                    hideLoader()

                                    if(ODRes != null){
                                        binding.orderDetailLnly?.visibility = View.VISIBLE
                                        bindingNoOrder.emptyFrame?.visibility = View.GONE


                                        SharedPrefUtils.getInstance(this).storeStringInSharedPref(SharedPrefUtils.ORDER_ID,ODRes?.vendor_order_info?._id)
                                        upDateUi(ODRes)
                                    }else{
                                        binding.orderDetailLnly?.visibility = View.GONE
                                        bindingNoOrder.emptyFrame?.visibility = View.VISIBLE
                                        if(isFromCP){
                                            bindingNoOrder.emptyText.text = getString(R.string.no_data_cp_empty_screen)
                                        }else{
                                            bindingNoOrder.emptyText.text = getString(R.string.no_data_empty_screen)
                                        }
                                    }
                                }
                            } else {

                                binding.orderDetailLnly?.visibility = View.GONE
                                bindingNoOrder.emptyFrame?.visibility = View.VISIBLE
                                if(isFromCP){
                                    bindingNoOrder.emptyText.text = getString(R.string.no_data_cp_empty_screen)
                                }else{
                                    bindingNoOrder.emptyText.text = getString(R.string.no_data_empty_screen)
                                }
                                Utils.setErrorData(this, resource.data.errorBody())
                            }

                        }
                        Status.ERROR -> {
                            hideLoader()
                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                            binding.orderDetailLnly?.visibility = View.GONE
                            bindingNoOrder.emptyFrame?.visibility = View.VISIBLE
                            if(isFromCP){
                                bindingNoOrder.emptyText.text = getString(R.string.no_data_cp_empty_screen)
                            }else{
                                bindingNoOrder.emptyText.text = getString(R.string.no_data_empty_screen)
                            }
                        }
                        Status.LOADING -> {

                        }



                    }
                }
            })
    }

    fun upDateUi(ODRes:OrderDetailObj?){

        if(ODRes != null){
            binding.menuIcon.visibility = View.VISIBLE
            binding.mapnavicn.visibility = View.VISIBLE
            binding.orderIDLabel.text = "ORDER ID #"+ODRes.vendor_order_info?._id
            val string1 = ODRes.created_at
            val Date1 = ISO8601Utils.parse(string1, ParsePosition(0))
            val posFormat = SimpleDateFormat("dd MMM yyyy")
            val oldPosDate = posFormat.format(Date1)

            binding.dateTxt.text = oldPosDate


            if(isFromCP){
                Utils.setImageUsingGlide(this@LoadOrderDetailsActivity, ODRes.CP_info?.concrete_pump_image_url?:"", binding.vehicleImg)
                binding.truckCategoryNameTxt.text = ODRes.CP_category_info?.category_name
                binding.truckRangeTxt.text = ODRes.CP_info?.company_name +" - "+ODRes.CP_info?.concrete_pump_model
                binding.vehicleNumberTxt?.visibility = View.GONE
                binding.qtyTitleTxt.text = getString(R.string.cp_subcategory_txt)
                binding.orderQtyTxt.text = (ODRes.CP_info?.concrete_pump_capacity?:0).toString() + " Cu.Mtr"

            }else{
                Utils.setImageUsingGlide(this@LoadOrderDetailsActivity, ODRes.TM_info?.TM_image_url?:"", binding.vehicleImg)
                binding.truckCategoryNameTxt.text = getString(R.string.vehicle_list_fix_title)
                binding.truckRangeTxt.text = ODRes.TM_category_info?.category_name
                binding.vehicleNumberTxt?.visibility = View.VISIBLE
                binding.vehicleNumberTxt.text = ODRes.TM_info?.TM_rc_number
                binding.qtyTitleTxt.text = getString(R.string.pickup_qty_txt)
                binding.orderQtyTxt.text = ODRes.pickup_quantity?.toInt().toString()+" Cu.Mtr"
            }



            if(ODRes.vendor_info?.company_name != null){
                binding.supplierCompLnly.visibility = View.VISIBLE
                binding.orderSupplierCompanyNameTxt.text = ODRes.vendor_info?.company_name

            }else{
                binding.supplierCompLnly.visibility = View.GONE
            }

            binding.orderSupplierNameTxt.text = ODRes.vendor_info?.full_name
            binding.orderSuppliercontactTxt.text = ODRes.vendor_info?.mobile_number

            binding.orderSuppliercontactTxt?.setOnClickListener{
                val callIntent = Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + Uri.encode(ODRes.vendor_info?.mobile_number?:"")))
                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(callIntent)
            }


            if(ODRes.buyer_info?.company_name != null){
                binding.buyerCompanyLnly.visibility = View.VISIBLE
                binding.buyerCompanynameTxt.text  = ODRes.buyer_info?.company_name

            }else{
                binding.buyerCompanyLnly.visibility = View.GONE
            }

            binding.buyerNameTxt.text = ODRes.buyer_info?.full_name
            binding.buyerContactTxt.text = ODRes.buyer_info?.mobile_number

            binding.buyerContactTxt?.setOnClickListener{
                val callIntent = Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + Uri.encode(ODRes.buyer_info?.mobile_number?:"")))
                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(callIntent)
            }

            pickUpAddrees = Utils.formatedAddresss(ODRes.pickup_address_info?.line1,
                ODRes.pickup_address_info?.line2,ODRes.pickup_address_info?.city_name,ODRes.pickup_address_info?.state_name,
                ODRes.pickup_address_info?.pincode)
            binding.pickupAddresstxt?.text = pickUpAddrees

            deliveryAddress = Utils.formatedAddresss(ODRes.delivery_address_info?.line1,
                ODRes.delivery_address_info?.line2,ODRes.delivery_address_info?.city_name,ODRes.delivery_address_info?.state_name,
                ODRes.delivery_address_info?.pincode)

            binding.deliverAddresstxt?.text = deliveryAddress

        }

    }

    override fun onBackPressed() {

        binding.navBackOrderDeatilBtn?.performClick()
    }

    private fun showLoader() {
        binding.ivLoadingEdit.visibility = View.VISIBLE
        binding.ivLoadingEdit.smoothToShow()
    }

    private fun hideLoader() {
        binding.ivLoadingEdit.visibility = View.GONE
        binding.ivLoadingEdit.smoothToHide()
    }

    fun getOtpCode(mobileNumber:String?){

        if(mobileNumber != null) {
            ApiClient.setToken()
            loginViewModel.getOtpCode(mobileNumber)
                .observe(this, androidx.lifecycle.Observer {
                    it?.let { resource ->
                        when (resource.status) {
                            Status.SUCCESS -> {

                                if (resource.data?.isSuccessful == true) {
                                    resource.data.let { response ->

                                        if(response.code() ==200){

                                            val intent = Intent(this@LoadOrderDetailsActivity, OTPActivity::class.java)
                                            intent.putExtra("fromEvnt","ACCEPTED")
                                            intent.putExtra("orderDtl",ODRes)
                                            intent.putExtra("vehicleId",vehicleId)
                                            intent.putExtra("isFromCP",isFromCP)
                                            startActivity(intent)
                                            Bungee.slideLeft(this@LoadOrderDetailsActivity)



                                        } else if (response.code() == 422) {

                                            Utils.showToast(
                                                this@LoadOrderDetailsActivity,
                                                response?.message(),
                                                Toast.LENGTH_SHORT
                                            )

                                        }

                                    }
                                } else {


                                    Utils.setErrorData(this, resource.data?.errorBody())
                                }

                            }
                            Status.ERROR -> {

                                Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)

                            }
                            Status.LOADING -> {

                            }

                        }
                    }
                })

        }else{
            Utils.showToast(this,getString(R.string.error_mobile_number), Toast.LENGTH_SHORT)
        }

    }

    override fun onDoneClicked() {
        val intent = Intent(this@LoadOrderDetailsActivity, RejectReasonActivity::class.java)
        intent.putExtra("fromEvnt","REJECTED")
        intent.putExtra("orderDtl",ODRes)
        intent.putExtra("cancelOrd",true)
        intent.putExtra("vehicleId",vehicleId)
        intent.putExtra("isFromCP",true)
        startActivity(intent)
        Bungee.slideLeft(this@LoadOrderDetailsActivity)
    }

    override fun onCancelClicked() {

    }
}