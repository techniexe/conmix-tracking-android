package com.conmixtracking.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.conmixtracking.ConmixTrackingApp
import com.conmixtracking.R
import com.conmixtracking.ViewModel.OrderViewModel
import com.conmixtracking.data.OrderDetailObj
import com.conmixtracking.data.UpdateQtyOBJ
import com.conmixtracking.databinding.ChangeOrderQtyActivityBinding
import com.conmixtracking.dialog.BreakAlertDialog
import com.conmixtracking.services.api.ApiClient
import com.conmixtracking.services.api.Status
import com.conmixtracking.utils.Utils
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 10,May,2021
 */
class OrderChangeQtyActivity:AppCompatActivity(),BreakAlertDialog.ClickListener {

    private lateinit var binding: ChangeOrderQtyActivityBinding
    private lateinit var orderViewModel: OrderViewModel
    var  ODRes: OrderDetailObj?= null
    var vehicleID:String?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ChangeOrderQtyActivityBinding.inflate(layoutInflater)

        setContentView(binding.root)

        ODRes = intent.getParcelableExtra("orderDtl")
       // vehicleID = intent?.getStringExtra("vehicle_Id")?:""

        vehicleID = ConmixTrackingApp.instance?.globalVehicle

        binding.pickupQtyEdt.setText(ODRes?.pickup_quantity?.toString())
        binding.royaltyPassEdt.setText(ODRes?.pickup_quantity?.toString())

        binding.royaltyPassEdt.setSelection(binding.royaltyPassEdt.text.length)

        setupViewModel()

        binding.navBackOrderQtyCngBtn.setOnClickListener {

            finish()
            Bungee.slideRight(this)

        }

        binding.nextBtn.setOnClickListener {

            /*if(validateDetails()){

                val royaltiqty = binding.royaltyPassEdt.text?.toString()?.toDouble()?:0.0
                val pickup  = binding.pickupQtyEdt.text?.toString()?.toDouble()?:0.0

                if(pickup >= royaltiqty && royaltiqty > 0.0){
                    val fragment: BreakAlertDialog = BreakAlertDialog(this, getString(R.string.change_order_qty_alert_txt), getString(R.string.yes_btn_txt), getString(R.string.no_btn_txt))
                    fragment.show(supportFragmentManager, "alert")
                    fragment.isCancelable = false
                    fragment.setDialogListener(this)
                }else{
                    Utils.showToast(this,getString(R.string.pickup_qty_royaltity_grather_than_msg),
                        Toast.LENGTH_SHORT)
                }
            }*/

            UpdateQty()
        }
    }

    private fun setupViewModel() {

        orderViewModel = ViewModelProvider(
            this,
            OrderViewModel.ViewModelFactory(ApiClient().apiService)
        )
            .get(OrderViewModel::class.java)
    }

    override fun onBackPressed() {

        binding.navBackOrderQtyCngBtn.performClick()
    }


    private fun UpdateQty(){
       // showLoader()

        val obj = UpdateQtyOBJ(binding.pickupQtyEdt.text.toString().toDouble(),0.0,null,null)


        ApiClient.setToken()
        orderViewModel.updateQtyData(ODRes?._id,vehicleID,ODRes?.vendor_order_info?._id,obj)
            .observe(this, {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            hideLoader()
                            if (resource.data?.isSuccessful == true) {
                                resource.data.let { response ->

                                    hideLoader()

                                   // ODRes?.royality_quantity = binding.royaltyPassEdt.text.toString().toDouble()
                                    ODRes?.royality_quantity = 0.0
                                    ODRes?.pickup_quantity = binding.pickupQtyEdt.text.toString().toDouble()

                                    val intent = Intent(this@OrderChangeQtyActivity, OrderInfoActivity::class.java)
                                    intent.putExtra("orderDtl",ODRes)
                                    intent.putExtra("vehicle_Id",vehicleID)
                                    startActivity(intent)
                                    Bungee.slideLeft(this@OrderChangeQtyActivity)
                                }
                            } else {

                                hideLoader()
                                Utils.setErrorData(this, resource.data?.errorBody())
                            }

                        }
                        Status.ERROR -> {
                            hideLoader()
                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)

                        }
                        Status.LOADING -> {

                        }



                    }
                }
            })
    }

    private fun validateDetails():Boolean{

        val pickuptxt = binding.pickupQtyEdt.text
        val royaltytxt = binding.royaltyPassEdt.text

        if(pickuptxt.isNullOrEmpty()){
            Utils.showToast(this, getString(R.string.pckup_qty_empty_msg),Toast.LENGTH_SHORT)
            return false
        }else if(royaltytxt.isNullOrEmpty()){
            Utils.showToast(this, getString(R.string.royalti_qty_empty_msg),Toast.LENGTH_SHORT)
            return false
        }
        return true

    }

    private fun showLoader() {
        binding.ivLoadingEdit.visibility = View.VISIBLE
        binding.ivLoadingEdit.smoothToShow()
    }

    fun hideLoader() {
        binding.ivLoadingEdit.visibility = View.GONE
        binding.ivLoadingEdit.smoothToHide()
    }

    override fun onDoneClicked() {

        UpdateQty()

    }

    override fun onCancelClicked() {

    }


}