package com.conmixtracking.activities

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.conmixtracking.ConmixTrackingApp
import com.conmixtracking.R
import com.conmixtracking.ViewModel.LoginViewModel
import com.conmixtracking.data.OrderDetailObj
import com.conmixtracking.databinding.RejectReasonActivityBinding
import com.conmixtracking.services.api.ApiClient
import com.conmixtracking.services.api.Status
import com.conmixtracking.utils.Utils
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 10,May,2021
 */
class RejectReasonActivity :AppCompatActivity(){

    private lateinit var binding: RejectReasonActivityBinding
    private lateinit var loginViewModel: LoginViewModel
    var  ODRes: OrderDetailObj?= null
    var event :String? = null
    var cancelOrd :Boolean = false
    var vehicleId:String? = null
    var reasonEdt:String? = null
    var isFromCP:Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = RejectReasonActivityBinding.inflate(layoutInflater)

        setContentView(binding.root)
        setupViewModel()

        ODRes = intent.getParcelableExtra("orderDtl")
        event = intent.getStringExtra("fromEvnt")
        cancelOrd = intent.getBooleanExtra("cancelOrd",false)
        isFromCP = intent.getBooleanExtra("isFromCP",false)
       // vehicleId = intent.getStringExtra("vehicleId")

        vehicleId = ConmixTrackingApp.instance?.globalVehicle

        if(isFromCP){
            binding.questionTextView.text = getString(R.string.order_cp_reject_title_txt)
            binding.reasonEdt.hint = getString(R.string.order_cp_write_reason_txt)

        }else{
            binding.questionTextView.text = getString(R.string.order_reject_title_txt)
            binding.reasonEdt.hint = getString(R.string.order_write_reason_txt)
        }

        binding.closeBtn.setOnClickListener {

            finish()
            Bungee.slideRight(this)
        }

        binding.doneBtn.setOnClickListener {
            if(validateFormDetails()){
                getOtpCode(ODRes?.buyer_info?.mobile_number)
            }
        }


    }
    private fun setupViewModel() {

        loginViewModel = ViewModelProvider(
            this,
            LoginViewModel.ViewModelFactory(ApiClient().apiService)
        )
            .get(LoginViewModel::class.java)


    }

    override fun onBackPressed() {

        binding.closeBtn?.performClick()
    }

    fun validateFormDetails(): Boolean {
        reasonEdt = binding.reasonEdt?.text?.toString()


        if (reasonEdt.isNullOrEmpty()) {
            if(isFromCP){
                Utils.showToast(this, getString(R.string.order_cp_reject_reason_empty), Toast.LENGTH_SHORT)
            }else{
                Utils.showToast(this, getString(R.string.order_reject_reason_empty), Toast.LENGTH_SHORT)
            }

            return false
        }



        return true
    }

    fun getOtpCode(mobileNumber:String?){

        if(mobileNumber != null) {


            ApiClient.setToken()
            loginViewModel.getOtpCode(mobileNumber)
                .observe(this, androidx.lifecycle.Observer {
                    it?.let { resource ->
                        when (resource.status) {
                            Status.SUCCESS -> {

                                if (resource.data?.isSuccessful == true) {
                                    resource.data.let { response ->

                                        if(response.code() ==200){

                                            val intent = Intent(this@RejectReasonActivity, OTPActivity::class.java)
                                            intent.putExtra("fromEvnt","REJECTED")
                                            intent.putExtra("orderDtl",ODRes)
                                            intent.putExtra("cancelOrd",cancelOrd)
                                            intent.putExtra("vehicleId",vehicleId)
                                            intent.putExtra("reasonRj",reasonEdt)
                                            intent.putExtra("isFromCP",isFromCP)
                                            startActivity(intent)
                                            Bungee.slideLeft(this@RejectReasonActivity)



                                        } else if (response.code() == 422) {

                                            Utils.showToast(
                                                this@RejectReasonActivity,
                                                response?.message(),
                                                Toast.LENGTH_SHORT
                                            )

                                        }

                                    }
                                } else {


                                    Utils.setErrorData(this, resource.data?.errorBody())
                                }

                            }
                            Status.ERROR -> {

                                Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)

                            }
                            Status.LOADING -> {

                            }

                        }
                    }
                })

        }else{
            Utils.showToast(this,getString(R.string.error_mobile_number), Toast.LENGTH_SHORT)
        }

    }

}