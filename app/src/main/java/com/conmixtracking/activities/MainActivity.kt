package com.conmixtracking.activities

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Build
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.conmixtracking.R
import com.conmixtracking.utils.SharedPrefUtils
import spencerstudios.com.bungeelib.Bungee
import java.util.*


/**
 * Created by Hitesh Patel on 07,May,2021
 */
class MainActivity:AppCompatActivity() {
    lateinit var myLocale: Locale
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_activity)



        val lan = SharedPrefUtils.getInstance(this).getStringFromSharedPref(SharedPrefUtils.LANGUAGE_KEY)


        if (lan != null) {
            // updating the language for devices above android nougat
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                updateResources(this, lan)
            }else{
                // for devices having lower version of android os
                updateResourcesLegacy(this, lan)
            }
        }

        val isLoogedIn = SharedPrefUtils.getInstance(this@MainActivity)
            .getBooleanFromSharedPref(SharedPrefUtils.IS_LOGED_IN)
        val isDelayStatus = SharedPrefUtils.getInstance(this)
            .getBooleanFromSharedPref(SharedPrefUtils.Is_Delay_Status)

        val handler = Handler()
        handler.postDelayed(Runnable {
            try {

                if (isLoogedIn) {

                   /* val intent = Intent(this, TMListActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                    finish()*/

                    val intent = Intent(this, ChooseTrackingActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                    finish()
                    Bungee.slideLeft(this)

                } else {

                    val intent = Intent(this, LoginActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                    finish()
                    Bungee.slideLeft(this)

                }
            } catch (e: Exception) {

            }
        }, 300)

    }

    @TargetApi(Build.VERSION_CODES.N)
    private fun updateResources(context: Context, language: String) {
        val locale = Locale(language)
        Locale.setDefault(locale)
        val configuration: Configuration = context.getResources().getConfiguration()
        configuration.setLocale(locale)
        configuration.setLayoutDirection(locale)
        // return context.createConfigurationContext(configuration)
        resources.updateConfiguration(configuration, resources.getDisplayMetrics())

    }


    @SuppressLint("ObsoleteSdkInt")
    private fun updateResourcesLegacy(context: Context, language: String) {
        val locale = Locale(language)
        Locale.setDefault(locale)
        val resources: Resources = context.resources
        val configuration: Configuration = resources.getConfiguration()
        configuration.locale = locale
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLayoutDirection(locale)
        }
        resources.updateConfiguration(configuration, resources.getDisplayMetrics())
        //return context


    }

}