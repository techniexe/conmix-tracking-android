package com.conmixtracking.activities

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.conmixtracking.ConmixTrackingApp
import com.conmixtracking.R
import com.conmixtracking.ViewModel.OrderViewModel
import com.conmixtracking.adapter.SelectTimeAdapter
import com.conmixtracking.data.DelayReasonObj
import com.conmixtracking.data.OrderDetailObj
import com.conmixtracking.databinding.DelayResonBinding
import com.conmixtracking.services.api.ApiClient
import com.conmixtracking.services.api.Status
import com.conmixtracking.utils.SharedPrefUtils
import com.conmixtracking.utils.Utils
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 10,May,2021
 */
class DelayReasonActivity:AppCompatActivity() {

    private lateinit var binding: DelayResonBinding
    private lateinit var orderViewModel: OrderViewModel
    var reasonEdt:String? = null
    var hoursTime_EdtTxt:String? = null
    var minutesTime_EdtTxt:String? = null
    var ODRes: OrderDetailObj?= null
    private var vehicleId:String? = null
    var timedelay:String? = null
    var hoursArray:ArrayList<String> = ArrayList<String>()
    var words = arrayOf("1 hrs", "2 hrs", "3 hrs", "4 hrs", "5 hrs", "6 hrs","7 hrs","8 hrs","9 hrs","10 hrs",
        "11 hrs", "12 hrs", "13 hrs", "14 hrs", "15 hrs", "16 hrs","17 hrs","18 hrs","19 hrs","20 hrs",
        "21 hrs", "22 hrs", "23 hrs", "24 hrs","1 day","2 day","3 day","4 day","5 day")

    var reasonDelay:String? = null
    var reasonArray:ArrayList<String> = ArrayList<String>()
    var reasonwords = arrayOf("Heavy Traffic", "Weather Conditions", "Vehicle Breakdowns","Puncture", "Accident")
    var isFromCP:Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DelayResonBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupViewModel()

        ODRes = intent.getParcelableExtra("ODRes")
        isFromCP = intent.getBooleanExtra("isFromCP",false)
        //vehicleId = SharedPrefrence.getTMID(this)

        vehicleId = ConmixTrackingApp.instance?.globalVehicle

        hoursArray.addAll(words)
        setResposerData(hoursArray,binding.daysSpinner)

        reasonArray.addAll(reasonwords)
        setReasonData(reasonArray,binding.reasonSpinner)

        binding.doneBtn.setOnClickListener {


            if (validateFormDetails()) {

                if(isFromCP){
                    postDelayCPReason(ODRes?.vendor_order_info?._id ?: "")
                }else{
                    postDelayReason(ODRes?.vendor_order_info?._id ?: "")
                }


            }
        }

        binding.closeBtn.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }
    }

    private fun setupViewModel() {

        orderViewModel = ViewModelProvider(
            this,
            OrderViewModel.ViewModelFactory(ApiClient().apiService)
        )
            .get(OrderViewModel::class.java)


    }
    override fun onBackPressed() {

        binding.closeBtn?.performClick()
    }

    private fun setResposerData(data: ArrayList<String>, view: View) {

        val spinnerAdapter: SelectTimeAdapter = SelectTimeAdapter(view.context, data)
        binding.daysSpinner.adapter = spinnerAdapter

        binding.daysSpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                timedelay = parent.selectedItem as String

            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    private fun setReasonData(data: ArrayList<String>, view: View) {

        val spinnerAdapter: SelectTimeAdapter = SelectTimeAdapter(view.context, data)
        binding.reasonSpinner.adapter = spinnerAdapter

        binding.reasonSpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                reasonDelay = parent.selectedItem as String

            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    fun validateFormDetails(): Boolean {
        // reasonEdt = reason_Edt?.text?.toString()

        val dtm = timedelay

        if(dtm.isNullOrEmpty()){
            Utils.showToast(
                this,
                getString(R.string.delayDateTimefield_empty),
                Toast.LENGTH_SHORT
            )
            return false
        }

        val dtm1 = reasonDelay

        if(dtm1.isNullOrEmpty()){
            Utils.showToast(
                this,
                getString(R.string.delayReasonfield_empty),
                Toast.LENGTH_SHORT
            )
            return false
        }




        return true
    }

    fun postDelayReason(orderId: String){


        val delayObj = DelayReasonObj(reasonDelay, timedelay)

        ApiClient.setToken()
        orderViewModel.postDelayReasonOrder(vehicleId,
            ODRes?.vendor_order_info?._id,ODRes?._id,
            delayObj)
            .observe(this, androidx.lifecycle.Observer {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {

                            if (resource.data?.isSuccessful == true) {


                                resource.data.let { response ->

                                    if(response.code() ==202){

                                        SharedPrefUtils.getInstance(this@DelayReasonActivity)
                                            .storeBooleanInSharedPref(SharedPrefUtils.Is_Delay_Status, true)
                                            finish()
                                            Bungee.slideRight(this@DelayReasonActivity)



                                    } else if (response.code() == 400) {

                                    Utils.showToast(
                                        this@DelayReasonActivity,
                                        response.message(),
                                        Toast.LENGTH_SHORT
                                    )

                                }

                                }


                            } else {


                                Utils.setErrorData(this, resource.data?.errorBody())
                            }

                        }
                        Status.ERROR -> {

                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)

                        }
                        Status.LOADING -> {

                        }

                    }
                }
            })
    }

    fun postDelayCPReason(orderId: String){

        val delayObj = DelayReasonObj(reasonDelay, timedelay)

        ApiClient.setToken()
        orderViewModel.postDelayCPReasonOrder(vehicleId,
            ODRes?.vendor_order_info?._id,ODRes?._id,
            delayObj)
            .observe(this, androidx.lifecycle.Observer {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {

                            if (resource.data?.isSuccessful == true) {


                                resource.data.let { response ->

                                    if(response.code() ==202){

                                        SharedPrefUtils.getInstance(this@DelayReasonActivity)
                                            .storeBooleanInSharedPref(SharedPrefUtils.Is_Delay_Status, true)
                                        finish()
                                        Bungee.slideRight(this@DelayReasonActivity)



                                    } else if (response.code() == 400) {

                                        Utils.showToast(
                                            this@DelayReasonActivity,
                                            response.message(),
                                            Toast.LENGTH_SHORT
                                        )

                                    }

                                }


                            } else {


                                Utils.setErrorData(this, resource.data?.errorBody())
                            }

                        }
                        Status.ERROR -> {

                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)

                        }
                        Status.LOADING -> {

                        }

                    }
                }
            })
    }

}