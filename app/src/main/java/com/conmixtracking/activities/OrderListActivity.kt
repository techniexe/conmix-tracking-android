package com.conmixtracking.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.conmixtracking.ConmixTrackingApp
import com.conmixtracking.R
import com.conmixtracking.ViewModel.OrderViewModel
import com.conmixtracking.adapter.OrderDetailListAdapter
import com.conmixtracking.data.OrderDetailObj
import com.conmixtracking.databinding.OrderDetailListBinding
import com.conmixtracking.services.api.ApiClient
import com.conmixtracking.services.api.Status
import com.conmixtracking.utils.Utils
import spencerstudios.com.bungeelib.Bungee
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by Hitesh Patel on 10,May,2021
 */
class OrderListActivity :AppCompatActivity(),OrderDetailListAdapter.orderlistInterface{
    private lateinit var binding: OrderDetailListBinding
    private lateinit var orderViewModel: OrderViewModel
    var orderList: ArrayList<OrderDetailObj> = ArrayList<OrderDetailObj>()
    var mAdapter: OrderDetailListAdapter? = null
    var vehicleID:String?= null
    var isFromCP:Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = OrderDetailListBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupViewModel()
       //vehicleID = intent?.getStringExtra("vehicleID")?:""

        vehicleID = ConmixTrackingApp.instance?.globalVehicle
        isFromCP = intent?.getBooleanExtra("isFromCP",false)?:false

        binding.navBackOrderList?.setOnClickListener {
            finish()
            Bungee.slideRight(this@OrderListActivity)
        }
    }

    private fun setupViewModel() {

        orderViewModel = ViewModelProvider(
            this,
            OrderViewModel.ViewModelFactory(ApiClient().apiService)
        )
            .get(OrderViewModel::class.java)


    }

    override fun onResume() {
        super.onResume()
        if (!Utils.checkConnectivity(this)) {
            Utils.showToast(
                this, getString(R.string.no_internet_msg),
                Toast.LENGTH_SHORT
            )
        } else {

            if(isFromCP){
                getOrderCPListData(vehicleID)
            }else{
                getOrderListData(vehicleID)
            }
        }
    }


    fun getOrderListData(vehicleId:String?) {
        ApiClient.setToken()

        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val currentDateN = sdf.format(Date())
        val crntDate = Utils.GmtCurrentDateFormate(currentDateN)

        orderViewModel.getOrderDetailListData(vehicleId,crntDate)
            .observe(this, androidx.lifecycle.Observer {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            if (resource.data?.isSuccessful!!) {
                                resource.data.let { response ->
                                    if (response?.body()?.order?.size ?: 0 > 0) {
                                        orderList.clear()
                                        orderList?.addAll(response?.body()?.order ?: arrayListOf())
                                        mAdapter = OrderDetailListAdapter(this@OrderListActivity, orderList,isFromCP)
                                        mAdapter?.setListener(this@OrderListActivity)
                                        binding.vehicleListRecycler?.setAdapter(mAdapter)

                                    }

                                    emptyView()
                                }
                            } else {
                                emptyView()
                                Utils.setErrorData(this, resource.data.errorBody())
                            }

                        }
                        Status.ERROR -> {
                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                            emptyView()
                        }
                        Status.LOADING -> {

                        }
                    }
                }
            })
    }


    fun getOrderCPListData(vehicleId:String?) {
        ApiClient.setToken()
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val currentDateN = sdf.format(Date())
        val crntDate = Utils.GmtCurrentDateFormate(currentDateN)

        orderViewModel.getOrderDetailListCPData(vehicleId,crntDate)
            .observe(this, androidx.lifecycle.Observer {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            if (resource.data?.isSuccessful!!) {
                                resource.data.let { response ->
                                    if (response?.body()?.order?.size ?: 0 > 0) {
                                        orderList.clear()
                                        orderList?.addAll(response?.body()?.order ?: arrayListOf())
                                        mAdapter = OrderDetailListAdapter(this@OrderListActivity, orderList,isFromCP)
                                        mAdapter?.setListener(this@OrderListActivity)
                                        binding.vehicleListRecycler?.setAdapter(mAdapter)
                                    }
                                    emptyView()
                                }
                            } else {
                                emptyView()
                                Utils.setErrorData(this, resource.data.errorBody())
                            }

                        }
                        Status.ERROR -> {
                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                            emptyView()
                        }
                        Status.LOADING -> {

                        }
                    }
                }
            })
    }

    fun emptyView() {

        if (orderList.size == 0) {
            binding.emptyRecylerview.visibility = View.VISIBLE
            if(isFromCP){
                binding.emptyTextFollowFragment?.text = getString(R.string.no_data_cp_empty_screen)
            }else{
                binding.emptyTextFollowFragment?.text = getString(R.string.no_data_empty_screen)
            }

            binding.vehicleListRecycler?.visibility = View.GONE

        } else {
            binding.emptyRecylerview.visibility = View.GONE
            binding.vehicleListRecycler?.visibility = View.VISIBLE
        }
    }


    override fun onVehicleListRowClick(ordobj: OrderDetailObj) {

        if(ordobj?.event_status?.equals("TM_ASSIGNED") == true || ordobj?.event_status?.equals("CP_ASSIGNED") == true){

            val intent = Intent(this@OrderListActivity, OrderDetailActivity::class.java)
            intent.putExtra("ODRes",ordobj)
            intent.putExtra("vehicleID",vehicleID)
            intent.putExtra("isFromCP",isFromCP)
            startActivity(intent)
            Bungee.slideLeft(this@OrderListActivity)


        }else{
            val intent = Intent(this@OrderListActivity, LoadOrderDetailsActivity::class.java)
            intent.putExtra("ODRes",ordobj)
            intent.putExtra("vehicleID",vehicleID)
            intent.putExtra("isFromCP",isFromCP)
            startActivity(intent)
            Bungee.slideLeft(this@OrderListActivity)
            finish()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        binding.navBackOrderList?.performClick()
    }
}