package com.conmixtracking.activities

import android.Manifest
import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.conmixtracking.ConmixTrackingApp
import com.conmixtracking.R
import com.conmixtracking.ViewModel.OrderViewModel
import com.conmixtracking.customview.URIPathHelper
import com.conmixtracking.data.OrderDetailObj
import com.conmixtracking.databinding.BuyerSignLayoutBinding
import com.conmixtracking.services.api.ApiClient
import com.conmixtracking.services.api.Status
import com.conmixtracking.utils.Utils
import com.williamww.silkysignature.views.SignaturePad
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import spencerstudios.com.bungeelib.Bungee
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.util.*


/**
 * Created by Hitesh Patel on 10,May,2021
 */
class DrawBuyerSignatureActivity:AppCompatActivity() {
    private lateinit var binding: BuyerSignLayoutBinding
    private lateinit var orderViewModel: OrderViewModel
    private val REQUEST_CODE_IMAGE = 100
    private val REQUEST_CODE_PERMISSIONS = 101

    private val KEY_PERMISSIONS_REQUEST_COUNT = "KEY_PERMISSIONS_REQUEST_COUNT"
    private val MAX_NUMBER_REQUEST_PERMISSIONS = 2
    private var vehicleId:String? = null

    private val permissions = Arrays.asList(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )

    private var permissionRequestCount: Int = 0

    var order_id:String? = null
    var ODRes: OrderDetailObj?= null
    var isSignedbuyer = false
    var isSigneddriver = false
    var isFromCP:Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = BuyerSignLayoutBinding.inflate(layoutInflater)

        setContentView(binding.root)
        setupViewModel()


        order_id = intent.getStringExtra("order_id")
        ODRes = intent.getParcelableExtra("orderDtl")
        isFromCP = intent.getBooleanExtra("isFromCP",false)
       //vehicleId = SharedPrefrence.getTMID(this)

        vehicleId = ConmixTrackingApp?.instance?.globalVehicle
        binding.buyerNameTxt?.text = " : "+ODRes?.buyer_info?.full_name

        if(isFromCP){
            binding.driverNameFix.text = getString(R.string.operator_name_txt)
            binding.driverNameTxt?.text = " : "+ODRes?.operator_info?.operator_name
            binding.hintDriverTxt?.text =getString(R.string.draw_operator_sign_hint_txt)
        }else{
            binding.driverNameFix.text = getString(R.string.driver_name_txt_fix)
            binding.driverNameTxt?.text = " : "+ODRes?.driver1_info?.driver_name
            binding.hintDriverTxt?.text =getString(R.string.draw_driver_sign_hint_txt)
        }

        // Make sure the app has correct permissions to run
        requestPermissionsIfNecessary()

        binding.buyerSignaturePad.setOnSignedListener(object : SignaturePad.OnSignedListener {
            override fun onStartSigning() {
                binding.hintBuyerTxt.visibility = View.GONE
                binding.buyerSignatureCancel.visibility = View.VISIBLE

                isSignedbuyer = true

                if(isSigneddriver){
                    binding.doneTxt.visibility = View.VISIBLE
                }
            }

            override fun onClear() {
                isSignedbuyer = false
                binding.hintBuyerTxt.visibility = View.VISIBLE
                binding.buyerSignatureCancel.visibility = View.GONE
                binding.doneTxt.visibility = View.GONE
            }

            override fun onSigned() {

            }

        })

        binding.driverSignaturePad.setOnSignedListener(object : SignaturePad.OnSignedListener {
            override fun onStartSigning() {
                binding.hintDriverTxt.visibility = View.GONE
                binding.driverSignatureCancel.visibility = View.VISIBLE

                isSigneddriver = true

                if(isSignedbuyer){
                    binding.doneTxt.visibility = View.VISIBLE
                }
            }

            override fun onClear() {
                isSigneddriver = false
                binding.hintDriverTxt.visibility = View.VISIBLE
                binding.driverSignatureCancel.visibility = View.GONE
                binding.doneTxt.visibility = View.GONE
            }

            override fun onSigned() {

            }

        })

        binding.driverSignatureCancel.setOnClickListener {
            binding.driverSignaturePad.clear()
        }

        binding.buyerSignatureCancel.setOnClickListener {
            binding.buyerSignaturePad.clear()
        }



        binding.navBackFollowersList?.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }
        binding.doneTxt.setOnClickListener {
            val suppliersignatureBitmap: Bitmap = binding.buyerSignaturePad.getSignatureBitmap()
            val suppliersignatureFile = addJpgSignatureToGallery(suppliersignatureBitmap)

            val driverSignatureBitmap: Bitmap = binding.driverSignaturePad.getSignatureBitmap()
            val driverSignatureFile = addJpgSignatureToGallery(driverSignatureBitmap)


            if (suppliersignatureFile?.exists()== true && driverSignatureFile?.exists() == true) {
                binding.uploadImage.visibility = View.VISIBLE
                binding.uploadImage.start()

                if(isFromCP){
                    uploadDocumentCPfun(suppliersignatureFile,driverSignatureFile)
                }else{
                    uploadDocumentfun(suppliersignatureFile,driverSignatureFile)
                }


                binding.doneTxt.visibility = View.GONE

            } else {
                Toast.makeText(
                    this@DrawBuyerSignatureActivity,
                    "Unable to store the signature",
                    Toast.LENGTH_SHORT
                ).show()
                binding.doneTxt.visibility = View.VISIBLE
            }
        }
    }

    private fun setupViewModel() {

        orderViewModel = ViewModelProvider(
            this,
            OrderViewModel.ViewModelFactory(ApiClient().apiService)
        )
            .get(OrderViewModel::class.java)


    }

    @SuppressLint("DefaultLocale")
    fun addJpgSignatureToGallery(signature: Bitmap?): File? {

        try {

            val pathvalue = saveToGallery(this, signature!!, "SignaturePad")

            //Log.v("log_tag", "path_v" + File(pathvalue).absolutePath)

            // val file = File(pathvalue)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                val uriPathHelper = URIPathHelper()
                val filePath = uriPathHelper.getPath(this, Uri.parse(pathvalue))
                return  File(filePath)
            }else{
                val file = File(pathvalue)
                return file
            }

        } catch (e: IOException) {
            e.printStackTrace()
        }
        return null
    }

    fun saveToGallery(context: Context, bitmap: Bitmap, albumName: String): String? {
        val filename = "${System.currentTimeMillis()}.jpg"
        val write: (OutputStream) -> Boolean = {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, it)
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val contentValues = ContentValues().apply {
                put(MediaStore.MediaColumns.DISPLAY_NAME, filename)
                put(MediaStore.MediaColumns.MIME_TYPE, "image/jpg")
                put(
                    MediaStore.MediaColumns.RELATIVE_PATH,
                    "${Environment.DIRECTORY_DCIM}/$albumName"
                )
            }

            context.contentResolver.let {
                it.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)?.let { uri ->
                    it.openOutputStream(uri)?.let(write)
                    return uri.toString()
                }
            }
        } else {
            val imagesDir =
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM)
                    .toString() + File.separator + albumName
            val file = File(imagesDir)
            if (!file.exists()) {
                file.mkdir()
            }
            val image = File(imagesDir, filename)
            write(FileOutputStream(image))

            return image.toString()
        }
        return null
    }

    /**
     * Request permissions twice - if the user denies twice then show a toast about how to update
     * the permission for storage. Also disable the button if we don't have access to pictures on
     * the device.
     */
    private fun requestPermissionsIfNecessary() {
        if (!checkAllPermissions()) {
            if (permissionRequestCount < MAX_NUMBER_REQUEST_PERMISSIONS) {
                permissionRequestCount += 1
                ActivityCompat.requestPermissions(
                    this,
                    permissions.toTypedArray(),
                    REQUEST_CODE_PERMISSIONS
                )
            } else {
                Toast.makeText(
                    this,
                    R.string.set_permissions_in_settings,
                    Toast.LENGTH_LONG
                ).show()
                binding.doneTxt.isEnabled = false
            }
        }
    }


    /** Permission Checking  */
    private fun checkAllPermissions(): Boolean {
        var hasPermissions = true
        for (permission in permissions) {
            hasPermissions = hasPermissions and isPermissionGranted(permission)
        }
        return hasPermissions
    }

    private fun isPermissionGranted(permission: String) =
        ContextCompat.checkSelfPermission(this, permission) ==
                PackageManager.PERMISSION_GRANTED

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            requestPermissionsIfNecessary() // no-op if permissions are granted already.
        }
    }

    override fun onBackPressed() {

        binding.navBackFollowersList.performClick()
    }

    fun uploadDocumentfun(buyerFile: File, driverFile: File){

        val buyerrequestFile = RequestBody.create("image/jpeg".toMediaTypeOrNull(), buyerFile)

        // MultipartBody.Part is used to send also the actual file name
        val buyerbody = MultipartBody.Part.createFormData("buyer_drop_signature", buyerFile.name, buyerrequestFile)


        val driverRequestFile = RequestBody.create("image/jpeg".toMediaTypeOrNull(), driverFile)

        // MultipartBody.Part is used to send also the actual file name
        val driverbody = MultipartBody.Part.createFormData("driver_drop_signature", driverFile.name, driverRequestFile)

        ApiClient.setToken()

        orderViewModel.uploadSignSupplierOrder(ODRes?._id,vehicleId,ODRes?.vendor_order_info?._id,null,null, buyerbody,driverbody).observe(this, androidx.lifecycle.Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {

                        if (resource.data?.isSuccessful!!) {
                            resource.data.let { response ->
                                if (response?.code() == 202) {
                                    binding.doneTxt.visibility = View.GONE

                                    binding.uploadImage.visibility = View.GONE
                                    binding.uploadImage.stop()
                                    Utils.showToast(this@DrawBuyerSignatureActivity, getString(R.string.sucessfully_upload_sign), Toast.LENGTH_SHORT)
                                    val intent = Intent(this@DrawBuyerSignatureActivity, OrderDeliverReciptActivity::class.java)
                                    intent.putExtra("ODRes",ODRes)
                                    intent.putExtra("isFromCP",false)
                                    startActivity(intent)
                                    Bungee.slideLeft(this@DrawBuyerSignatureActivity)
                                    finish()

                                }
                            }
                        } else {
                            binding.uploadImage.visibility = View.GONE
                            binding.uploadImage.stop()
                            binding.doneTxt.visibility = View.VISIBLE
                            Utils.setErrorData(this, resource.data.errorBody())
                        }

                    }
                    Status.ERROR -> {
                        binding.uploadImage.visibility = View.GONE
                        binding.uploadImage.stop()
                        binding.doneTxt.visibility = View.VISIBLE
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {

                    }
                }
            }
        })

    }

    fun uploadDocumentCPfun(buyerFile: File, driverFile: File){

        val buyerrequestFile = RequestBody.create("image/jpeg".toMediaTypeOrNull(), buyerFile)

        // MultipartBody.Part is used to send also the actual file name
        val buyerbody = MultipartBody.Part.createFormData("buyer_drop_signature", buyerFile.name, buyerrequestFile)


        val driverRequestFile = RequestBody.create("image/jpeg".toMediaTypeOrNull(), driverFile)

        // MultipartBody.Part is used to send also the actual file name
        val driverbody = MultipartBody.Part.createFormData("driver_drop_signature", driverFile.name, driverRequestFile)

        ApiClient.setToken()

        orderViewModel.uploadSignSupplierCPOrder(ODRes?._id,vehicleId,ODRes?.vendor_order_info?._id,null,null, buyerbody,driverbody).observe(this, androidx.lifecycle.Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {

                        if (resource.data?.isSuccessful!!) {
                            resource.data.let { response ->
                                if (response?.code() == 202) {
                                    binding.doneTxt.visibility = View.GONE

                                    binding.uploadImage.visibility = View.GONE
                                    binding.uploadImage.stop()
                                    Utils.showToast(this@DrawBuyerSignatureActivity, getString(R.string.sucessfully_upload_sign), Toast.LENGTH_SHORT)
                                    val intent = Intent(this@DrawBuyerSignatureActivity, OrderDeliverReciptActivity::class.java)
                                    intent.putExtra("ODRes",ODRes)
                                    intent.putExtra("isFromCP",isFromCP)
                                    startActivity(intent)
                                    Bungee.slideLeft(this@DrawBuyerSignatureActivity)
                                    finish()

                                }
                            }
                        } else {
                            binding.uploadImage.visibility = View.GONE
                            binding.uploadImage.stop()
                            binding.doneTxt.visibility = View.VISIBLE
                            Utils.setErrorData(this, resource.data.errorBody())
                        }

                    }
                    Status.ERROR -> {
                        binding.uploadImage.visibility = View.GONE
                        binding.uploadImage.stop()
                        binding.doneTxt.visibility = View.VISIBLE
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {

                    }
                }
            }
        })

    }

}