package com.conmixtracking.activities

import android.Manifest
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.content.res.Resources
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.PopupMenu
import androidx.core.app.ActivityCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.conmixtracking.ConmixTrackingApp

import com.conmixtracking.R

import com.conmixtracking.ViewModel.TMListViewModel
import com.conmixtracking.adapter.VehicleListAdapter

import com.conmixtracking.data.TMListObjRes

import com.conmixtracking.databinding.VehicleListBinding
import com.conmixtracking.dialog.BreakAlertDialog
import com.conmixtracking.dialog.SelectLanguageDialog
import com.conmixtracking.services.api.ApiClient
import com.conmixtracking.services.api.Status
import com.conmixtracking.utils.PermissionUtils
import com.conmixtracking.utils.SharedPrefUtils
import com.conmixtracking.utils.Utils
import com.google.android.gms.location.*
import spencerstudios.com.bungeelib.Bungee
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by Hitesh Patel on 07,May,2021
 */
class TMListActivity :AppCompatActivity(), VehicleListAdapter.vehiclelistInterface,
    BreakAlertDialog.ClickListener, SelectLanguageDialog.DialogToFragment{
    private lateinit var binding: VehicleListBinding
    var vehicleList:ArrayList<TMListObjRes>? = ArrayList<TMListObjRes>()
    var mAdapter: VehicleListAdapter? = null
    var beforeTime: String? = null
    var isLoading = true
    private lateinit var tmListViewModel: TMListViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = VehicleListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupViewModel()
        binding.loaderView.visibility = View.VISIBLE
        binding.loaderView.show()



        binding.navBackFollowersList.setOnClickListener {

            finish()
            Bungee.slideRight(this)
        }


        if (!Utils.checkConnectivity(this)) {
            Utils.showToast(this, getString(R.string.no_internet_msg),Toast.LENGTH_SHORT)
        } else {
            getVehicleListData(beforeTime)
        }

        try {
            binding.vehicleListRecycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    if (dy > 0) {
                        val visibleItemCount = binding.vehicleListRecycler.layoutManager?.getChildCount()
                        val totalItemCount = binding.vehicleListRecycler.layoutManager?.getItemCount()
                        val pastVisiblesItems = (binding.vehicleListRecycler.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()


                        if (isLoading) {
                            if (visibleItemCount != null) {
                                if (visibleItemCount.plus(pastVisiblesItems) >= totalItemCount ?: 0) {


                                    isLoading = false
                                    if (mAdapter != null) {
                                        //mAdapter!!.loading()

                                        binding.progressBar.visibility = View.VISIBLE

                                        // afterTime = null
                                        val handler = Handler()
                                        handler.postDelayed(Runnable {


                                            isLoading = true

                                            try {

                                                beforeTime = vehicleList?.get(vehicleList?.size?:0 - 1)?.created_at
                                            } catch (e: Exception) {

                                            }


                                            if (!Utils.checkConnectivity(this@TMListActivity)) {
                                                Utils.showToast(
                                                    this@TMListActivity, getString(R.string.no_internet_msg),
                                                    Toast.LENGTH_SHORT
                                                )
                                            } else {
                                                // getVehicleListData()

                                                getVehicleListData(beforeTime)

                                            }

                                        }, 500)
                                    }
                                }
                            }
                        }
                    }
                }
            })

        } catch (e: Exception) {
            e.printStackTrace()
        }

        binding.menuIcon.setOnClickListener {

            showPopupMenu(binding.menuIcon)
        }

    }

    override fun onBackPressed() {
        binding.navBackFollowersList?.performClick()
    }

    private fun setupViewModel() {

        tmListViewModel = ViewModelProvider(
            this,
            TMListViewModel.ViewModelFactory(ApiClient().apiService)
        )
            .get(TMListViewModel::class.java)


    }





    fun showPopupMenu(v: View) {
        val popup = PopupMenu(this@TMListActivity, v)
        val inflater = popup.menuInflater

        inflater.inflate(R.menu.log_out_menu, popup.menu)
        popup.show()
        popup.setOnMenuItemClickListener { item ->

            when (item.itemId) {

                /* R.id.logoutMenu -> {
                     val fragment: BreakAlertDialog = BreakAlertDialog(
                         this,
                         getString(R.string.logout_alert_txt),
                         "YES",
                         "NO"
                     )
                     fragment.show(supportFragmentManager, "alert")
                     fragment.isCancelable = false
                     fragment.setDialogListener(this)
                     true
                 }*/

                R.id.selectLangMenu -> {

                    val languageDialog = SelectLanguageDialog()
                    languageDialog.setListener(this)
                    languageDialog.show(
                        supportFragmentManager,
                        SelectLanguageDialog::class.java.simpleName
                    )

                    true
                }

                else -> false
            }

        }
    }


    fun getVehicleListData(befrTime:String?) {


            ApiClient.setToken()


        tmListViewModel.getTMListData(null,null,null,null,null,befrTime,null,null)
                .observe(this, androidx.lifecycle.Observer {
                    it?.let { resource ->
                        when (resource.status) {
                            Status.SUCCESS -> {
                                if (resource.data?.isSuccessful!!) {
                                    resource.data.let { response ->
                                        setResposerData(response.body()?.data)
                                    }
                                } else {
                                    emptyView()
                                    Utils.setErrorData(this, resource.data.errorBody())
                                }

                            }
                            Status.ERROR -> {
                                Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                            }
                            Status.LOADING -> {

                            }

                        }
                    }
                })

    }

    private fun setResposerData(response: ArrayList<TMListObjRes>?) {

            try {
                if (mAdapter == null) {
                    beforeTime = null

                    vehicleList?.clear()
                    if (response != null) {
                        vehicleList?.addAll(response)
                        mAdapter = vehicleList?.let { VehicleListAdapter(this@TMListActivity, it) }
                        val mLayoutManager = LinearLayoutManager(this@TMListActivity)
                        binding.vehicleListRecycler.setLayoutManager(mLayoutManager)
                        mAdapter?.setListener(this@TMListActivity)
                        binding.vehicleListRecycler.setAdapter(mAdapter)
                    }



                    // myOrderRV?.setAdapter(adapter)
                    // recyclerView?.smoothSnapToPosition(0)
                    // mAdapter?.loadDone()
                    // progressBar?.visibility = View.GONE


                } else if (vehicleList != null) {

                    if (beforeTime != null) {

                        if (response != null) {
                            vehicleList?.addAll(response)
                        }

                        try {
                            mAdapter?.notifyDataSetChanged()
                        } catch (e: Exception) {

                        }
                        binding.progressBar.visibility = View.GONE

                    }  else {


                        vehicleList?.clear()
                        response?.forEach {
                            if (vehicleList!!.contains(it)) {
                                val index = vehicleList!!.indexOf(it)
                                vehicleList!!.removeAt(index)
                                vehicleList!!.add(it)
                            } else {
                                vehicleList!!.add(it)
                            }

                        }
                        try {
                            mAdapter?.notifyDataSetChanged()
                        } catch (e: Exception) {

                        }
                        binding.progressBar.visibility = View.GONE
                        //mAdapter?.loadDone()
                        //(spottfeedListRv.getItemAnimator() as SimpleItemAnimator).supportsChangeAnimations = false
                    }
                }


                binding.loaderView.visibility = View.GONE
                binding.loaderView.hide()
                binding.progressBar.visibility = View.GONE
                emptyView()

            } catch (e: Exception) {
                binding.loaderView.visibility = View.GONE
                binding.loaderView.hide()
                emptyView()
                binding.progressBar.visibility = View.GONE
            }


        emptyView()

    }

    override fun onVehicleListRowClick(vdo: TMListObjRes) {
        val intent = Intent(this, TruckDetailActivity::class.java)
        intent.putExtra("id", vdo?._id)
        intent.putExtra("vehicleObj", vdo)
        startActivity(intent)
        Bungee.slideLeft(this)
    }

    override fun onDoneClicked() {



        SharedPrefUtils.getInstance(this).storeBooleanInSharedPref(
            SharedPrefUtils.IS_LOGED_IN,
            false
        )
        SharedPrefUtils.getInstance(this).storeBooleanInSharedPref(
            SharedPrefUtils.Is_Delay_Status,
            false
        )
        SharedPrefUtils.getInstance(this).storeStringInSharedPref(SharedPrefUtils.ORDER_ID, null)
        SharedPrefUtils.getInstance(this).storeStringInSharedPref(SharedPrefUtils.TM_ID, null)

        ConmixTrackingApp.instance?.globalVehicle = null

        Utils.showToast(this, "Successfully Logout", Toast.LENGTH_SHORT)

        val intent = Intent(this, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
        finish()


    }

    override fun onCancelClicked() {

    }

    override fun sortDialogDismiss() {

    }

    override fun sortDialogSave(sortType: String) {

        var slv = "en"

        if(sortType == "English"){
            slv = "en"


            SharedPrefUtils.getInstance(this@TMListActivity).storeStringInSharedPref(
                SharedPrefUtils.LANGUAGE_KEY,
                "en"
            )
        }else{
            slv = "hi"

            SharedPrefUtils.getInstance(this@TMListActivity).storeStringInSharedPref(
                SharedPrefUtils.LANGUAGE_KEY,
                "hi"
            )
        }



        // updating the language for devices above android nougat
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            updateResources(this, slv)
        }else{
            // for devices having lower version of android os
            updateResourcesLegacy(this, slv)
        }

    }

    @TargetApi(Build.VERSION_CODES.N)
    private fun updateResources(context: Context, language: String) {
        val locale = Locale(language)
        Locale.setDefault(locale)
        val configuration: Configuration = context.getResources().getConfiguration()
        configuration.setLocale(locale)
        configuration.setLayoutDirection(locale)
        // return context.createConfigurationContext(configuration)
        resources.updateConfiguration(configuration, resources.getDisplayMetrics())

        beforeTime = null
        recreate()
    }

    @SuppressLint("ObsoleteSdkInt")
    private fun updateResourcesLegacy(context: Context, language: String) {
        val locale = Locale(language)
        Locale.setDefault(locale)
        val resources: Resources = context.resources
        val configuration: Configuration = resources.getConfiguration()
        configuration.locale = locale
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLayoutDirection(locale)
        }
        resources.updateConfiguration(configuration, resources.getDisplayMetrics())
        //return context
        beforeTime = null
        recreate()
    }


    fun emptyView() {

        if (vehicleList?.isEmpty() == true) {
            binding.emptyRecylerview.visibility = View.VISIBLE
            binding.emptyTextFollowFragment.text = getString(R.string.no_data_empty_screen)
            binding.vehicleListRecycler.visibility = View.GONE

        } else {
            binding.emptyRecylerview.visibility = View.GONE
            binding.vehicleListRecycler.visibility = View.VISIBLE
        }
    }



}