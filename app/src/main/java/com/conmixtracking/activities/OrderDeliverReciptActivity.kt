package com.conmixtracking.activities

import android.content.Intent

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.conmixtracking.ConmixTrackingApp
import com.conmixtracking.R
import com.conmixtracking.ViewModel.OrderViewModel
import com.conmixtracking.data.OrderDetailObj
import com.conmixtracking.data.UploadTmInvoiceObj
import com.conmixtracking.databinding.EmptyLayoutBinding
import com.conmixtracking.databinding.OrderDeliverRecieptLayoutBinding
import com.conmixtracking.services.api.ApiClient
import com.conmixtracking.services.api.Status
import com.conmixtracking.utils.SharedPrefUtils
import com.conmixtracking.utils.Utils
import com.google.gson.internal.bind.util.ISO8601Utils
import com.rajat.pdfviewer.PdfViewerActivity

import spencerstudios.com.bungeelib.Bungee
import java.text.ParsePosition
import java.text.SimpleDateFormat


/**
 * Created by Hitesh Patel on 10,May,2021
 */
class OrderDeliverReciptActivity:AppCompatActivity() {

    private lateinit var binding: OrderDeliverRecieptLayoutBinding
    private lateinit var bindingNoOrder: EmptyLayoutBinding
    private lateinit var orderViewModel: OrderViewModel
    var  ODRes: OrderDetailObj?= null
    private var vehicleId:String? = null
    var isFromCP:Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = OrderDeliverRecieptLayoutBinding.inflate(layoutInflater)
        bindingNoOrder = binding.emptyOrder
        setContentView(binding.root)
        setupViewModel()

        ODRes = intent.getParcelableExtra("ODRes")
        isFromCP = intent.getBooleanExtra("isFromCP",false)
        //vehicleId = SharedPrefrence.getTMID(this)

        vehicleId = ConmixTrackingApp.instance?.globalVehicle


        if(isFromCP){
           getOrderCPDetailData()
        }else{
            getOrderDetailData()
        }


        binding.navBackFollowersList.setOnClickListener {

            val intent = Intent(this@OrderDeliverReciptActivity, ChooseTrackingActivity::class.java)
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            this@OrderDeliverReciptActivity.startActivity(intent)
            Bungee.slideRight(this@OrderDeliverReciptActivity)
        }
    }

    private fun setupViewModel() {

        orderViewModel = ViewModelProvider(
            this,
            OrderViewModel.ViewModelFactory(ApiClient().apiService)
        )
            .get(OrderViewModel::class.java)
   }


    private fun getOrderDetailData(){

        ApiClient.setToken()
        orderViewModel.getOrdeIdDetailData(ODRes?.vendor_order_info?._id,ODRes?._id)
            .observe(this, {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {

                            if (resource.data?.isSuccessful!!) {
                                resource.data.let { response ->
                                    ODRes = response?.body()?.order


                                    if(ODRes != null){
                                        binding.cardviewly.visibility = View.VISIBLE
                                        bindingNoOrder.emptyFrame.visibility = View.GONE
                                        upDateUi(ODRes)
                                        sendDeliverStatus()
                                    }else{
                                        binding.cardviewly.visibility = View.GONE
                                        bindingNoOrder.emptyFrame.visibility = View.VISIBLE
                                    }
                                }
                            } else {

                                binding.cardviewly.visibility = View.GONE
                                bindingNoOrder.emptyFrame.visibility = View.VISIBLE
                                Utils.setErrorData(this, resource.data.errorBody())
                            }

                        }
                        Status.ERROR -> {

                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                            binding.cardviewly.visibility = View.GONE
                            bindingNoOrder.emptyFrame.visibility = View.VISIBLE
                        }
                        Status.LOADING -> {

                        }
                   }
                }
            })
    }

    private fun getOrderCPDetailData(){

        ApiClient.setToken()
        orderViewModel.getOrdeIdCPDetailData(ODRes?.vendor_order_info?._id,ODRes?._id)
            .observe(this, {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {

                            if (resource.data?.isSuccessful!!) {
                                resource.data.let { response ->
                                    ODRes = response?.body()?.order


                                    if(ODRes != null){
                                        binding.cardviewly.visibility = View.VISIBLE
                                        bindingNoOrder.emptyFrame.visibility = View.GONE
                                        upDateUi(ODRes)
                                        sendDeliverCPStatus()
                                    }else{
                                        binding.cardviewly.visibility = View.GONE
                                        bindingNoOrder.emptyFrame.visibility = View.VISIBLE
                                    }
                                }
                            } else {

                                binding.cardviewly.visibility = View.GONE
                                bindingNoOrder.emptyFrame.visibility = View.VISIBLE
                                Utils.setErrorData(this, resource.data.errorBody())
                            }

                        }
                        Status.ERROR -> {

                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                            binding.cardviewly.visibility = View.GONE
                            bindingNoOrder.emptyFrame.visibility = View.VISIBLE
                        }
                        Status.LOADING -> {

                        }



                    }
                }
            })
    }

    private fun sendDeliverStatus(){
        ApiClient.setToken()

        orderViewModel.deliverSuceessCall(vehicleId,ODRes?.vendor_order_info?._id,ODRes?._id)
            .observe(this, androidx.lifecycle.Observer {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {

                            if (resource.data?.isSuccessful!!) {
                                resource.data.let { response ->

                                    if(response.code() == 202 ){

                                       // uploadTmInvoice()
                                        Utils.showToast(this@OrderDeliverReciptActivity,getString(R.string.order_success_toast_msg),Toast.LENGTH_SHORT)
                                        SharedPrefUtils.getInstance(this@OrderDeliverReciptActivity).storeBooleanInSharedPref(SharedPrefUtils.Is_Delay_Status,false)
                                    }else if(response.code() == 400){
                                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                                    }
                                }
                            } else {


                                Utils.setErrorData(this, resource.data.errorBody())
                            }

                        }
                        Status.ERROR -> {

                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)

                        }
                        Status.LOADING -> {

                        }
                    }
                }
            })
    }


    private fun uploadTmInvoice(){
        val upldtmInvcObj = UploadTmInvoiceObj(ODRes?._id,ODRes?.vendor_order_info?._id,vehicleId)
        ApiClient.setToken()

        orderViewModel.uploadTmInvoice(upldtmInvcObj)
            .observe(this, androidx.lifecycle.Observer {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {

                            if (resource.data?.isSuccessful!!) {
                                resource.data.let { response ->
                                    hideLoader()
                                    if(response.code() == 200 ){

                                    }else if(response.code() == 400){
                                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                                    }
                                }
                            } else {

                                hideLoader()
                                Utils.setErrorData(this, resource.data.errorBody())
                            }

                        }
                        Status.ERROR -> {
                            hideLoader()
                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)

                        }
                        Status.LOADING -> {
                            showLoader()
                        }
                    }
                }
            })
    }

    private fun sendDeliverCPStatus(){
        ApiClient.setToken()

        orderViewModel.deliverSuceessCPCall(vehicleId,ODRes?.vendor_order_info?._id,ODRes?._id)
            .observe(this, androidx.lifecycle.Observer {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {

                            if (resource.data?.isSuccessful!!) {
                                resource.data.let { response ->

                                    if(response.code() == 202 ){
                                        Utils.showToast(this@OrderDeliverReciptActivity,getString(R.string.order_success_toast_msg),Toast.LENGTH_SHORT)
                                        SharedPrefUtils.getInstance(this@OrderDeliverReciptActivity).storeBooleanInSharedPref(SharedPrefUtils.Is_Delay_Status,false)
                                    }else if(response.code() == 400){
                                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                                    }
                                }
                            } else {


                                Utils.setErrorData(this, resource.data.errorBody())
                            }

                        }
                        Status.ERROR -> {

                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)

                        }
                        Status.LOADING -> {

                        }
                    }
                }
            })
    }

    fun upDateUi(ODRes:OrderDetailObj?){

        if(ODRes != null){

            binding.orderIDLabel.text = "ORDER ID #"+ODRes.vendor_order_info?._id
            val string1 = ODRes.created_at
            val Date1 = ISO8601Utils.parse(string1, ParsePosition(0))
            val posFormat = SimpleDateFormat("dd MMM yyyy")
            val oldPosDate = posFormat.format(Date1)

            binding.dateTxt.text = oldPosDate

            binding.orderInfoComNameTxt.text = ODRes?.vendor_info?.company_name

            binding.orderIdTxt.text ="#"+ODRes?.vendor_order_info?._id


            if(isFromCP){

                binding.qtyLnLy.visibility = View.GONE
                binding.qtyLnLyVW.visibility = View.GONE
                binding.invoiceLNLY.visibility = View.GONE
                binding.invoiceLNLYVW.visibility = View.GONE

                Utils.GlidewithLoadImageNew(this,ODRes.buyer_drop_signature?:"",binding.buyerImg)
                Utils.GlidewithLoadImageNew(this,ODRes.driver_drop_signature?:"",binding.driverImg)
                // Utils.GlidewithLoadImageNew(this,ODRes.royalty_pass_image_url,binding.royaltyImg)
                //  Utils.GlidewithLoadImageNew(this,ODRes.way_slip_image_url,binding.wayslipImg)
                Utils.GlidewithLoadImageNew(this,ODRes.challan_image_url?:"",binding.challanImg)


            }else{

                binding.qtyLnLy.visibility = View.VISIBLE
                binding.qtyLnLyVW.visibility = View.VISIBLE

                binding.orderQtyTxt.text = (ODRes?.pickup_quantity?:0.0).toInt().toString()+" Cu.Mtr"
                binding.pickupQtyTxt.text = (ODRes?.pickup_quantity?:0.0).toInt().toString()+" Cu.Mtr"
                binding.royaltyPassTxt.text = (ODRes?.royality_quantity?:0.0).toInt().toString()+" Cu.Mtr"

                // val diffrenceQty = ODRes?.quantity?.minus(ODRes?.pickup_quantity?:0.0)

                binding.remainingQtyTxt.text = (ODRes.remaining_quantity?:0.0).toInt().toString()+" Cu.Mtr"

                Utils.GlidewithLoadImageNew(this,ODRes.buyer_drop_signature?:"",binding.buyerImg)
                Utils.GlidewithLoadImageNew(this,ODRes.driver_drop_signature?:"",binding.driverImg)
                // Utils.GlidewithLoadImageNew(this,ODRes.royalty_pass_image_url,binding.royaltyImg)
                //  Utils.GlidewithLoadImageNew(this,ODRes.way_slip_image_url,binding.wayslipImg)
                Utils.GlidewithLoadImageNew(this,ODRes.challan_image_url?:"",binding.challanImg)
                //Utils.GlidewithLoadImageNew(this,ODRes.invoice_image_url?:"",binding.invoiceImg)


                if(ODRes?.invoice_url != null){

                    binding.invoiceLNLY.visibility = View.VISIBLE
                    binding.invoiceLNLYVW.visibility = View.VISIBLE

                    binding.invoiceLNLY.setOnClickListener {
                        startActivity(
                            PdfViewerActivity.launchPdfFromUrl(
                                this@OrderDeliverReciptActivity, ODRes?.invoice_url,
                                "Invoice", "dir",false
                            )
                        )
                    }
                }else{
                    binding.invoiceLNLY.visibility = View.GONE
                    binding.invoiceLNLYVW.visibility = View.GONE
                }
            }

/*
            binding.royaltyImg.setOnClickListener {
                val intent = Intent(this, ImageDetailViewActivity::class.java)
                intent.putExtra("profileUrl",ODRes.royalty_pass_image_url)
                startActivity(intent)
                Bungee.slideLeft(this)
            }

            binding.wayslipImg.setOnClickListener {
                val intent = Intent(this, ImageDetailViewActivity::class.java)
                intent.putExtra("profileUrl", ODRes.way_slip_image_url)
                startActivity(intent)
                Bungee.slideLeft(this)
            }*/


            binding.challanImg.setOnClickListener {
                val intent = Intent(this, ImageDetailViewActivity::class.java)
                intent.putExtra("profileUrl", ODRes.challan_image_url)
                startActivity(intent)
                Bungee.slideLeft(this)
            }

            binding.invoiceImg.setOnClickListener {
                val intent = Intent(this, ImageDetailViewActivity::class.java)
                intent.putExtra("profileUrl", ODRes.invoice_image_url)
                startActivity(intent)
                Bungee.slideLeft(this)
            }
        }
    }

    override fun onBackPressed() {
        // doneLabel?.performClick()
        binding.navBackFollowersList.performClick()
    }

    private fun showLoader() {
        binding.ivLoadingEdit.visibility = View.VISIBLE
        binding.ivLoadingEdit.smoothToShow()
    }

    private fun hideLoader() {
        binding.ivLoadingEdit.visibility = View.GONE
        binding.ivLoadingEdit.smoothToHide()
    }

}