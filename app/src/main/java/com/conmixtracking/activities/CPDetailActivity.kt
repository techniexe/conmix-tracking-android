package com.conmixtracking.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.conmixtracking.ConmixTrackingApp
import com.conmixtracking.R
import com.conmixtracking.data.CPListObjRes
import com.conmixtracking.data.TMListObjRes
import com.conmixtracking.databinding.CpDetailPageBinding
import com.conmixtracking.databinding.TruckDetailPageBinding
import com.conmixtracking.utils.SharedPrefUtils
import com.conmixtracking.utils.Utils
import com.facebook.drawee.backends.pipeline.Fresco
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 27,July,2021
 */
class CPDetailActivity:AppCompatActivity() {
    private lateinit var binding: CpDetailPageBinding
    var id:String?= null
    var svdata: CPListObjRes?= null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Fresco.initialize(this)
        binding = CpDetailPageBinding.inflate(layoutInflater)
        setContentView(binding.root)

        id = intent?.getStringExtra("id")?:""
        svdata = intent?.getParcelableExtra("cpObj")

        SharedPrefUtils.getInstance(this).storeStringInSharedPref(SharedPrefUtils.CP_ID,svdata?._id)

        ConmixTrackingApp.instance?.globalVehicle = svdata?._id


        Utils.setImageUsingGlide(this@CPDetailActivity, svdata?.concrete_pump_image_url ?: "", binding.vehicleImg)

        binding.truckRangeTxt.text = svdata?.concrete_pump_category?.category_name

        binding.cpSerialNumTxt.text = svdata?.serial_number?:""


        binding.cpSubcapacityTxt.text = (svdata?.concrete_pump_capacity?:0.0).toInt().toString() + " Cu.Mtr"

        binding.menufectureTxt.text = svdata?.company_name

        binding.manufecturerModelTxt.text = svdata?.manufacture_year?.toString()

        binding.vehicleModelTxt.text = svdata?.concrete_pump_model?.toString()

        binding.opratorNameTxt.text = svdata?.operator_info?.operator_name

        binding.opratorcontactnomTxt.text = svdata?.operator_info?.operator_mobile_number



        binding.opratorcontactnomTxt?.setOnClickListener{
            val callIntent = Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + Uri.encode(svdata?.operator_info?.operator_mobile_number?:"")))
            callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(callIntent)
        }



        binding.submitBtn.setOnClickListener {


            val intent = Intent(this@CPDetailActivity, OrderListActivity::class.java)
            intent.putExtra("vehicleID",svdata?._id)
            intent.putExtra("isFromCP",true)
            startActivity(intent)
            Bungee.slideLeft(this)

        }

        binding.navBack?.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }

    }

    override fun onBackPressed() {
        binding.navBack?.performClick()
    }
}