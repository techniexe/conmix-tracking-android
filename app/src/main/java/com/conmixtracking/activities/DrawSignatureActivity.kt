package com.conmixtracking.activities

import android.Manifest
import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.conmixtracking.ConmixTrackingApp
import com.conmixtracking.R
import com.conmixtracking.ViewModel.OrderViewModel
import com.conmixtracking.customview.URIPathHelper
import com.conmixtracking.data.OrderDetailObj
import com.conmixtracking.data.UploadTmInvoiceObj

import com.conmixtracking.databinding.DrawSignCreateBinding
import com.conmixtracking.services.api.ApiClient
import com.conmixtracking.services.api.Status
import com.conmixtracking.utils.Utils
import com.williamww.silkysignature.views.SignaturePad

import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody

import spencerstudios.com.bungeelib.Bungee
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.util.*


/**
 * Created by Hitesh Patel on 10,May,2021
 */
class DrawSignatureActivity : AppCompatActivity() {
    private lateinit var binding: DrawSignCreateBinding
    private lateinit var orderViewModel: OrderViewModel


    private val REQUEST_CODE_IMAGE = 100
    private val REQUEST_CODE_PERMISSIONS = 101

    private val KEY_PERMISSIONS_REQUEST_COUNT = "KEY_PERMISSIONS_REQUEST_COUNT"
    private val MAX_NUMBER_REQUEST_PERMISSIONS = 2

    private val permissions = Arrays.asList(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )

    private var permissionRequestCount: Int = 0

    var order_id: String? = null
    var ODRes: OrderDetailObj? = null
    private var vehicleId: String? = null

    var isSignedSuplier = false
    var isSigneddriver = false
    var isFromCP: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DrawSignCreateBinding.inflate(layoutInflater)

        setContentView(binding.root)
        setupViewModel()

        order_id = intent.getStringExtra("order_id")
        ODRes = intent.getParcelableExtra("ODRes")
        isFromCP = intent.getBooleanExtra("isFromCP", false)
        //  vehicleId = SharedPrefrence.getTMID(this)

        vehicleId = ConmixTrackingApp.instance?.globalVehicle

        binding.supplierNameTxt.text = " : " + ODRes?.vendor_info?.full_name

        if (isFromCP) {
            binding.driverNameFix.text = getString(R.string.operator_name_txt)
            binding.driverNameTxt.text = " : " + ODRes?.operator_info?.operator_name
            binding.hintDriverTxt?.text = getString(R.string.draw_operator_sign_hint_txt)
        } else {
            binding.driverNameFix.text = getString(R.string.driver_name_txt_fix)
            binding.driverNameTxt.text = " : " + ODRes?.driver1_info?.driver_name
            binding.hintDriverTxt?.text = getString(R.string.draw_driver_sign_hint_txt)
        }


        // Make sure the app has correct permissions to run
        requestPermissionsIfNecessary()

        binding.suplierSignaturePad.setOnSignedListener(object : SignaturePad.OnSignedListener {
            override fun onStartSigning() {
                binding.hintSupplierTxt.visibility = View.GONE
                binding.suplierSignatureCancel.visibility = View.VISIBLE

                isSignedSuplier = true

                if (isSigneddriver) {
                    binding.doneTxt.visibility = View.VISIBLE
                }
            }

            override fun onClear() {


                if (isFromCP) {
                    isSignedSuplier = true
                    binding.hintSupplierTxt.visibility = View.VISIBLE
                    binding.suplierSignatureCancel.visibility = View.GONE
                    if (isSigneddriver) {
                        binding.doneTxt.visibility = View.VISIBLE
                    }
                } else {
                    isSignedSuplier = false
                    binding.hintSupplierTxt.visibility = View.VISIBLE
                    binding.suplierSignatureCancel.visibility = View.GONE
                    binding.doneTxt.visibility = View.GONE
                }

            }

            override fun onSigned() {

            }

        })

        binding.driverSignaturePad.setOnSignedListener(object : SignaturePad.OnSignedListener {
            override fun onStartSigning() {
                binding.hintDriverTxt.visibility = View.GONE
                binding.driverSignatureCancel.visibility = View.VISIBLE


                if (isFromCP) {
                    binding.doneTxt.visibility = View.VISIBLE
                } else {
                    isSigneddriver = true

                    if (isSignedSuplier) {
                        binding.doneTxt.visibility = View.VISIBLE
                    }
                }

            }

            override fun onClear() {
                isSigneddriver = false
                binding.hintDriverTxt.visibility = View.VISIBLE
                binding.driverSignatureCancel.visibility = View.GONE
                binding.doneTxt.visibility = View.GONE
            }

            override fun onSigned() {

            }

        })

        binding.driverSignatureCancel.setOnClickListener {
            binding.driverSignaturePad.clear()
        }

        binding.suplierSignatureCancel.setOnClickListener {
            binding.suplierSignaturePad.clear()
        }


        binding.doneTxt.setOnClickListener {
            val suppliersignatureBitmap: Bitmap = binding.suplierSignaturePad.getSignatureBitmap()
            val suppliersignatureFile = addJpgSignatureToGallery(suppliersignatureBitmap)

            val driverSignatureBitmap: Bitmap = binding.driverSignaturePad.getSignatureBitmap()
            val driverSignatureFile = addJpgSignatureToGallery(driverSignatureBitmap)


            if (isFromCP) {
                if (driverSignatureFile?.exists() == true) {

                    binding.uploadImage.visibility = View.VISIBLE
                    binding.uploadImage.start()

                    uploadDocumentCPfun(suppliersignatureFile, driverSignatureFile)

                    binding.doneTxt.visibility = View.GONE
                } else {
                    Toast.makeText(
                        this@DrawSignatureActivity,
                        "Unable to store the signature",
                        Toast.LENGTH_SHORT
                    ).show()
                    binding.doneTxt.visibility = View.VISIBLE
                }
            } else {
                if (suppliersignatureFile?.exists() == true && driverSignatureFile?.exists() == true) {

                    binding.uploadImage.visibility = View.VISIBLE
                    binding.uploadImage.start()

                    uploadDocumentfun(suppliersignatureFile, driverSignatureFile)

                    binding.doneTxt.visibility = View.GONE
                } else {
                    Toast.makeText(
                        this@DrawSignatureActivity,
                        "Unable to store the signature",
                        Toast.LENGTH_SHORT
                    ).show()
                    binding.doneTxt.visibility = View.VISIBLE
                }
            }


        }

        binding.navBackFollowersList.setOnClickListener {

            finish()
            Bungee.slideRight(this)

        }
    }

    private fun setupViewModel() {

        orderViewModel = ViewModelProvider(
            this,
            OrderViewModel.ViewModelFactory(ApiClient().apiService)
        )
            .get(OrderViewModel::class.java)


    }

    override fun onBackPressed() {

        binding.navBackFollowersList.performClick()
    }

    @SuppressLint("DefaultLocale")
    fun addJpgSignatureToGallery(signature: Bitmap?): File? {

        try {

            val pathvalue = saveToGallery(this, signature!!, "SignaturePad")

            //Log.v("log_tag", "path_v" + File(pathvalue).absolutePath)

            // val file = File(pathvalue)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                val uriPathHelper = URIPathHelper()
                val filePath = uriPathHelper.getPath(this, Uri.parse(pathvalue))
                return File(filePath)
            } else {
                val file = File(pathvalue)
                return file
            }


        } catch (e: IOException) {
            e.printStackTrace()
        }
        return null
    }


    fun saveToGallery(context: Context, bitmap: Bitmap, albumName: String): String? {
        val filename = "${System.currentTimeMillis()}.jpg"
        val write: (OutputStream) -> Boolean = {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, it)
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val contentValues = ContentValues().apply {
                put(MediaStore.MediaColumns.DISPLAY_NAME, filename)
                put(MediaStore.MediaColumns.MIME_TYPE, "image/jpg")
                put(
                    MediaStore.MediaColumns.RELATIVE_PATH,
                    "${Environment.DIRECTORY_DCIM}/$albumName"
                )
            }

            context.contentResolver.let {
                it.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)?.let { uri ->
                    it.openOutputStream(uri)?.let(write)
                    return uri.toString()
                }
            }
        } else {
            val imagesDir =
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM)
                    .toString() + File.separator + albumName
            val file = File(imagesDir)
            if (!file.exists()) {
                file.mkdir()
            }
            val image = File(imagesDir, filename)
            write(FileOutputStream(image))

            return image.toString()
        }
        return null
    }


    /**
     * Request permissions twice - if the user denies twice then show a toast about how to update
     * the permission for storage. Also disable the button if we don't have access to pictures on
     * the device.
     */
    private fun requestPermissionsIfNecessary() {
        if (!checkAllPermissions()) {
            if (permissionRequestCount < MAX_NUMBER_REQUEST_PERMISSIONS) {
                permissionRequestCount += 1
                ActivityCompat.requestPermissions(
                    this,
                    permissions.toTypedArray(),
                    REQUEST_CODE_PERMISSIONS
                )
            } else {
                Toast.makeText(
                    this,
                    R.string.set_permissions_in_settings,
                    Toast.LENGTH_LONG
                ).show()
                binding.doneTxt.isEnabled = false
            }
        }
    }


    /** Permission Checking  */
    private fun checkAllPermissions(): Boolean {
        var hasPermissions = true
        for (permission in permissions) {
            hasPermissions = hasPermissions and isPermissionGranted(permission)
        }
        return hasPermissions
    }

    private fun isPermissionGranted(permission: String) =
        ContextCompat.checkSelfPermission(this, permission) ==
                PackageManager.PERMISSION_GRANTED

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            requestPermissionsIfNecessary() // no-op if permissions are granted already.
        }
    }


    fun uploadDocumentfun(supplierFile: File, driverFile: File) {


        val supplierrequestFile = RequestBody.create("image/jpeg".toMediaTypeOrNull(), supplierFile)


        // MultipartBody.Part is used to send also the actual file name
        val supplierbody = MultipartBody.Part.createFormData(
            "supplier_pickup_signature",
            supplierFile.name,
            supplierrequestFile
        )


        val driverRequestFile = RequestBody.create("image/jpeg".toMediaTypeOrNull(), driverFile)

        // MultipartBody.Part is used to send also the actual file name
        val driverbody = MultipartBody.Part.createFormData(
            "driver_pickup_signature",
            driverFile.name,
            driverRequestFile
        )




        ApiClient.setToken()

        orderViewModel.uploadSignSupplierOrder(
            ODRes?._id,
            vehicleId,
            ODRes?.vendor_order_info?._id,
            supplierbody,
            driverbody,
            null,
            null
        ).observe(this, androidx.lifecycle.Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {

                        if (resource.data?.isSuccessful!!) {
                            resource.data.let { response ->
                                if (response?.code() == 202) {

                                    Utils.showToast(
                                        this@DrawSignatureActivity,
                                        getString(R.string.sucessfully_upload_sign),
                                        Toast.LENGTH_SHORT
                                    )


                                    getUpdateOrderIdForPickup(ODRes?._id ?: "")

                                }
                            }
                        } else {
                            binding.uploadImage.visibility = View.GONE
                            binding.uploadImage.stop()
                            binding.doneTxt.visibility = View.VISIBLE


                            Utils.setErrorData(this, resource.data.errorBody())
                        }

                    }
                    Status.ERROR -> {
                        binding.uploadImage.visibility = View.GONE
                        binding.uploadImage.stop()
                        binding.doneTxt.visibility = View.VISIBLE
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {

                    }
                }
            }
        })
    }

    fun uploadDocumentCPfun(supplierFile: File?, driverFile: File) {


        val supplierrequestFile =
            supplierFile?.let { RequestBody.create("image/jpeg".toMediaTypeOrNull(), it) }


        // MultipartBody.Part is used to send also the actual file name
        val supplierbody = supplierrequestFile?.let {
            MultipartBody.Part.createFormData(
                "supplier_pickup_signature", supplierFile?.name,
                it
            )
        }


        val driverRequestFile =
            driverFile?.let { RequestBody.create("image/jpeg".toMediaTypeOrNull(), it) }

        // MultipartBody.Part is used to send also the actual file name
        val driverbody = driverRequestFile?.let {
            MultipartBody.Part.createFormData(
                "driver_pickup_signature", driverFile?.name,
                it
            )
        }

        ApiClient.setToken()

        orderViewModel.uploadSignSupplierCPOrder(
            ODRes?._id,
            vehicleId,
            ODRes?.vendor_order_info?._id,
            supplierbody,
            driverbody,
            null,
            null
        ).observe(this, androidx.lifecycle.Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {

                        if (resource.data?.isSuccessful!!) {
                            resource.data.let { response ->
                                if (response?.code() == 202) {
                                    binding.uploadImage.visibility = View.GONE
                                    binding.uploadImage.stop()
                                    binding.doneTxt.visibility = View.GONE
                                    Utils.showToast(
                                        this@DrawSignatureActivity,
                                        getString(R.string.sucessfully_upload_sign),
                                        Toast.LENGTH_SHORT
                                    )


                                    getUpdateOrderIdCPForPickup(ODRes?._id ?: "")

                                }
                            }
                        } else {
                            binding.uploadImage.visibility = View.GONE
                            binding.uploadImage.stop()
                            binding.doneTxt.visibility = View.VISIBLE


                            Utils.setErrorData(this, resource.data.errorBody())
                        }

                    }
                    Status.ERROR -> {
                        binding.uploadImage.visibility = View.GONE
                        binding.uploadImage.stop()
                        binding.doneTxt.visibility = View.VISIBLE
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {

                    }
                }
            }
        })
    }


    fun getUpdateOrderIdForPickup(orderId: String) {
        ApiClient.setToken()

        orderViewModel.confirmPickupOrder(ODRes?._id, vehicleId, ODRes?.vendor_order_info?._id)
            .observe(this, androidx.lifecycle.Observer {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {

                            if (resource.data?.isSuccessful!!) {
                                resource.data.let { response ->
                                    if (response?.code() == 202) {
                                        Utils.showToast(
                                            this@DrawSignatureActivity,
                                            getString(R.string.change_order_status_pickup),
                                            Toast.LENGTH_SHORT
                                        )
                                        /*val intent = Intent(this@DrawSignatureActivity, LoadOrderDetailsActivity::class.java)
                                        intent.putExtra("ODRes",ODRes)
                                        intent.putExtra("isFromCP",false)
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                        startActivity(intent)
                                        Bungee.slideLeft(this@DrawSignatureActivity)
                                        finish()*/

                                        uploadTmInvoice(false)

                                    }
                                }
                            } else {

                                binding.uploadImage.visibility = View.GONE
                                binding.uploadImage.stop()
                                binding.doneTxt.visibility = View.VISIBLE
                                Utils.setErrorData(this, resource.data.errorBody())
                            }

                        }
                        Status.ERROR -> {
                            binding.uploadImage.visibility = View.GONE
                            binding.uploadImage.stop()
                            binding.doneTxt.visibility = View.VISIBLE
                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                        }
                        Status.LOADING -> {

                        }
                    }
                }
            })
    }

    fun getUpdateOrderIdCPForPickup(orderId: String) {
        ApiClient.setToken()

        orderViewModel.confirmPickupCPOrder(ODRes?._id, vehicleId, ODRes?.vendor_order_info?._id)
            .observe(this, androidx.lifecycle.Observer {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {

                            if (resource.data?.isSuccessful!!) {
                                resource.data.let { response ->
                                    if (response?.code() == 202) {
                                        Utils.showToast(
                                            this@DrawSignatureActivity,
                                            getString(R.string.change_order_status_pickup),
                                            Toast.LENGTH_SHORT
                                        )
                                        val intent = Intent(
                                            this@DrawSignatureActivity,
                                            LoadOrderDetailsActivity::class.java
                                        )
                                        intent.putExtra("ODRes", ODRes)
                                        intent.putExtra("isFromCP", isFromCP)
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                        startActivity(intent)
                                        Bungee.slideLeft(this@DrawSignatureActivity)
                                        finish()
                                    }
                                }
                            } else {
                                Utils.setErrorData(this, resource.data.errorBody())
                            }

                        }
                        Status.ERROR -> {

                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                        }
                        Status.LOADING -> {

                        }
                    }
                }
            })
    }


    private fun uploadTmInvoice(isFromCP: Boolean) {
        val upldtmInvcObj = UploadTmInvoiceObj(ODRes?._id, ODRes?.vendor_order_info?._id, vehicleId)
        ApiClient.setToken()

        orderViewModel.uploadTmInvoice(upldtmInvcObj)
            .observe(this, androidx.lifecycle.Observer {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {

                            if (resource.data?.isSuccessful!!) {
                                resource.data.let { response ->

                                    if (response.code() == 200) {

                                       /* binding.uploadImage.visibility = View.GONE
                                        binding.uploadImage.stop()
                                        binding.doneTxt.visibility = View.GONE
                                        val intent = Intent(
                                            this@DrawSignatureActivity,
                                            LoadOrderDetailsActivity::class.java
                                        )
                                        intent.putExtra("ODRes", ODRes)
                                        intent.putExtra("isFromCP", isFromCP)
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                        startActivity(intent)
                                        Bungee.slideLeft(this@DrawSignatureActivity)
                                        finish()*/


                                        uploadVendorInvoice(isFromCP)



                                    } else if (response.code() == 400) {
                                        binding.uploadImage.visibility = View.GONE
                                        binding.uploadImage.stop()
                                        binding.doneTxt.visibility = View.VISIBLE
                                        Utils.showToast(
                                            this,
                                            it.message.toString(),
                                            Toast.LENGTH_LONG
                                        )
                                    }
                                }
                            } else {
                                binding.uploadImage.visibility = View.GONE
                                binding.uploadImage.stop()
                                binding.doneTxt.visibility = View.VISIBLE

                                Utils.setErrorData(this, resource.data.errorBody())
                            }

                        }
                        Status.ERROR -> {
                            binding.uploadImage.visibility = View.GONE
                            binding.uploadImage.stop()
                            binding.doneTxt.visibility = View.VISIBLE
                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)

                        }
                        Status.LOADING -> {

                        }
                    }
                }
            })
    }

    private fun uploadVendorInvoice(isFromCP: Boolean) {
        val upldtmInvcObj = UploadTmInvoiceObj(ODRes?._id, ODRes?.vendor_order_info?._id, vehicleId)
        ApiClient.setToken()

        orderViewModel.uploadVendorInvoice(upldtmInvcObj)
            .observe(this, androidx.lifecycle.Observer {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {

                            if (resource.data?.isSuccessful!!) {
                                resource.data.let { response ->

                                    if (response.code() == 200) {

                                        binding.uploadImage.visibility = View.GONE
                                        binding.uploadImage.stop()
                                        binding.doneTxt.visibility = View.GONE
                                        val intent = Intent(
                                            this@DrawSignatureActivity,
                                            LoadOrderDetailsActivity::class.java
                                        )
                                        intent.putExtra("ODRes", ODRes)
                                        intent.putExtra("isFromCP", isFromCP)
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                        startActivity(intent)
                                        Bungee.slideLeft(this@DrawSignatureActivity)
                                        finish()


                                    } else if (response.code() == 400) {
                                        binding.uploadImage.visibility = View.GONE
                                        binding.uploadImage.stop()
                                        binding.doneTxt.visibility = View.VISIBLE
                                        Utils.showToast(
                                            this,
                                            it.message.toString(),
                                            Toast.LENGTH_LONG
                                        )
                                    }
                                }
                            } else {
                                binding.uploadImage.visibility = View.GONE
                                binding.uploadImage.stop()
                                binding.doneTxt.visibility = View.VISIBLE

                                Utils.setErrorData(this, resource.data.errorBody())
                            }

                        }
                        Status.ERROR -> {
                            binding.uploadImage.visibility = View.GONE
                            binding.uploadImage.stop()
                            binding.doneTxt.visibility = View.VISIBLE
                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)

                        }
                        Status.LOADING -> {

                        }
                    }
                }
            })
    }

}