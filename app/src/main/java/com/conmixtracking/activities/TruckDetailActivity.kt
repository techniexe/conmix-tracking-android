package com.conmixtracking.activities

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.conmixtracking.ConmixTrackingApp
import com.conmixtracking.R
import com.conmixtracking.data.TMListObjRes
import com.conmixtracking.databinding.TruckDetailPageBinding
import com.conmixtracking.utils.PermissionUtils
import com.conmixtracking.utils.SharedPrefUtils
import com.conmixtracking.utils.Utils
import com.facebook.drawee.backends.pipeline.Fresco
import com.google.android.gms.location.*
import spencerstudios.com.bungeelib.Bungee
import java.util.*


/**
 * Created by Hitesh Patel on 10,May,2021
 */
class TruckDetailActivity:AppCompatActivity() {
    private lateinit var binding: TruckDetailPageBinding
    var id:String?= null
    var svdata: TMListObjRes?= null
    var address: String? = null
    private  var locationRequest: LocationRequest?= null
    private  var fusedLocationClient: FusedLocationProviderClient?= null
    private  var locationCallback: LocationCallback?= null
    var buttonClk:Boolean = false
    var curntLat:Double = 0.0
    var curntlong:Double = 0.0
    var curntAdd:String? = null
    var address1: String = ""
    var address2: String = ""

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 999
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Fresco.initialize(this)
        binding = TruckDetailPageBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getCurrentLocation()

        id = intent?.getStringExtra("id")?:""
        svdata = intent?.getParcelableExtra("vehicleObj")

        SharedPrefUtils.getInstance(this).storeStringInSharedPref(SharedPrefUtils.TM_ID,svdata?._id)

        ConmixTrackingApp.instance?.globalVehicle = svdata?._id


        Utils.setImageUsingGlide(this@TruckDetailActivity, svdata?.TM_image_url ?: "", binding.vehicleImg)

        binding.truckRangeTxt.text = getString(R.string.vehicle_list_fix_title)
        /*binding.capacityTextView.text = Utils.setMTTextview(svdata?.vehicle_sub_category?.sub_category_name,
            svdata?.vehicle_sub_category?.min_load_capacity,svdata?.vehicle_sub_category?.max_load_capacity)*/

        binding.capacityTextView.text =svdata?.TM_category?.category_name

        binding.vehicleNumberTxt.text = svdata?.TM_rc_number


        binding.vehicleSubcategoryTxt.text = svdata?.TM_sub_category?.sub_category_name

        binding.menufectureTxt.text = svdata?.manufacturer_name

        binding.manufecturerModelTxt.text = svdata?.manufacture_year?.toString()

        binding.vehicleModelTxt.text = svdata?.TM_model?.toString()

        binding.driverName1Txt.text = svdata?.driver1_info?.driver_name

        binding.driverContactnom1Txt.text = svdata?.driver1_info?.driver_mobile_number


        binding.driverContactnom1Txt?.setOnClickListener{
            val callIntent = Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + Uri.encode(svdata?.driver1_info?.driver_mobile_number?:"")))
            callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(callIntent)
        }

        binding.driverContactnom2Txt?.setOnClickListener{
            val callIntent = Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + Uri.encode(svdata?.driver2_info?.driver_mobile_number?:"")))
            callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(callIntent)
        }


        if(svdata?.driver2_info == null ){

            binding.driver2cntlnly.visibility = View.GONE
            binding.driver2view.visibility = View.GONE
            binding.driver2lnly.visibility = View.GONE
            binding.driver2lnlyView.visibility = View.GONE


        }else{

            binding.driver2lnly.visibility = View.VISIBLE
            binding.driver2lnlyView.visibility = View.VISIBLE
            binding.driver2cntlnly.visibility = View.VISIBLE
            binding.driver2view.visibility = View.VISIBLE
            binding.driverName2Txt.text= svdata?.driver2_info?.driver_name
            binding.driverContactnom2Txt.text = svdata?.driver2_info?.driver_mobile_number

        }


        binding.garageLocationTxt.text =  Utils.formatedAddresss(svdata?.line1,
            svdata?.line2,svdata?.city_name,svdata?.state_name,
            svdata?.pincode)


        binding.submitBtn.setOnClickListener {
            /*  val intent = Intent(this, OrderDetailActivity::class.java)
              startActivity(intent)*/

            /* val fragment: BreakAlertDialog = BreakAlertDialog(this@TruckDetailActivity, getString(R.string.pickup_alert_txt), "YES", "NO")
               fragment.show(supportFragmentManager, "alert")
               fragment.isCancelable = false
               fragment.setDialogListener(this)*/

            val intent = Intent(this@TruckDetailActivity, OrderListActivity::class.java)
            intent.putExtra("vehicleID",svdata?._id)
            intent.putExtra("isFromCP",false)
            startActivity(intent)
            Bungee.slideLeft(this)

        }

        binding.navBackFollowersList?.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }


        binding.garageLocationLnly?.setOnClickListener {

            val intent = Intent(this, MapMarkerActivity::class.java)
            intent.putExtra("latitude", svdata?.pickup_location?.coordinates?.get(1) ?:0.0)
            intent.putExtra("longitude",svdata?.pickup_location?.coordinates?.get(0) ?:0.0)
            intent.putExtra("address",address)
            startActivity(intent)
            Bungee.slideLeft(this)

            /* val gmmIntentUri: Uri =
                 Uri.parse("google.navigation:q=23.0810,72.5768&avoid=tf")
             val mapIntent =
                 Intent(Intent.ACTION_VIEW, gmmIntentUri)
             mapIntent.setPackage("com.google.android.apps.maps")
             startActivity(mapIntent)*/

        }

    }

    override fun onBackPressed() {
        binding.navBackFollowersList?.performClick()
    }

    fun getCurrentLocation() {

        startAnim()

        buttonClk = true
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            when {
                PermissionUtils.isAccessFineLocationGranted(this) -> {
                    when {
                        PermissionUtils.isLocationEnabled(this) -> {
                            startAnim()
                            setUpLocationListener()
                        }
                        else -> {
                            PermissionUtils.showGPSNotEnabledDialog(this)
                        }
                    }
                }
                else -> {
                    PermissionUtils.requestAccessFineLocationPermission(
                        this,
                        LOCATION_PERMISSION_REQUEST_CODE
                    )
                }
            }
            //setUpLocationListener()
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {

                return
            }
            fusedLocationClient?.lastLocation?.addOnSuccessListener { location: Location? ->
                useLocation(location)
            }
        }
    }

    private fun setUpLocationListener() {


        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            stopAnim()
            return
        }

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        locationRequest = LocationRequest.create()
        locationRequest?.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest?.interval = 2 * 1000
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                LocationServices.getFusedLocationProviderClient(this@TruckDetailActivity)
                    .removeLocationUpdates(this)
                if (locationResult.locations.size > 0) {


                    if (Utils.checkConnectivity(this@TruckDetailActivity)) {
                        useLocation(locationResult.lastLocation)
                    } else {

                        Utils.showToast(
                            this@TruckDetailActivity,
                            getString(R.string.no_internet_msg),
                            Toast.LENGTH_SHORT
                        )

                    }
                }
            }
        }

        /* fusedLocationClient?.requestLocationUpdates(
             locationRequest,
             object : LocationCallback() {
                 override fun onLocationResult(locationResult: LocationResult) {
                     super.onLocationResult(locationResult)
                     LocationServices.getFusedLocationProviderClient(this@UseLocationActivity)
                         .removeLocationUpdates(this)
                     if (locationResult.locations.size > 0) {
                         useLocation(locationResult.lastLocation)
                     }
                     // Few more things we can do here:
                     // For example: Update the location of user on server
                 }
             },*/
        fusedLocationClient?.requestLocationUpdates(locationRequest,locationCallback, Looper.getMainLooper())

    }

    fun useLocation(location : Location?){

        if (Utils.checkConnectivity(this@TruckDetailActivity)) {
            if (location != null) {
                val gcd = Geocoder(this, Locale.getDefault())
                val addresses = gcd.getFromLocation(location!!.latitude, location!!.longitude, 1)
                if (addresses.size > 0) {
                    val address = addresses[0]

                    generateFinalAddress(address.getAddressLine(0))

                    /*  val state = "5b27adcdba933e1f06a78560"//addresses[0].adminArea
                      search(cityName, state)*/


                    /* SharedPrefUtils.getInstance(this@TMListActivity)
                         .storeStringInSharedPref(
                             SharedPrefUtils.CITY_NAME,
                             addresses[0].locality
                         )
                     SharedPrefUtils.getInstance(this@TMListActivity)
                         .storeStringInSharedPref(SharedPrefUtils.LATITUDE,addresses[0].latitude.toString())

                     SharedPrefUtils.getInstance(this@TMListActivity)
                         .storeStringInSharedPref(SharedPrefUtils.LONGITUDE,addresses[0].longitude.toString())*/

                    binding.currentlatview.visibility = View.VISIBLE
                    binding.currentLocationLnly.visibility = View.VISIBLE
                    curntLat = address.latitude
                    curntlong = address.longitude

                    val sb = StringBuilder()
                    /*for (i in 0 until address.getMaxAddressLineIndex()) {
                        sb.append(address.getAddressLine(i)).append("\n")
                    }*/
                    //sb.append(address1).append(",")
                    sb.append(address2).append(",\n")
                    sb.append(address.locality).append(",\n")
                    sb.append(address.adminArea).append(",")
                    sb.append(address.countryName).append("-")
                    sb.append(address.postalCode)
                    val result = sb.toString()
                    curntAdd = result

                    binding.currentLocationTxt.text = result

                    binding.currentAddLnLy.setOnClickListener {
                        val intent = Intent(this, MapMarkerActivity::class.java)
                        intent.putExtra("latitude", curntLat?:0.0)
                        intent.putExtra("longitude",curntlong?:0.0)
                        intent.putExtra("address",curntAdd)
                        startActivity(intent)
                        Bungee.slideLeft(this)
                    }

                    stopAnim()
                    /*val intent = Intent(this@TruckDetailActivity, MainActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                    finish()
                    Bungee.slideRight(this)*/

                } else {
                    startAnim()
                    binding.currentlatview.visibility = View.GONE
                    binding.currentLocationLnly.visibility = View.GONE
                    //llProgressBar.visibility = View.GONE
                    Utils.showToast(this,"Enable to fetch your location, Please turn on your location",
                        Toast.LENGTH_SHORT)



                }
            }else{
                binding.currentlatview.visibility = View.GONE
                binding.currentLocationLnly.visibility = View.GONE
                Utils.showToast(this,"Enable to fetch your location,Try again", Toast.LENGTH_SHORT)
            }
        } else {

            Utils.showToast(
                this@TruckDetailActivity,
                getString(R.string.no_internet_msg),
                Toast.LENGTH_SHORT
            )

        }


    }


    private fun generateFinalAddress(
        address: String
    ) {
        val s = address.split(",")
        address1 = ""
        address2 = ""

        var subString = s.subList(0, s.size - 3)
        if (subString.size == 1) {
            address1 = subString[0]
            address2 = subString[0]
        }

        if (subString.size == 2) {
            address1 = subString[0]
            address2 = subString[1]
        }

        if (subString.size == 3) {

            address1 = subString[0] + subString[1]
            address2 = subString[2]
        }
        if (subString.size > 3) {

            for ((index, value) in subString.withIndex()) {
                if (index < 2) {
                    address1 += value
                } else {
                    address2 += value
                }
            }
        }


    }

    override fun onResume() {
        super.onResume()

        if(buttonClk){
            when {
                PermissionUtils.isAccessFineLocationGranted(this) -> {
                    when {
                        PermissionUtils.isLocationEnabled(this) -> {
                            //startAnim()
                            setUpLocationListener()
                        }
                        else -> {
                            // PermissionUtils.showGPSNotEnabledDialog(this)
                        }
                    }

                }
                else -> {
                    stopAnim()
                    binding.currentlatview.visibility = View.GONE

                    Utils.showToast(this,"Enable to fetch your location,Try again", Toast.LENGTH_SHORT)


                }
            }
        }
    }

    override fun onPause() {
        super.onPause()
        //fusedLocationClient?.removeLocationUpdates(locationCallback)

        if(fusedLocationClient != null && locationCallback != null){
            fusedLocationClient?.removeLocationUpdates(locationCallback)
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            LOCATION_PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    when {
                        PermissionUtils.isLocationEnabled(this) -> {
                            setUpLocationListener()
                        }
                        else -> {
                            PermissionUtils.showGPSNotEnabledDialog(this)

                        }
                    }
                } else {

                    val newAlrt =  AlertDialog.Builder(this)
                        .setTitle(this.getString(R.string.get_location))
                        .setMessage(this.getString(R.string.rational_location_permission))
                        .setCancelable(false)
                        .setPositiveButton(this.getString(R.string.ok)) { d, _ ->
                            val intent: Intent = Intent(
                                Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", packageName, null)
                            )
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            startActivity(intent)
                            d.cancel()
                        }
                    newAlrt.show()

                    // Utils().showToast(this,getString(R.string.location_permission_not_granted),100)
                }
            }
        }
    }


    private fun startAnim() {
        if (binding.loaderView != null)
            binding.loaderView.visibility= View.VISIBLE
            binding.loaderView.smoothToShow()

    }

    private fun stopAnim() {
        if (binding.loaderView != null)
            binding.loaderView.smoothToHide()
            binding.loaderView.visibility= View.GONE


    }
}