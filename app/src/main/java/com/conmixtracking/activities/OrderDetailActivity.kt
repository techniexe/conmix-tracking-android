package com.conmixtracking.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.conmixtracking.ConmixTrackingApp
import com.conmixtracking.R
import com.conmixtracking.ViewModel.OrderViewModel
import com.conmixtracking.data.ErrorRes
import com.conmixtracking.data.OrderDetailObj
import com.conmixtracking.databinding.EmptyLayoutBinding
import com.conmixtracking.databinding.OrderDetailPageBinding
import com.conmixtracking.services.api.ApiClient
import com.conmixtracking.services.api.Status
import com.conmixtracking.utils.SharedPrefUtils
import com.conmixtracking.utils.Utils
import com.google.gson.Gson
import com.google.gson.internal.bind.util.ISO8601Utils
import com.google.gson.reflect.TypeToken
import spencerstudios.com.bungeelib.Bungee
import java.text.ParsePosition
import java.text.SimpleDateFormat


/**
 * Created by Hitesh Patel on 10,May,2021
 */
class OrderDetailActivity:AppCompatActivity() {
    private lateinit var binding: OrderDetailPageBinding
    private lateinit var bindingNoOrder: EmptyLayoutBinding
    var vehicleID:String?= null
    var ODRes: OrderDetailObj?= null
    var pickUpAddrees:String? = null
    var deliveryAddress:String? = null
    var ODResNew: OrderDetailObj?= null
    var tracking_Id:String? = null
    private lateinit var orderViewModel: OrderViewModel
    var isFromCP:Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = OrderDetailPageBinding.inflate(layoutInflater)
        bindingNoOrder = binding.emptyOrder
        setContentView(binding.root)
        isFromCP = intent?.getBooleanExtra("isFromCP",false)?:false

    //    vehicleID = intent?.getStringExtra("vehicleID")?:""
        vehicleID = ConmixTrackingApp.instance?.globalVehicle

        ODResNew = intent.getParcelableExtra("ODRes")
        setupViewModel()

        if(isFromCP){
            getOrderCPDetailData(ODResNew?.vendor_order_info?._id)
        }else{
            getOrderDetailData(ODResNew?.vendor_order_info?._id)
        }



        binding.navBackOrderDeatilBtn?.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }

        binding.submitBtn?.setOnClickListener {

            /*val intent = Intent(this@OrderDetailActivity, OrderChangeQtyActivity::class.java)
            intent.putExtra("orderDtl",ODRes)
            startActivity(intent)
            Bungee.slideLeft(this@OrderDetailActivity)*/
            // getUpdateOrderIdForPickup(ODRes?._id?:"")

            /*val intent = Intent(this@OrderDetailActivity, OrderChangeQtyActivity::class.java)
            intent.putExtra("orderDtl",ODRes)
            intent.putExtra("vehicle_Id",vehicleID)
            startActivity(intent)
            Bungee.slideLeft(this@OrderDetailActivity)*/


            /*if(isFromCP){

            }else{
                val intent = Intent(this@OrderDetailActivity, OrderInfoActivity::class.java)
                intent.putExtra("orderDtl",ODRes)
                intent.putExtra("vehicle_Id",vehicleID)
                startActivity(intent)
                Bungee.slideLeft(this@OrderDetailActivity)
            }*/

            if(isFromCP){
                val intent = Intent(this@OrderDetailActivity, UploadDocumentActivity::class.java)
                intent.putExtra("order_id",ODRes?.vendor_order_info?._id)
                intent.putExtra("vehicle_Id",vehicleID)
                intent.putExtra("ODRes",ODRes)
                intent.putExtra("isFromCP",isFromCP)
                startActivity(intent)
                Bungee.slideLeft(this@OrderDetailActivity)
            }else{
                getCPDeliverCheck(ODRes?.vendor_order_info?._id)
            }



            /**/


        }

        binding.pickUpLnly?.setOnClickListener {
            val intent = Intent(this, MapMarkerActivity::class.java)
            intent.putExtra("latitude", ODRes?.pickup_address_info?.pickup_location?.coordinates?.get(1) ?:0.0)
            intent.putExtra("longitude",ODRes?.pickup_address_info?.pickup_location?.coordinates?.get(0) ?:0.0)
            intent.putExtra("address",pickUpAddrees)
            startActivity(intent)
            Bungee.slideLeft(this)
        }

        binding.deliverLnly?.setOnClickListener {
            val intent = Intent(this, MapMarkerActivity::class.java)
            intent.putExtra("latitude", ODRes?.delivery_address_info?.delivery_location?.coordinates?.get(1) ?:0.0)
            intent.putExtra("longitude",ODRes?.delivery_address_info?.delivery_location?.coordinates?.get(0) ?:0.0)
            intent.putExtra("address",deliveryAddress)
            startActivity(intent)
            Bungee.slideLeft(this)
        }
    }

    private fun setupViewModel() {

        orderViewModel = ViewModelProvider(
            this,
            OrderViewModel.ViewModelFactory(ApiClient().apiService)
        )
            .get(OrderViewModel::class.java)


    }

    private fun getCPDeliverCheck(id:String?){
        showLoader()
        ApiClient.setToken()
        orderViewModel.getCPDeliverCheck(id)
            .observe(this, androidx.lifecycle.Observer {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            hideLoader()
                            if (resource.data?.isSuccessful!!) {
                                binding.cpErrortxt.visibility = View.GONE
                                resource.data.let { response ->
                                    if(response.code() == 202){
                                        val intent = Intent(this@OrderDetailActivity, UploadDocumentActivity::class.java)
                                        intent.putExtra("order_id",ODRes?.vendor_order_info?._id)
                                        intent.putExtra("vehicle_Id",vehicleID)
                                        intent.putExtra("ODRes",ODRes)
                                        intent.putExtra("isFromCP",isFromCP)
                                        startActivity(intent)
                                        Bungee.slideLeft(this@OrderDetailActivity)
                                    }
                                }
                            } else {
                                binding.cpErrortxt.visibility = View.VISIBLE

                                Utils.setErrorData(this, resource.data.errorBody())


                                val res = resource.data.errorBody()?.string().toString()
                                val gson = Gson()
                                val type = object : TypeToken<ErrorRes>() {}.type
                                val err = gson.fromJson<ErrorRes>(res, type)
                                val msg = err.error.message

                                binding.cpErrortxt.text = msg

                            }

                        }
                        Status.ERROR -> {
                            hideLoader()
                            binding.cpErrortxt.visibility = View.VISIBLE
                            binding.cpErrortxt.text =  it.message.toString()

                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)

                        }
                        Status.LOADING -> {

                        }

                    }
                }
            })
    }

    private fun getOrderDetailData(id:String?){
        showLoader()
        ApiClient.setToken()
        orderViewModel.getOrdeIdDetailData(id,ODResNew?._id)
            .observe(this, androidx.lifecycle.Observer {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            hideLoader()
                            if (resource.data?.isSuccessful!!) {
                                resource.data.let { response ->
                                    ODRes = response?.body()?.order
                                    hideLoader()

                                    if(ODRes != null){
                                        binding.orderDetailLnly?.visibility = View.VISIBLE
                                        bindingNoOrder.emptyFrame?.visibility = View.GONE

                                        SharedPrefUtils.getInstance(this@OrderDetailActivity).storeStringInSharedPref(SharedPrefUtils.ORDER_ID,ODRes?.vendor_order_info?._id)

                                        upDateUi(ODRes)
                                    }else{
                                        binding.orderDetailLnly?.visibility = View.GONE
                                        bindingNoOrder.emptyFrame?.visibility = View.VISIBLE
                                        if(isFromCP){
                                            bindingNoOrder.emptyText.text = getString(R.string.no_data_cp_empty_screen)
                                        }else{
                                            bindingNoOrder.emptyText.text = getString(R.string.no_data_empty_screen)
                                        }
                                    }
                                }
                            } else {

                                binding.orderDetailLnly?.visibility = View.GONE
                                bindingNoOrder.emptyFrame?.visibility = View.VISIBLE
                                if(isFromCP){
                                    bindingNoOrder.emptyText.text = getString(R.string.no_data_cp_empty_screen)
                                }else{
                                    bindingNoOrder.emptyText.text = getString(R.string.no_data_empty_screen)
                                }
                                Utils.setErrorData(this, resource.data.errorBody())
                            }

                        }
                        Status.ERROR -> {
                            hideLoader()
                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                            binding.orderDetailLnly?.visibility = View.GONE
                            bindingNoOrder.emptyFrame?.visibility = View.VISIBLE
                            if(isFromCP){
                                bindingNoOrder.emptyText.text = getString(R.string.no_data_cp_empty_screen)
                            }else{
                                bindingNoOrder.emptyText.text = getString(R.string.no_data_empty_screen)
                            }
                        }
                        Status.LOADING -> {

                        }

                    }
                }
            })
    }


    private fun getOrderCPDetailData(id:String?){
        showLoader()
        ApiClient.setToken()
        orderViewModel.getOrdeIdCPDetailData(id,ODResNew?._id)
            .observe(this, androidx.lifecycle.Observer {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            hideLoader()
                            if (resource.data?.isSuccessful!!) {
                                resource.data.let { response ->
                                    ODRes = response?.body()?.order
                                    hideLoader()

                                    if(ODRes != null){
                                        binding.orderDetailLnly?.visibility = View.VISIBLE
                                        bindingNoOrder.emptyFrame?.visibility = View.GONE

                                        SharedPrefUtils.getInstance(this@OrderDetailActivity).storeStringInSharedPref(SharedPrefUtils.ORDER_ID,ODRes?.vendor_order_info?._id)

                                        upDateUi(ODRes)
                                    }else{
                                        binding.orderDetailLnly?.visibility = View.GONE
                                        bindingNoOrder.emptyFrame?.visibility = View.VISIBLE
                                        if(isFromCP){
                                            bindingNoOrder.emptyText.text = getString(R.string.no_data_cp_empty_screen)
                                        }else{
                                            bindingNoOrder.emptyText.text = getString(R.string.no_data_empty_screen)
                                        }
                                    }
                                }
                            } else {

                                binding.orderDetailLnly?.visibility = View.GONE
                                bindingNoOrder.emptyFrame?.visibility = View.VISIBLE
                                if(isFromCP){
                                    bindingNoOrder.emptyText.text = getString(R.string.no_data_cp_empty_screen)
                                }else{
                                    bindingNoOrder.emptyText.text = getString(R.string.no_data_empty_screen)
                                }
                                Utils.setErrorData(this, resource.data.errorBody())
                            }

                        }
                        Status.ERROR -> {
                            hideLoader()
                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                            binding.orderDetailLnly?.visibility = View.GONE
                            bindingNoOrder.emptyFrame?.visibility = View.VISIBLE
                            if(isFromCP){
                                bindingNoOrder.emptyText.text = getString(R.string.no_data_cp_empty_screen)
                            }else{
                                bindingNoOrder.emptyText.text = getString(R.string.no_data_empty_screen)
                            }
                        }
                        Status.LOADING -> {

                        }
                    }
                }
            })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        binding.navBackOrderDeatilBtn?.performClick()
    }

    private fun showLoader() {
        binding.ivLoadingEdit.visibility = View.VISIBLE
        binding.ivLoadingEdit.smoothToShow()
    }

    private fun hideLoader() {
        binding.ivLoadingEdit.visibility = View.GONE
        binding.ivLoadingEdit.smoothToHide()
    }

    private fun upDateUi(ODRes:OrderDetailObj?){

        if(ODRes != null){

            binding.orderIDLabel.text = getString(R.string.order_id_txt)+ODRes.vendor_order_info?._id
            val string1 = ODRes.created_at
            val Date1 = ISO8601Utils.parse(string1, ParsePosition(0))
            val posFormat = SimpleDateFormat("dd MMM yyyy")
            val oldPosDate = posFormat.format(Date1)

            binding.dateTxt.text = getString(R.string.order_list_assign_date)+oldPosDate


            if(isFromCP){
                Utils.setImageUsingGlide(this@OrderDetailActivity, ODRes.CP_info?.concrete_pump_image_url?:"", binding.vehicleImg)
                binding.truckCategoryNameTxt.text = ODRes.CP_category_info?.category_name
                binding.truckRangeTxt.text = ODRes.CP_info?.company_name +" - "+ODRes.CP_info?.concrete_pump_model
                binding.vehicleNumberTxt?.visibility = View.GONE
                binding.qtyTitleTxt.text = getString(R.string.cp_subcategory_txt)
                binding.deliverQtyTxt.text = (ODRes.CP_info?.concrete_pump_capacity?:0).toString() + " Cu.Mtr"

            }else{
                Utils.setImageUsingGlide(this@OrderDetailActivity, ODRes.TM_info?.TM_image_url?:"", binding.vehicleImg)
                binding.truckCategoryNameTxt.text = getString(R.string.vehicle_list_fix_title)
                binding.truckRangeTxt.text = ODRes.TM_category_info?.category_name
                binding.vehicleNumberTxt?.visibility = View.VISIBLE
                binding.vehicleNumberTxt.text = ODRes.TM_info?.TM_rc_number
                binding.qtyTitleTxt.text = getString(R.string.pickup_qty_txt)
                binding.deliverQtyTxt.text = ODRes.pickup_quantity?.toInt().toString()+" Cu.Mtr"
            }

           /* binding.truckRangeTxt.text = Utils.setMTTextview(ODRes.TM_sub_category_info?.sub_category_name,
                ODRes.vehicle_sub_category_info?.min_load_capacity?.toString(),ODRes.vehicle_sub_category_info?.max_load_capacity?.toString())*/


            if(ODRes.vendor_info?.company_name != null){
                binding.supplierCompLnly.visibility = View.VISIBLE
                binding.orderSupplierCompanyNameTxt.text = ODRes.vendor_info?.company_name

            }else{
                binding.supplierCompLnly.visibility = View.GONE
            }

            binding.orderSupplierNameTxt.text = ODRes.vendor_info?.full_name
            binding.orderSuppliercontactTxt.text = ODRes.vendor_info?.mobile_number
            binding.orderSuppliercontactTxt?.setOnClickListener{
                val callIntent = Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + Uri.encode(ODRes.vendor_info?.mobile_number?:"")))
                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(callIntent)
            }

            if(ODRes.buyer_info?.company_name != null ){
                binding.buyerCompanyLnly.visibility = View.VISIBLE
                binding.buyerCompanynameTxt.text = ODRes.buyer_info?.company_name
            }else{
                binding.buyerCompanyLnly.visibility = View.GONE
            }

            binding.buyerNameTxt.text = ODRes.buyer_info?.full_name
            binding.buyerContactTxt.text = ODRes.buyer_info?.mobile_number

            binding.buyerContactTxt?.setOnClickListener{
                val callIntent = Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + Uri.encode(ODRes.buyer_info?.mobile_number?:"")))
                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(callIntent)
            }

            pickUpAddrees = Utils.formatedAddresss(ODRes.pickup_address_info?.line1,
                ODRes.pickup_address_info?.line2,ODRes.pickup_address_info?.city_name,ODRes.pickup_address_info?.state_name,
                ODRes.pickup_address_info?.pincode)
            binding.pickupAddresstxt.text = pickUpAddrees

            deliveryAddress = Utils.formatedAddresss(ODRes.delivery_address_info?.line1,
                ODRes.delivery_address_info?.line2,ODRes.delivery_address_info?.city_name,ODRes.delivery_address_info?.state_name,
                ODRes.delivery_address_info?.pincode)

            binding.deliverAddresstxt.text = deliveryAddress

        }
    }
}