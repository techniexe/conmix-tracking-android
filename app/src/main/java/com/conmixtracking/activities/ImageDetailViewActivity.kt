package com.conmixtracking.activities

import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.conmixtracking.R
import com.conmixtracking.databinding.ImageDetailActivityBinding
import com.conmixtracking.utils.MyImageRequestListener
import com.jpeng.progress.CircleProgress
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.assist.FailReason
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 10,May,2021
 */
class ImageDetailViewActivity:AppCompatActivity(), MyImageRequestListener.Callback {
    private lateinit var binding: ImageDetailActivityBinding



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ImageDetailActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val profileUrl = intent.getStringExtra("profileUrl")
        val progress = CircleProgress.Builder()
            .setTextShow(false)
            .setProgressColorRes(R.color.image_progress_image, this)
            .setBottomColorRes(R.color.white, this)
            .build();
        val imageLoader = ImageLoader.getInstance()
        val options = DisplayImageOptions.Builder()
            .cacheInMemory(true)
            .showImageOnLoading(R.drawable.placeholder)
            .showImageOnFail(R.drawable.placeholder)
            .showImageForEmptyUri(R.drawable.placeholder)
            .cacheOnDisk(true)
            .build();

        imageLoader.displayImage(profileUrl, binding.profilePic, options, object : ImageLoadingListener {
            override fun onLoadingComplete(imageUri: String?, view: View?, loadedImage: Bitmap?) {
                binding.thumnailProfilePic.visibility = View.GONE
            }

            override fun onLoadingStarted(imageUri: String?, view: View?) {

            }

            override fun onLoadingCancelled(imageUri: String?, view: View?) {

            }

            override fun onLoadingFailed(imageUri: String?, view: View?, failReason: FailReason?) {

            }


        }, object : ImageLoadingProgressListener {
            override fun onProgressUpdate(imageUri: String?, view: View?, current: Int, total: Int) {
                progress.setLevel(current);
                progress.setMaxValue(total.toLong());
            }

        })


        progress.inject(binding.profilePic)

        binding.navBackProfilePic.setOnClickListener {
            finish()
            Bungee.slideRight(this@ImageDetailViewActivity)
        }
    }

    override fun onFailure(message: String?) {
        binding.loaderView.visibility = View.GONE
    }

    override fun onSuccess(dataSource: String) {
        binding.loaderView.visibility = View.GONE
    }

    override fun onBackPressed() {
        finish()
        Bungee.slideRight(this@ImageDetailViewActivity)
    }



}