package com.conmixtracking.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.conmixtracking.R
import com.conmixtracking.databinding.ActivityMapBinding
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions


/**
 * Created by Hitesh Patel on 10,May,2021
 */
class MapMarkerActivity:AppCompatActivity(), OnMapReadyCallback {

    private lateinit var binding: ActivityMapBinding
    var  latitude:Double = 0.0
    var  longitude:Double = 0.0
    var  address:String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMapBinding.inflate(layoutInflater)
        setContentView(binding.root)

        latitude = intent.getDoubleExtra("latitude",0.0)
        longitude = intent.getDoubleExtra("longitude",0.0)
        address = intent.getStringExtra("address")?:""

        // Get the SupportMapFragment and request notification when the map is ready to be used.
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment
        mapFragment?.getMapAsync(this@MapMarkerActivity)
    }

    override fun onMapReady(googleMap: GoogleMap?) {

        googleMap?.apply {
            val latlng = LatLng(latitude, longitude)
            addMarker(
                MarkerOptions()
                    .position(latlng)
                    .title(address)
            )
            moveCamera(latlng,googleMap)
            animateCamera(latlng,googleMap)
        }

    }

    private fun moveCamera(latLng: LatLng?, googleMap: GoogleMap?) {
        googleMap?.moveCamera(CameraUpdateFactory.newLatLng(latLng))
    }

    private fun animateCamera(latLng: LatLng?, googleMap: GoogleMap?) {
        val cameraPosition = CameraPosition.Builder().target(latLng).zoom(15.5f).build()
        googleMap?.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
    }
}