package com.conmixtracking.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.conmixtracking.BuildConfig
import com.conmixtracking.R
import com.conmixtracking.ViewModel.LoginViewModel
import com.conmixtracking.ViewModel.SessionViewModel
import com.conmixtracking.data.LoginReqModel
import com.conmixtracking.data.LoginResponse
import com.conmixtracking.data.SessionRequest
import com.conmixtracking.data.SessionResponse
import com.conmixtracking.databinding.LoginActivityBinding
import com.conmixtracking.services.api.ApiClient
import com.conmixtracking.services.api.Status
import com.conmixtracking.utils.SharedPrefUtils
import com.conmixtracking.utils.Utils
import com.google.i18n.phonenumbers.NumberParseException
import com.google.i18n.phonenumbers.PhoneNumberUtil
import spencerstudios.com.bungeelib.Bungee
import java.util.*


/**
 * Created by Hitesh Patel on 07,May,2021
 */
class LoginActivity : AppCompatActivity() {
    private lateinit var binding: LoginActivityBinding
    private lateinit var sessionViewModel: SessionViewModel
    private lateinit var loginViewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = LoginActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupViewModel()
        binding.submitBtn?.setOnClickListener { v ->
            signIn(v)
        }
        binding.passwordEdtName.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                signIn(binding.passwordEdtName)
            }
            false
        }

    }

    private fun setupViewModel() {

        sessionViewModel = ViewModelProvider(
            this,
            SessionViewModel.ViewModelFactory(ApiClient().apiServiceWithAuth)
        )
            .get(SessionViewModel::class.java)

        loginViewModel =
            ViewModelProvider(this, LoginViewModel.ViewModelFactory(ApiClient().apiService))
                .get(LoginViewModel::class.java)
    }

    private fun signIn(v: View) {
       Utils.hideKeyboard(v)
        if (Utils.isNetworkAvailable(this)) {
            val userName = binding.userEdtName.text.toString().trim()
            val password = binding.passwordEdtName.text.toString().trim()
            val phoneUtil = PhoneNumberUtil.getInstance()

            if (validateFields(userName, password)) {
                try {

                    val numberProto = phoneUtil.parse(userName, binding.ccp.selectedCountryNameCode)
                    val mobNum = phoneUtil.format(numberProto, PhoneNumberUtil.PhoneNumberFormat.E164)


                    if (phoneUtil.getNumberType(numberProto) == PhoneNumberUtil.PhoneNumberType.MOBILE
                        || phoneUtil.getNumberType(numberProto) == PhoneNumberUtil.PhoneNumberType.FIXED_LINE_OR_MOBILE
                    ) {
                        getSessionToken(mobNum, password)
                    } else {
                        Utils.showToast(
                            this,
                            getString(R.string.err_invalid_number),
                            Toast.LENGTH_SHORT
                        )
                    }
                } catch (e: NumberParseException) {

                    if (userName.isNotEmpty()) {

                        val emval = Utils.isValidEmail(userName)
                        if (emval) {
                            getSessionToken(userName, password)
                        } else {

                            Utils.showToast(this, "Email is Invalid", Toast.LENGTH_SHORT)

                        }
                    }
                }
            }
        } else {
            Utils.showToast(this, getString(R.string.no_internet_msg), Toast.LENGTH_SHORT)
        }
    }

    private fun validateFields(identifier: String, password: String): Boolean {
        if (identifier.isEmpty()) {
            Utils.showToast(
                this,
                getString(R.string.err_ragister_mobile_number),
                Toast.LENGTH_SHORT
            )
            return false
        }
        if (password.isEmpty()) {
            Utils.showToast(this, getString(R.string.err_password), Toast.LENGTH_SHORT)
            return false
        }
        return true
    }

    private fun getSessionToken(userName: String?, password: String?) {
        if (userName != null) {
            showLoader()
        }
        ApiClient.setToken()
        // dev access Token adUysa2Hlah9hykTuw4Hkqh
        // production token alylLUYKhohh72hjlk2hll2ll3jhhl2hhl
        val tokenReq = SessionRequest(BuildConfig.AUTH_TOKEN, UUID.randomUUID().toString())
        sessionViewModel.getSession(tokenReq)
            .observe(this, androidx.lifecycle.Observer {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            if (resource.data?.isSuccessful!!) {
                                resource.data?.let { response ->
                                    setData(
                                        response.body()?.data,
                                        userName,
                                        password
                                    )
                                }
                            } else {
                                Utils.setErrorData(this, resource.data.errorBody())
                            }

                        }
                        Status.ERROR -> {
                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                        }
                        Status.LOADING -> {

                        }

                    }
                }
            })
    }

    private fun showLoader() {
        binding.ivLoadingEdit.visibility = View.VISIBLE
        binding.ivLoadingEdit.smoothToShow()
    }

    fun hideLoader() {
        binding.ivLoadingEdit.visibility = View.INVISIBLE
        binding.ivLoadingEdit.smoothToHide()
    }

    private fun setData(response: SessionResponse?, userName: String?, password: String?) {
        if (response != null) {
          //  SharedPrefrence.setSessionToken(this, response.token)

            SharedPrefUtils.getInstance(this@LoginActivity).storeStringInSharedPref(SharedPrefUtils.SESSION_TOKEN, response.token)
        }

        if (userName != null && password != null) {
            login(userName, password)
        }
    }

    private fun login(userName: String, password: String) {
        ApiClient.setToken()
        loginViewModel.loginWithIdentifier(
            userName = userName,
            loginReqModel = LoginReqModel(password)
        ).observe(this, androidx.lifecycle.Observer {

            it?.let {

                    resource ->

                when (resource.status) {

                    Status.SUCCESS -> {
                        hideLoader()
                        if (resource.data?.isSuccessful!!) {

                            Utils.showToast(
                                this,
                                getString(R.string.sucess_login),
                                Toast.LENGTH_SHORT
                            )
                            setResponseFromLogin(resource.data.body()!!.data)
                        } else {
                            Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.ERROR -> {

                        hideLoader()
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_SHORT)
                    }
                    Status.LOADING -> {
                        showLoader()
                    }
                }
            }
        })
    }

    private fun setResponseFromLogin(loginResponse: LoginResponse) {
        val customToken = loginResponse.customToken
        SharedPrefUtils.getInstance(this@LoginActivity)
            .storeStringInSharedPref(SharedPrefUtils.SESSION_TOKEN, customToken)
        SharedPrefUtils.getInstance(this@LoginActivity)
            .storeBooleanInSharedPref(SharedPrefUtils.IS_LOGED_IN, true)
        navigateToMainActivity()
    }
    private fun navigateToMainActivity() {
        val intent = Intent(this@LoginActivity, ChooseTrackingActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
        finish()
        Bungee.slideLeft(this)
    }
}