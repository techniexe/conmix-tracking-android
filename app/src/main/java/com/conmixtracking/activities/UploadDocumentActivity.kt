package com.conmixtracking.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.conmixtracking.ConmixTrackingApp
import com.conmixtracking.R
import com.conmixtracking.ViewModel.OrderViewModel
import com.conmixtracking.data.OrderDetailObj

import com.conmixtracking.databinding.UploadDocumentScreeenBinding
import com.conmixtracking.services.api.ApiClient
import com.conmixtracking.services.api.Status
import com.conmixtracking.utils.Constants
import com.conmixtracking.utils.Utils
import com.github.dhaval2404.imagepicker.ImagePicker
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import spencerstudios.com.bungeelib.Bungee
import java.io.File


/**
 * Created by Hitesh Patel on 10,May,2021
 */
class UploadDocumentActivity:AppCompatActivity() {
    private lateinit var binding: UploadDocumentScreeenBinding
    private lateinit var orderViewModel: OrderViewModel

  //  private var royalImage: String? = null
  //  private var wayBillImg: String? = null
    private var challanImg: String? = null
    private var inVoiceImg: String? = null
    private var orderId: String? = null
    private var ODRes: OrderDetailObj? = null
    private var vehicleId:String? = null

  //  private var royalImgPath: String? = null
  //  private var waybillPath: String? = null
    private var challanPath: String? = null
    private var invoicePath:String? = null
    var isFromCP:Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = UploadDocumentScreeenBinding.inflate(layoutInflater)

        setContentView(binding.root)
        setupViewModel()

        orderId = intent.getStringExtra("order_id")
        ODRes = intent.getParcelableExtra("ODRes")
        isFromCP = intent.getBooleanExtra("isFromCP",false)?:false
        //vehicleId = intent.getStringExtra("vehicle_Id")

        vehicleId = ConmixTrackingApp.instance?.globalVehicle

        if(isFromCP){
            binding.invoiceLNLY.visibility = View.GONE
            binding.invoiceVw.visibility = View.GONE
        }else{
            binding.invoiceLNLY.visibility = View.GONE
            binding.invoiceVw.visibility = View.GONE
        }

       // SharedPrefrence.setTMID(this,vehicleId)

        binding.navBackUpload.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }

      /*  binding.royaltyLNLY.setOnClickListener {

            // ImagePicker.cameraOnly().start(this, Constants.REQUEST_CODE_SELECT_ROYALTY_IMAGE)

            ImagePicker.with(this)
                .cameraOnly()	//User can only capture image using Camera
                .compress(1024)			//Final image size will be less than 1 MB(Optional)
                .maxResultSize(1080, 1080)
                .start(Constants.REQUEST_CODE_SELECT_ROYALTY_IMAGE)

        }*/

        /*binding.wayslipLNLY.setOnClickListener {

            //ImagePicker.cameraOnly().start(this, Constants.REQUEST_CODE_SELECT_WAYSLIP_IMAGE)

            ImagePicker.with(this)
                .cameraOnly()	//User can only capture image using Camera
                .compress(1024)			//Final image size will be less than 1 MB(Optional)
                .maxResultSize(1080, 1080)
                .start(Constants.REQUEST_CODE_SELECT_WAYSLIP_IMAGE)

        }*/

        binding.challanLNLY.setOnClickListener {

            //  ImagePicker.cameraOnly().start(this, Constants.REQUEST_CODE_SELECT_CHALLAN_IMAGE)

            ImagePicker.with(this)
                .cameraOnly()	//User can only capture image using Camera
                .compress(1024)			//Final image size will be less than 1 MB(Optional)
                .maxResultSize(1080, 1080)
                .start(Constants.REQUEST_CODE_SELECT_CHALLAN_IMAGE)

        }

        binding.invoiceLNLY.setOnClickListener {

            // ImagePicker.cameraOnly().start(this, Constants.REQUEST_CODE_SELECT_INVOICE_IMAGE)

            ImagePicker.with(this)
                .cameraOnly()	//User can only capture image using Camera
                .compress(1024)			//Final image size will be less than 1 MB(Optional)
                .maxResultSize(1080, 1080)
                .start(Constants.REQUEST_CODE_SELECT_INVOICE_IMAGE)
        }

        binding.doneLabel.setOnClickListener {

            if (validationSelectImage()) {
                binding.uploadImage.visibility = View.VISIBLE
                binding.uploadImage.start()

                if(isFromCP){
                    uploadDocumentCPfun()
                }else{
                    uploadDocumentfun()
                }


                binding.doneLabel.visibility = View.GONE
            }
        }
    }

    private fun setupViewModel() {

        orderViewModel = ViewModelProvider(
            this,
            OrderViewModel.ViewModelFactory(ApiClient().apiService)
        )
            .get(OrderViewModel::class.java)


    }

    private fun validationSelectImage(): Boolean {

        if(isFromCP){
            if (challanImg == null) {

                Utils.showToast(this, getString(R.string.challan_pass_empty_txt), Toast.LENGTH_SHORT)
                return false

            }
        }else{
            if (challanImg == null) {

                Utils.showToast(this, getString(R.string.challan_pass_empty_txt), Toast.LENGTH_SHORT)
                return false

            }
            /* else if (royalImage == null) {

                 Utils.showToast(this, getString(R.string.royal_pass_empty_txt), Toast.LENGTH_SHORT)
                 return false

             }else if (wayBillImg == null) {

                 Utils.showToast(this, getString(R.string.waybill_pass_empty_txt), Toast.LENGTH_SHORT)
                 return false

             }*/
            /*else if (inVoiceImg  == null) {

                Utils.showToast(this, getString(R.string.invoice_pass_empty_txt), Toast.LENGTH_SHORT)
                return false

            }*/
        }



        return true
    }

    override fun onBackPressed() {


        binding.navBackUpload.performClick()

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
      /*  if (requestCode == Constants.REQUEST_CODE_SELECT_ROYALTY_IMAGE) {
            if (resultCode == AppCompatActivity.RESULT_OK && null != data) {

                try{

                    //Image Uri will not be null for RESULT_OK
                    val fileUri = data?.data
                    binding.royaltyImg.setImageURI(fileUri)

                    royalImage = fileUri.toString()

                    //You can get File object from intent
                    val file: File = ImagePicker.getFile(data)!!

                    //You can also get File Path from intent
                    royalImgPath = ImagePicker.getFilePath(data)!!



                }catch (e:Exception){
                    e.printStackTrace()
                }
            }

        } else if (requestCode == Constants.REQUEST_CODE_SELECT_WAYSLIP_IMAGE) {

            if (resultCode == AppCompatActivity.RESULT_OK && null != data) {

                try {



                    //Image Uri will not be null for RESULT_OK
                    val fileUri = data?.data
                    binding.wayslipImg.setImageURI(fileUri)

                    wayBillImg = fileUri.toString()

                    //You can get File object from intent
                    val file: File = ImagePicker.getFile(data)!!

                    //You can also get File Path from intent
                    waybillPath = ImagePicker.getFilePath(data)!!
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }else*/
        if (requestCode == Constants.REQUEST_CODE_SELECT_CHALLAN_IMAGE) {

            if (resultCode == AppCompatActivity.RESULT_OK && null != data) {

                try {
                    /*val images =
                    com.esafirm.imagepicker.features.ImagePicker
                        .getImages(data) as ArrayList<Image>

                challanImg = images.get(0).path
                challanPath = Utils().compressImageUploadMedia(images.get(0).path, this)

                val requestOption = RequestOptions()
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .centerCrop()
                    .diskCacheStrategy(
                        DiskCacheStrategy.ALL
                    ).dontAnimate()
                Glide.with(this).load(challanImg)
                    .apply(requestOption)
                    .into(challan_img)*/


                    //Image Uri will not be null for RESULT_OK
                    val fileUri = data?.data
                    binding.challanImg.setImageURI(fileUri)

                    challanImg = fileUri.toString()

                    //You can get File object from intent
                    val file: File = ImagePicker.getFile(data)!!

                    //You can also get File Path from intent
                    challanPath = ImagePicker.getFilePath(data)!!


                } catch (e: Exception) {

                }

            }
        } else if (requestCode == Constants.REQUEST_CODE_SELECT_INVOICE_IMAGE) {

            if (resultCode == AppCompatActivity.RESULT_OK && null != data) {

                try {

                    /* val images =
                    com.esafirm.imagepicker.features.ImagePicker
                        .getImages(data) as ArrayList<Image>

                inVoiceImg = images.get(0).path
                invoicePath = Utils().compressImageUploadMedia(images.get(0).path, this)

                val requestOption = RequestOptions()
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .centerCrop()
                    .diskCacheStrategy(
                        DiskCacheStrategy.ALL
                    ).dontAnimate()
                Glide.with(this).load(inVoiceImg)
                    .apply(requestOption)
                    .into(invoice_img)
*/


                    //Image Uri will not be null for RESULT_OK
                    val fileUri = data?.data
                    binding.invoiceImg.setImageURI(fileUri)

                    inVoiceImg = fileUri.toString()

                    //You can get File object from intent
                    val file: File = ImagePicker.getFile(data)!!

                    //You can also get File Path from intent
                    invoicePath = ImagePicker.getFilePath(data)!!

                } catch (e: Exception) {

                }
            }

        }
    }

    fun uploadDocumentfun() {

        /*val royalfile = File(royalImgPath)
        val royalrequestFile = RequestBody.create("image/jpeg".toMediaTypeOrNull(), royalfile)

        // MultipartBody.Part is used to send also the actual file name
        val royalbody = MultipartBody.Part.createFormData(
            "royalty_pass_image",
            royalfile.name,
            royalrequestFile
        )

        val waybillfile = File(waybillPath)
        val waybillrequestFile = RequestBody.create("image/jpeg".toMediaTypeOrNull(), waybillfile)

        // MultipartBody.Part is used to send also the actual file name
        val waybillbody =
            MultipartBody.Part.createFormData("way_slip_image", royalfile.name,
                waybillrequestFile)*/

        val challanfile = File(challanPath)
        val challanrequestFile = RequestBody.create("image/jpeg".toMediaTypeOrNull(), challanfile)

        // MultipartBody.Part is used to send also the actual file name
        val challanbody =
            MultipartBody.Part.createFormData("challan_image", challanfile.name,
                challanrequestFile)

      /*  val invoicefile = File(invoicePath)
        val invoicerequestFile = RequestBody.create("image/jpeg".toMediaTypeOrNull(), invoicefile)

        // MultipartBody.Part is used to send also the actual file name
        val invoicebody =
            MultipartBody.Part.createFormData("invoice_image", invoicefile.name,
                invoicerequestFile)*/



        ApiClient.setToken()

        orderViewModel.uploadDocumentOrder(ODRes?._id,vehicleId,ODRes?.vendor_order_info?._id,null, null, challanbody,null).observe(this, androidx.lifecycle.Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {

                        if (resource.data?.isSuccessful!!) {
                            resource.data.let { response ->
                                if (response?.code() == 202) {
                                    binding.uploadImage.visibility = View.GONE
                                    binding.uploadImage.stop()
                                    binding.doneLabel?.visibility = View.VISIBLE
                                    Utils.showToast(
                                        this@UploadDocumentActivity,
                                        getString(R.string.sucessfully_upload),
                                        Toast.LENGTH_SHORT
                                    )
                                    val intent = Intent(this@UploadDocumentActivity, DrawSignatureActivity::class.java)
                                    intent.putExtra("order_id", ODRes?.vendor_order_info?._id)
                                    intent.putExtra("ODRes", ODRes)
                                    intent.putExtra("isFromCP",false)
                                    startActivity(intent)
                                    Bungee.slideLeft(this@UploadDocumentActivity)

                                }else{
                                    binding.uploadImage.visibility = View.GONE
                                    binding.uploadImage.stop()
                                    binding.doneLabel?.visibility = View.VISIBLE
                                    Utils.showToast(
                                        this@UploadDocumentActivity,
                                        getString(R.string.error_upload),
                                        Toast.LENGTH_SHORT
                                    )
                                }
                            }
                        } else {
                            binding.uploadImage.visibility = View.GONE
                            binding.uploadImage.stop()
                            binding.doneLabel?.visibility = View.VISIBLE


                            Utils.setErrorData(this, resource.data.errorBody())
                        }

                    }
                    Status.ERROR -> {
                        binding.uploadImage.visibility = View.GONE
                        binding.uploadImage.stop()
                        binding.doneLabel?.visibility = View.VISIBLE
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {

                    }
                }
            }
        })
    }

    fun uploadDocumentCPfun() {

        /*val royalfile = File(royalImgPath)
        val royalrequestFile = RequestBody.create("image/jpeg".toMediaTypeOrNull(), royalfile)

        // MultipartBody.Part is used to send also the actual file name
        val royalbody = MultipartBody.Part.createFormData(
            "royalty_pass_image",
            royalfile.name,
            royalrequestFile
        )

        val waybillfile = File(waybillPath)
        val waybillrequestFile = RequestBody.create("image/jpeg".toMediaTypeOrNull(), waybillfile)

        // MultipartBody.Part is used to send also the actual file name
        val waybillbody =
            MultipartBody.Part.createFormData("way_slip_image", royalfile.name,
                waybillrequestFile)*/

        val challanfile = File(challanPath)
        val challanrequestFile = RequestBody.create("image/jpeg".toMediaTypeOrNull(), challanfile)

        // MultipartBody.Part is used to send also the actual file name
        val challanbody =
            MultipartBody.Part.createFormData("challan_image", challanfile.name,
                challanrequestFile)





        ApiClient.setToken()

        orderViewModel.uploadDocumentCPOrder(ODRes?._id,vehicleId,ODRes?.vendor_order_info?._id,null, null, challanbody,null).observe(this, androidx.lifecycle.Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {

                        if (resource.data?.isSuccessful!!) {
                            resource.data.let { response ->
                                if (response?.code() == 202) {
                                    binding.uploadImage.visibility = View.GONE
                                    binding.uploadImage.stop()
                                    binding.doneLabel?.visibility = View.VISIBLE
                                    Utils.showToast(
                                        this@UploadDocumentActivity,
                                        getString(R.string.sucessfully_upload),
                                        Toast.LENGTH_SHORT
                                    )
                                    val intent = Intent(this@UploadDocumentActivity, DrawSignatureActivity::class.java)
                                    intent.putExtra("order_id", ODRes?.vendor_order_info?._id)
                                    intent.putExtra("ODRes", ODRes)
                                    intent.putExtra("isFromCP",isFromCP)
                                    startActivity(intent)
                                    Bungee.slideLeft(this@UploadDocumentActivity)

                                }else{
                                    binding.uploadImage.visibility = View.GONE
                                    binding.uploadImage.stop()
                                    binding.doneLabel?.visibility = View.VISIBLE
                                    Utils.showToast(
                                        this@UploadDocumentActivity,
                                        getString(R.string.error_upload),
                                        Toast.LENGTH_SHORT
                                    )
                                }
                            }
                        } else {
                            binding.uploadImage.visibility = View.GONE
                            binding.uploadImage.stop()
                            binding.doneLabel?.visibility = View.VISIBLE


                            Utils.setErrorData(this, resource.data.errorBody())
                        }

                    }
                    Status.ERROR -> {
                        binding.uploadImage.visibility = View.GONE
                        binding.uploadImage.stop()
                        binding.doneLabel?.visibility = View.VISIBLE
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {

                    }
                }
            }
        })
    }
}