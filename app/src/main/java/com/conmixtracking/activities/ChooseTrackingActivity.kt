package com.conmixtracking.activities

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.PopupMenu
import com.conmixtracking.R
import com.conmixtracking.databinding.ChooseTrackingActivityBinding
import com.conmixtracking.dialog.SelectLanguageDialog
import com.conmixtracking.utils.SharedPrefUtils
import spencerstudios.com.bungeelib.Bungee
import java.util.*


/**
 * Created by Hitesh Patel on 27,July,2021
 */
class ChooseTrackingActivity:AppCompatActivity(),SelectLanguageDialog.DialogToFragment{

    private lateinit var binding: ChooseTrackingActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ChooseTrackingActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.tmBtn.setOnClickListener {
            val intent = Intent(this@ChooseTrackingActivity, TMListActivity::class.java)
            startActivity(intent)
            Bungee.slideLeft(this)

        }

        binding.cpBtn.setOnClickListener {
            val intent = Intent(this@ChooseTrackingActivity, CPListActivity::class.java)
            startActivity(intent)
            Bungee.slideLeft(this)

        }

        binding.menuIcon.setOnClickListener {

            showPopupMenu(binding.menuIcon)
        }
    }
    fun showPopupMenu(v: View) {
        val popup = PopupMenu(this@ChooseTrackingActivity, v)
        val inflater = popup.menuInflater

        inflater.inflate(R.menu.log_out_menu, popup.menu)
        popup.show()
        popup.setOnMenuItemClickListener { item ->

            when (item.itemId) {

                /* R.id.logoutMenu -> {
                     val fragment: BreakAlertDialog = BreakAlertDialog(
                         this,
                         getString(R.string.logout_alert_txt),
                         "YES",
                         "NO"
                     )
                     fragment.show(supportFragmentManager, "alert")
                     fragment.isCancelable = false
                     fragment.setDialogListener(this)
                     true
                 }*/

                R.id.selectLangMenu -> {

                    val languageDialog = SelectLanguageDialog()
                    languageDialog.setListener(this)
                    languageDialog.show(
                        supportFragmentManager,
                        SelectLanguageDialog::class.java.simpleName
                    )

                    true
                }

                else -> false
            }

        }
    }

    override fun sortDialogDismiss() {

    }

    override fun sortDialogSave(sortType: String) {

        var slv = "en"

        if(sortType == "English"){
            slv = "en"


            SharedPrefUtils.getInstance(this@ChooseTrackingActivity).storeStringInSharedPref(
                SharedPrefUtils.LANGUAGE_KEY,
                "en"
            )
        }else{
            slv = "hi"

            SharedPrefUtils.getInstance(this@ChooseTrackingActivity).storeStringInSharedPref(
                SharedPrefUtils.LANGUAGE_KEY,
                "hi"
            )
        }



        // updating the language for devices above android nougat
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            updateResources(this, slv)
        }else{
            // for devices having lower version of android os
            updateResourcesLegacy(this, slv)
        }

    }

    @TargetApi(Build.VERSION_CODES.N)
    private fun updateResources(context: Context, language: String) {
        val locale = Locale(language)
        Locale.setDefault(locale)
        val configuration: Configuration = context.getResources().getConfiguration()
        configuration.setLocale(locale)
        configuration.setLayoutDirection(locale)
        // return context.createConfigurationContext(configuration)
        resources.updateConfiguration(configuration, resources.getDisplayMetrics())


        recreate()
    }

    @SuppressLint("ObsoleteSdkInt")
    private fun updateResourcesLegacy(context: Context, language: String) {
        val locale = Locale(language)
        Locale.setDefault(locale)
        val resources: Resources = context.resources
        val configuration: Configuration = resources.getConfiguration()
        configuration.locale = locale
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLayoutDirection(locale)
        }
        resources.updateConfiguration(configuration, resources.getDisplayMetrics())
        //return context

        recreate()
    }
}