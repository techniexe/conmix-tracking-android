package com.conmixtracking.activities

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.conmixtracking.ConmixTrackingApp
import com.conmixtracking.R
import com.conmixtracking.ViewModel.LoginViewModel
import com.conmixtracking.ViewModel.OrderViewModel
import com.conmixtracking.data.OrderDetailObj
import com.conmixtracking.data.UpdateQtyOBJ
import com.conmixtracking.databinding.OtpActivityBinding
import com.conmixtracking.services.api.ApiClient
import com.conmixtracking.services.api.Status
import com.conmixtracking.utils.SharedPrefUtils
import com.conmixtracking.utils.Utils

import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 10,May,2021
 */
class OTPActivity:AppCompatActivity(), View.OnClickListener {
    private lateinit var binding: OtpActivityBinding
    private lateinit var loginViewModel: LoginViewModel
    private lateinit var orderViewModel: OrderViewModel

    var  ODRes: OrderDetailObj?= null
    var event :String? = null
    private var mobNum = ""
    private var otp: String? = ""
    private var cTimer: CountDownTimer? = null
    var cancelOrd :Boolean = false
    var vehicleId:String? = null
    var reasonEdt:String? = null
    var isFromCP:Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = OtpActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupViewModel()

        ODRes = intent.getParcelableExtra("orderDtl")
        event = intent.getStringExtra("fromEvnt")
        cancelOrd = intent.getBooleanExtra("cancelOrd",false)
        isFromCP = intent.getBooleanExtra("isFromCP",false)
      //  vehicleId = intent.getStringExtra("vehicleId")

        vehicleId = ConmixTrackingApp.instance?.globalVehicle
        reasonEdt = intent.getStringExtra("reasonRj")?:null

        mobNum = ODRes?.buyer_info?.mobile_number?:""
        if (mobNum.isNotEmpty()) {

           // binding.tvDescription.text =String.format(resources.getString(R.string.txt_description_sms), mobNum)
            binding.tvDescription.text =Utils.getMaskMobileNumber(mobNum,false,this)
        }
        reverseTimer(120)
        binding.btnOtpVerify.setOnClickListener(this@OTPActivity)
        binding.tvResendCode.setOnClickListener(this@OTPActivity)


        binding.navBackOTP?.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }

        binding.codeEdtLogin.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                stopAnim()
                otp = binding.codeEdtLogin.text?.toString()
                if (otp?.isNotEmpty() == true && otp?.length == 6) {
                    cTimer?.cancel()
                    verifyOtp()
                } else {
                    stopAnim()
                }
            }
            false
        }
        binding.codeEdtLogin.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                binding.btnOtpVerify.isEnabled = s?.length == 6
            }

        })
    }

    private fun setupViewModel() {

        loginViewModel = ViewModelProvider(
            this,
            LoginViewModel.ViewModelFactory(ApiClient().apiService)
        )
            .get(LoginViewModel::class.java)

        orderViewModel = ViewModelProvider(
            this,
            OrderViewModel.ViewModelFactory(ApiClient().apiService)
        )
            .get(OrderViewModel::class.java)


    }


    private fun reverseTimer(Seconds: Int) {
        // tvResendCode.visibility = View.GONE

        binding.tvResendCode.visibility = View.INVISIBLE
        binding.tvSecond.visibility = View.VISIBLE
        binding.tvSecond.text = "00:00"
        cTimer?.cancel()
        cTimer = object : CountDownTimer((Seconds * 1000 + 1000).toLong(), 1000) {
            override fun onTick(millisUntilFinished: Long) {
                var seconds = (millisUntilFinished / 1000).toInt()
                val minutes = seconds / 60
                seconds %= 60
                binding.tvSecond.text =
                    String.format("%02d", minutes) + ":" + String.format("%02d", seconds)

            }

            override fun onFinish() {

                binding.tvResendCode.visibility = View.VISIBLE
                binding.tvSecond.visibility = View.GONE
                binding.tvResendCode.text = getString(R.string.txt_resend_code)


            }
        }.start()
    }

    private fun startAnim() {
        if (binding.avLoading != null) {
            binding.avLoading.show()
            binding.avLoading.visibility = View.VISIBLE
        }
        //loaderFrame.visibility = View.VISIBLE
    }

    private fun stopAnim() {
        if (binding.avLoading != null) {
            binding.avLoading.hide()
            binding.avLoading.visibility = View.GONE
        }
        //loaderFrame.visibility = View.GONE
    }

    override fun onBackPressed() {

        binding.navBackOTP.performClick()
    }

    private fun verifyOtp() {

        ApiClient.setToken()
        loginViewModel.postMobileCode(mobNum,otp)
            .observe(this, androidx.lifecycle.Observer {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {

                            if (resource.data?.isSuccessful == true) {
                                resource.data.let { response ->

                                    if(response.code() ==200){

                                        if(isFromCP){
                                            UpdateQtyStatusCP(event!!)
                                        }else{
                                            UpdateQty(event!!)
                                        }



                                    } else if (response.code() == 422) {

                                        Utils.showToast(
                                            this@OTPActivity,
                                            response?.message(),
                                            Toast.LENGTH_SHORT
                                        )
                                    }

                                }
                            } else {


                                Utils.setErrorData(this, resource.data?.errorBody())
                            }

                        }
                        Status.ERROR -> {

                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)

                        }
                        Status.LOADING -> {

                        }



                    }
                }
            })

    }

    override fun onClick(v: View?) {

        when (v?.id) {
            R.id.btnOtpVerify -> {
                stopAnim()
                otp = binding.codeEdtLogin.text?.toString()
                if (otp?.isNotEmpty() == true && otp?.length == 6) {
                    cTimer?.cancel()
                    verifyOtp()
                } else {
                    stopAnim()
                }
            }
            R.id.tvResendCode -> {
                getOtpCode()
                reverseTimer(120)
            }
        }

    }

    fun getOtpCode(){

        if(mobNum != null) {


            ApiClient.setToken()
            loginViewModel.getOtpCode(mobNum)
                .observe(this, androidx.lifecycle.Observer {
                    it?.let { resource ->
                        when (resource.status) {
                            Status.SUCCESS -> {

                                if (resource.data?.isSuccessful == true) {
                                    resource.data.let { response ->

                                        if(response.code() ==200){




                                        } else if (response.code() == 422) {

                                            Utils.showToast(
                                                this@OTPActivity,
                                                response?.message(),
                                                Toast.LENGTH_SHORT
                                            )

                                        }

                                    }
                                } else {


                                    Utils.setErrorData(this, resource.data?.errorBody())
                                }

                            }
                            Status.ERROR -> {

                                Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)

                            }
                            Status.LOADING -> {

                            }



                        }
                    }
                })

        }else{
            Utils.showToast(this,getString(R.string.error_mobile_number), Toast.LENGTH_SHORT)
        }

    }

    private fun UpdateQty(status:String){
        val obj = UpdateQtyOBJ(null,null,status,reasonEdt)

        ApiClient.setToken()
        orderViewModel.updateQtyData(ODRes?._id,vehicleId,ODRes?.vendor_order_info?._id,obj)
            .observe(this, androidx.lifecycle.Observer {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {

                            if (resource.data?.isSuccessful == true) {
                                resource.data.let { response ->

                                    if(response.code() == 202){
                                        if(status == "REJECTED"){


                                            SharedPrefUtils.getInstance(this@OTPActivity).storeBooleanInSharedPref(
                                                SharedPrefUtils.IS_REJECT_STATUS,
                                                true
                                            )
                                            val intent = Intent(this@OTPActivity, ChooseTrackingActivity::class.java)
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                            this@OTPActivity.startActivity(intent)
                                            Bungee.slideRight(this@OTPActivity)
                                            finish()

                                        }else{
                                            val intent = Intent(this@OTPActivity, DrawBuyerSignatureActivity::class.java)
                                            intent.putExtra("order_id",ODRes?.vendor_order_info?._id)
                                            intent.putExtra("orderDtl",ODRes)
                                            intent.putExtra("isFromCP",isFromCP)
                                            startActivity(intent)
                                            Bungee.slideLeft(this@OTPActivity)
                                            finish()
                                        }
                                    }else if(response?.code() == 400){

                                        Utils.showToast(this@OTPActivity,response?.message(), Toast.LENGTH_SHORT)
                                    }


                                }
                            } else {


                                Utils.setErrorData(this, resource.data?.errorBody())
                            }

                        }
                        Status.ERROR -> {

                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)

                        }
                        Status.LOADING -> {

                        }
                    }
                }
            })
    }

    private fun UpdateQtyStatusCP(status:String){
        val obj = UpdateQtyOBJ(null,null,status,reasonEdt)

        ApiClient.setToken()
        orderViewModel.updateQtyStatusCPData(ODRes?._id,vehicleId,ODRes?.vendor_order_info?._id,obj)
            .observe(this, androidx.lifecycle.Observer {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {

                            if (resource.data?.isSuccessful == true) {
                                resource.data.let { response ->

                                    if(response.code() == 202){
                                        if(status == "REJECTED"){


                                            SharedPrefUtils.getInstance(this@OTPActivity).storeBooleanInSharedPref(
                                                SharedPrefUtils.IS_REJECT_STATUS,
                                                true
                                            )
                                            val intent = Intent(this@OTPActivity, ChooseTrackingActivity::class.java)
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                            this@OTPActivity.startActivity(intent)
                                            Bungee.slideRight(this@OTPActivity)
                                            finish()

                                        }else{
                                            val intent = Intent(this@OTPActivity, DrawBuyerSignatureActivity::class.java)
                                            intent.putExtra("order_id",ODRes?.vendor_order_info?._id)
                                            intent.putExtra("orderDtl",ODRes)
                                            intent.putExtra("isFromCP",isFromCP)
                                            startActivity(intent)
                                            Bungee.slideLeft(this@OTPActivity)
                                            finish()
                                        }
                                    }else if(response?.code() == 400){

                                        Utils.showToast(this@OTPActivity,response?.message(), Toast.LENGTH_SHORT)
                                    }


                                }
                            } else {


                                Utils.setErrorData(this, resource.data?.errorBody())
                            }

                        }
                        Status.ERROR -> {

                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)

                        }
                        Status.LOADING -> {

                        }
                    }
                }
            })
    }
}