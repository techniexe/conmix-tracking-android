package com.conmixtracking.services.api

import com.conmixtracking.data.*
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.*



/**
 * Created by Hitesh Patel on 06,May,2021
 */
interface ApiInterface {
    @POST("sessions")
    suspend fun getSessionToken(@Body sessionRequest: SessionRequest?): Response<Data<SessionResponse>>

    @POST("vendor/auth/{identifier}")
    suspend fun loginWithIdentifier(
        @Path("identifier") identifier: String,
        @Body loginReqModel: LoginReqModel
    ): Response<Data<LoginResponse>>

    @GET("vendor/TM")
    suspend fun getTMListData(
        @Query("is_active")is_active:Boolean?,
        @Query("TM_category_id")TM_category_id:String?,
        @Query("TM_sub_category_id")TM_sub_category_id:String?,
        @Query("is_gps_enabled")is_gps_enabled:Boolean?,
        @Query("is_insurance_active")is_insurance_active:Boolean?,
        @Query("before") before: String?,
        @Query("after") after: String?,
        @Query("limit_count")limit_count:Int?  ): Response<TMListResponse>

    @GET("vendor/concrete_pump")
    suspend fun getCPListData():Response<CPListResponse>

    @GET("vendor/TM/{TMId}")
    suspend fun getSpecificTMData(@Path("TMId") id: String?): Response<SpecificTMRes>

    @GET("vendor/order_track/{TMId}")
    suspend fun getOrderDetailListData(@Path("TMId") id: String?,@Query("current_date") current_date:String?):Response<OrderDeatilListRes>

    @GET("vendor/order_track/CP/{CPId}")
    suspend fun getOrderDetailCPListData(@Path("CPId") id: String?,@Query("current_date") current_date:String?):Response<OrderDeatilListRes>

    @GET("vendor/order_track/OrderDetails/{order_id}/{track_id}")
    suspend fun getOrdeIdDetailData(
        @Path("order_id") orderId: String?,
        @Path("track_id") track_id: String?
    ): Response<OrderDeatilRes>

    @GET("vendor/order_track/CP/OrderDetails/{order_id}/{track_id}")
    suspend fun getOrdeIdCPDetailData(
        @Path("order_id") orderId: String?,
        @Path("track_id") track_id: String?
    ): Response<OrderDeatilRes>

    @PATCH("vendor/order_track/{track_id}/{TMId}/{order_id}")
    suspend fun updateQtyData(
        @Path("track_id") track_id: String?,
        @Path("TMId") tmId: String?,
        @Path("order_id") order_id: String?,
        @Body uQb: UpdateQtyOBJ?
    ): Response<ResponseBody>

    @PATCH("vendor/order_track/CP/{track_id}/{CPId}/{order_id}")
    suspend fun updateQtyStatusCPData(
        @Path("track_id") track_id: String?,
        @Path("CPId") cpId: String?,
        @Path("order_id") order_id: String?,
        @Body uQb: UpdateQtyOBJ?
    ): Response<ResponseBody>

    @POST("vendor/order_track/{track_id}/{TMId}/{order_id}/pickup")
    suspend fun confirmPickupOrder(
        @Path("track_id") track_id: String?,
        @Path("TMId") tmId: String?,
        @Path("order_id") order_id: String?
    ): Response<ResponseBody>


    @POST("vendor/order_track/CP/{track_id}/{CPId}/{order_id}/pickup")
    suspend fun confirmPickupCPOrder(
        @Path("track_id") track_id: String?,
        @Path("CPId") cpId: String?,
        @Path("order_id") order_id: String?
    ): Response<ResponseBody>


    @Multipart
    @POST("vendor/order_track/{track_id}/{TMId}/{order_id}/uploadDoc")
    suspend fun uploadDocumentOrder(
        @Path("track_id") track_id: String?,
        @Path("TMId") tmId: String?,
        @Path("order_id") order_id: String?,
        @Part royalty_pass_image: MultipartBody.Part?,
        @Part way_slip_image: MultipartBody.Part?,
        @Part challan_image: MultipartBody.Part?,
        @Part invoice_image: MultipartBody.Part?
    ): Response<ResponseBody>


    @Multipart
    @POST("vendor/order_track/CP/{track_id}/{CPId}/{order_id}/uploadDoc")
    suspend fun uploadDocumentCPOrder(
        @Path("track_id") track_id: String?,
        @Path("CPId") cpId: String?,
        @Path("order_id") order_id: String?,
        @Part royalty_pass_image: MultipartBody.Part?,
        @Part way_slip_image: MultipartBody.Part?,
        @Part challan_image: MultipartBody.Part?,
        @Part invoice_image: MultipartBody.Part?
    ): Response<ResponseBody>



    @Multipart
    @POST("vendor/order_track/{track_id}/{TMId}/{order_id}/uploadDoc")
    suspend fun uploadSignSupplierOrder(
        @Path("track_id") track_id: String?,
        @Path("TMId") tmId: String?,
        @Path("order_id") order_id: String?,
        @Part supplier_pickup_signature: MultipartBody.Part?,
        @Part driver_pickup_signature: MultipartBody.Part?,
        @Part buyer_drop_signature: MultipartBody.Part?,
        @Part driver_drop_signature: MultipartBody.Part?,
    ):  Response<ResponseBody>


    @Multipart
    @POST("vendor/order_track/CP/{track_id}/{CPId}/{order_id}/uploadDoc")
    suspend fun uploadSignSupplierCPOrder(
        @Path("track_id") track_id: String?,
        @Path("CPId") tmId: String?,
        @Path("order_id") order_id: String?,
        @Part supplier_pickup_signature: MultipartBody.Part?,
        @Part driver_pickup_signature: MultipartBody.Part?,
        @Part buyer_drop_signature: MultipartBody.Part?,
        @Part driver_drop_signature: MultipartBody.Part?,
    ):  Response<ResponseBody>


    @POST("vendor/order_track/{TMId}/{orderId}/{track_id}/delay")
    suspend fun postDelayReasonOrder(
        @Path("TMId") tmId: String?,
        @Path("orderId") orderId: String?,
        @Path("track_id") trackId: String?,
        @Body dROBJ: DelayReasonObj?
    ): Response<ResponseBody>

    @POST("vendor/order_track/CP/{CPId}/{orderId}/{track_id}/delay")
    suspend fun postDelayReasonCPOrder(
        @Path("CPId") cpId: String?,
        @Path("orderId") orderId: String?,
        @Path("track_id") trackId: String?,
        @Body dROBJ: DelayReasonObj?
    ): Response<ResponseBody>



    @POST("vendor/order_track/{TMId}/{orderId}/{track_id}/deliver")
    suspend fun deliverSuceessCall(
        @Path("TMId") tmId: String?,
        @Path("orderId") orderId: String?,
        @Path("track_id") track_id: String?
    ): Response<ResponseBody>

    @POST("vendor/order_track/uploadInvoice")
    suspend fun uploadTmInvoice(@Body uploadTmInvoiceObj:UploadTmInvoiceObj?):Response<ResponseBody>

    @POST("vendor/order_track/uploadVendorInvoice")
    suspend fun uploadVendorInvoice(@Body uploadTmInvoiceObj:UploadTmInvoiceObj?):Response<ResponseBody>


    @POST("vendor/order_track/CP/{CPId}/{orderId}/{track_id}/deliver")
    suspend fun deliverSuceessCPCall(
        @Path("CPId") cpId: String?,
        @Path("orderId") orderId: String?,
        @Path("track_id") track_id: String?
    ): Response<ResponseBody>

    @GET("vendor/order_track/requestOTP/{mobile_number}")
    suspend fun getOtpCode(@Path("mobile_number") mobilenumber: String?): Response<ResponseBody>


    @POST("vendor/order_track/mobile/{mobile_number}/code/{code}")
    suspend fun postMobileCode(
        @Path("mobile_number") mobile_number: String?,
        @Path("code") code: String?
    ): Response<ResponseBody>


    @GET("vendor/order_track/checkCPStatus/{order_id}")
    suspend fun getCPDeliverCheck(@Path("order_id") order_id:String?):Response<ResponseBody>

}