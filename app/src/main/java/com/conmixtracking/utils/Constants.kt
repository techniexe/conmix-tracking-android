package com.conmixtracking.utils


/**
 * Created by Hitesh Patel on 10,May,2021
 */
object Constants {
    const val  REQUEST_CODE_SELECT_ROYALTY_IMAGE = 201
    const val  REQUEST_CODE_SELECT_WAYSLIP_IMAGE = 202
    const val REQUEST_CODE_SELECT_CHALLAN_IMAGE = 203
    const val  REQUEST_CODE_SELECT_INVOICE_IMAGE = 204

    const val  ACTION_START_FOREGROUND_SERVICE = "start_uplod_service"
    const val  ACTION_STOP_FOREGROUND_SERVICE = "stop_uplod_service"
}