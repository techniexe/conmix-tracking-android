package com.conmixtracking.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Hitesh Patel on 11,May,2021
 */
public class SharedPrefUtils {
    public static final String SESSION_TOKEN = "session_token";
    public static final String IS_LOGED_IN = "is_logged_in";
    public static final String Is_Delay_Status = "is_dealy";
    public static final String IS_REJECT_STATUS = "is_rejected";
    public static final String TM_ID = "tm_id";
    public static final String CP_ID = "cp_id";

    public static final String ORDER_ID = "order_id";
    public static final String LANGUAGE_KEY="language_key";
    public static final String ISCHOOSE_TRACK ="ischoose_track";
    private static SharedPrefUtils mSharedPrefUtils;

    private SharedPreferences.Editor editor;
    private SharedPreferences mSharedPreferences;
    private Context mContext;

    private SharedPrefUtils(Context context) {
        mContext = context;
        mSharedPreferences = context.getSharedPreferences("LOGISTIC", Context.MODE_PRIVATE);
        editor = mSharedPreferences.edit();
    }

    public static SharedPrefUtils getInstance(Context context) {
        if (mSharedPrefUtils == null) {
            mSharedPrefUtils = new SharedPrefUtils(context);
        }
        return mSharedPrefUtils;
    }

    public void storeStringInSharedPref(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public String getStringFromSharedPref(String key) {
        String restoredValue = mSharedPreferences.getString(key, "");
        return restoredValue;
    }


    public void saveStringSet(HashSet<String> mSet, String key) {

        editor.putStringSet(key, mSet);
        editor.commit();
    }

    public Set<String> getSavedStringSets(String key) {

        return mSharedPreferences.getStringSet(key, null);
    }


    public void storeBooleanInSharedPref(String key, boolean value) {
        editor.putBoolean(key, value);

        editor.commit();
    }

    public void storeIntInSharedPref(String key, int value) {
        editor.putInt(key, value);
        editor.commit();
    }

    public Integer getIntFromSharedPref(String key) {
        Integer restoredValue = mSharedPreferences.getInt(key, 0);
        return restoredValue;
    }

    public void storeLongInSharedPref(String key, Long value) {
        editor.putLong(key, value);
        editor.commit();
    }

    public Long getLongFromSharedPref(String key) {
        Long restoredValue = mSharedPreferences.getLong(key, 0L);
        return restoredValue;
    }

    public Boolean getBooleanFromSharedPref(String key) {
        Boolean restoredValue = mSharedPreferences.getBoolean(key, false);
        return restoredValue;
    }
}
