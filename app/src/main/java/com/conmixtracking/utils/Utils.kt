package com.conmixtracking.utils

import android.app.Activity
import android.content.Context
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.text.TextUtils
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.conmixtracking.R
import com.conmixtracking.data.ErrorRes
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder
import com.facebook.drawee.generic.RoundingParams
import com.facebook.drawee.view.SimpleDraweeView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.ResponseBody
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by Hitesh Patel on 06,May,2021
 */
object Utils {

    fun isNetworkAvailable(context: Context?): Boolean {
        val connectivityManager =context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }


    fun checkConnectivity(act: Context?): Boolean {
        try {
            val cm = act?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val info = cm.activeNetworkInfo
            if (cm == null) {
                return false
            } else if (info == null) {
                return false
            } else if (info.isConnectedOrConnecting) {
                return true
            }
        } catch (e: Exception) {
        }

        return false
    }


    fun showToast(context: Context?, mesaage: String, time: Int) {
        if (context != null) {

            val inflater: LayoutInflater = (context as Activity).layoutInflater
            val layout: View = inflater.inflate(
                R.layout.custom_toast,
                (context as Activity).findViewById(R.id.toast_layout_root) as ViewGroup?
            )


            val text = layout.findViewById<View>(R.id.text) as TextView
            text.text = mesaage

            val toast = Toast(context)

            //toast.setGravity(Gravity.BOTTOM, 0, 0)
            toast.duration = Toast.LENGTH_LONG
            toast.view = layout
            toast.show()
        }

    }


    fun hideKeyboard(view: View) {
        val imm = view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }



    fun isValidEmail(target: CharSequence?): Boolean {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    fun setErrorData(context: Context, data: ResponseBody?) {
        val res = data!!.string().toString()
        val gson = Gson()
        val type = object : TypeToken<ErrorRes>() {}.type
        val err = gson.fromJson<ErrorRes>(res, type)
        if (err?.error?.message != null && err.error.message.isNotEmpty())
            showToast(context, err.error.message, Toast.LENGTH_SHORT)


    }

    fun loadProfPic(context: Context?, uri: String?, draweeView: SimpleDraweeView?) {

        // val color = context?.getResources()?.getColor(R.color.circle_border_color);
        val color = context?.resources?.getColor(R.color.circle_border_color);
        val roundingParams = RoundingParams.fromCornersRadius(5f);
        roundingParams.setBorder(color!!, 1.0f);
        roundingParams.setRoundAsCircle(true);
        val builder = GenericDraweeHierarchyBuilder(context.resources)
        val hierarchy = builder.build()
        hierarchy.setPlaceholderImage(R.color.profile_pic_color)
        hierarchy.setFailureImage(R.color.profile_pic_color)
        hierarchy.roundingParams = roundingParams
        draweeView?.hierarchy = hierarchy


        draweeView?.setImageURI(Uri.parse(uri ?: ""), draweeView.context);
    }


    fun setImageUsingGlide(context: Context, imageUrl: String?, imageView: ImageView) {
        val requestOptions = RequestOptions().apply {
            placeholder(R.drawable.ic_category)
            error(R.drawable.ic_category)
            diskCacheStrategy(DiskCacheStrategy.ALL)
        }


        Glide.with(context)
            .setDefaultRequestOptions(requestOptions)
            .load(imageUrl ?: "")
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any,
                    target: com.bumptech.glide.request.target.Target<Drawable>,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable,
                    model: Any,
                    target: com.bumptech.glide.request.target.Target<Drawable>,
                    dataSource: DataSource,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }
            })
            .into(imageView)


    }

    fun setMTTextview(categoryName:String?,minloadTxt:String?,maxLoadTxt:String?):String{

        val minv = setPrecesionFormate(minloadTxt?.toDouble())
        val maxV = setPrecesionFormate(maxLoadTxt?.toDouble())

        val str = categoryName+" - ("+minv+" MT to "+maxV+" MT)"

        return str
    }

    fun setPrecesionFormate(number: Double?): String {
        val form = DecimalFormat("##,##,##,##,###.##")
        return form.format(number)
    }

    fun getEventStatus(context: Context,status:String):String{

        if(status == "PICKUP"){
            return context.getString(R.string.status_pickup)
        }else if(status == "TM_ASSIGNED"){
            return context.getString(R.string.status_assign)
        }else if(status == "DELAYED"){
            return context.getString(R.string.status_delay)
        }else if(status == "CP_ASSIGNED"){
            return context.getString(R.string.status_cp_assign)
        }

        return ""
    }

    fun getEventStatusColor(context: Context,status:String,statusLnLy:LinearLayout){

        if(status == "PICKUP"){
            statusLnLy.setBackgroundResource(R.drawable.blue_rounded_btn)
        }else if(status == "TM_ASSIGNED"){
            statusLnLy.setBackgroundResource(R.drawable.yellow_rounded_btn)
        }else if(status == "DELAYED"){
            statusLnLy.setBackgroundResource(R.drawable.orange_rounded_btn)
        }else if(status == "CP_ASSIGNED"){
            statusLnLy.setBackgroundResource(R.drawable.yellow_rounded_btn)
        }

    }

    fun formatedAddresss( line1:String?, line2:String?,
                          city_name:String?, state_name:String?,pincode:Int?=0 ):String{
        var address = ""

        if(line1 != null){
            address = line1+",\n"
        }

        if(line2 != null){
            address = address+line2+",\n"
        }

        if(city_name != null){
            address = address+ city_name+",\n"
        }
        if(state_name != null){
            address = address+ state_name+"-"
        }

        if(pincode != null){
            address = address+ pincode?.toString()
        }

        return  address
    }
    fun GlidewithLoadImageNew(context: Context?, url: String?, imageView: ImageView) {
        if (context != null) {
            val requestOptions = RequestOptions()
            requestOptions.placeholder(R.drawable.image_placeholder)
            requestOptions.error(R.drawable.image_placeholder)
            requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE)
            Glide.with(context)
                .setDefaultRequestOptions(requestOptions)
                .load(url?: "")
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any,
                        target: com.bumptech.glide.request.target.Target<Drawable>,
                        isFirstResource: Boolean
                    ): Boolean {

                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable,
                        model: Any,
                        target: com.bumptech.glide.request.target.Target<Drawable>,
                        dataSource: DataSource,
                        isFirstResource: Boolean
                    ): Boolean {


                        return false
                    }
                })
                .into(imageView)
        }
    }

    fun getMaskMobileNumber(mobNum:String?,isEmail:Boolean,context: Context):String{


        if(!isEmail) {

            val lengthmob = mobNum.toString().length
            if(lengthmob >= 10){

                val newString = mobNum?.drop(lengthmob - 10)
                val newFirststr = newString?.take(2)
                val newLatStr = newString?.takeLast(2)
                val newxxStr ="XXXXXX"

                val newStrlatest = newFirststr+newxxStr+newLatStr
                return String.format(context.getString(R.string.txt_description_sms), newStrlatest)

            }


        }else{


            return String.format(context.getString(R.string.txt_description_email), mobNum)
        }

        return ""
    }

    fun GmtCurrentDateFormate(time:String):String{
        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        dateFormat.timeZone = TimeZone.getTimeZone("GMT")
        try {
            val date = dateFormat.parse(time)
            val format = SimpleDateFormat("yyyy-MM-dd");
            val dateString = format.format(date);

            return dateString
        } catch (e: ParseException) {

        }
        return ""
    }


}