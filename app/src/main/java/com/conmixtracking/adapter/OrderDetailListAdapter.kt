package com.conmixtracking.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.conmixtracking.R
import com.conmixtracking.data.OrderDetailObj
import com.conmixtracking.utils.Utils
import com.google.gson.internal.bind.util.ISO8601Utils
import java.text.ParsePosition
import java.text.SimpleDateFormat


/**
 * Created by Hitesh Patel on 10,May,2021
 */
class OrderDetailListAdapter(val context: Context, val orderList: ArrayList<OrderDetailObj>,val isFromCP:Boolean) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val ROW_TYPE = 1
    private val LOAD_TYPE = 2
    var isLoading = false
    var mOnLoadMoreListener: OnLoadMoreListener? = null

    var mListener: orderlistInterface? = null

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is LikesViewHolder) {

            val vData = orderList[position]

            if(isFromCP){

                Utils.setImageUsingGlide(context, vData.CP_info?.concrete_pump_image_url?:"", holder.vehicleImg)
                holder.orderId_txt.text = context.getString(R.string.order_id_txt)+vData.vendor_order_info?._id
                holder.capacityTextView.text = vData.CP_info?.company_name +" - "+vData.CP_info?.concrete_pump_model
                holder.vehicle_number_txt.text = (vData.CP_info?.concrete_pump_capacity?:0.0).toString() + " Cu.Mtr"

                val string1 = vData.created_at
                val Date1 = ISO8601Utils.parse(string1, ParsePosition(0))
                val posFormat = SimpleDateFormat("dd MMM yyyy")
                val oldPosDate = posFormat.format(Date1)


                holder.date_txt.text =    context.getString(R.string.order_list_assign_date)+oldPosDate




            }else{
                Utils.setImageUsingGlide(context, vData.TM_info?.TM_image_url?:"", holder.vehicleImg)
                holder.orderId_txt.text = context.getString(R.string.order_id_txt)+vData.vendor_order_info?._id
                holder.capacityTextView.text = vData.TM_sub_category_info?.sub_category_name
                holder.vehicle_number_txt.text = vData.TM_info?.TM_rc_number

                val string1 = vData.created_at
                val Date1 = ISO8601Utils.parse(string1, ParsePosition(0))
                val posFormat = SimpleDateFormat("dd MMM yyyy")
                val oldPosDate = posFormat.format(Date1)


                holder.date_txt.text =    context.getString(R.string.order_list_assign_date)+oldPosDate



            }
            holder.statusTxt.text = Utils.getEventStatus(context,vData.event_status?:"")
            Utils.getEventStatusColor(context,vData.event_status?:"",holder.statusLnLy)

            holder.buyerComTextView.text = vData?.buyer_info?.company_name?:""


            holder.lnly.setOnClickListener {
                mListener?.onVehicleListRowClick(vData)
            }


        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.order_detail_row, parent, false)
        return LikesViewHolder(view)
    }

    override fun getItemViewType(position: Int): Int {
        return LOAD_TYPE
    }

    override fun getItemCount(): Int {
        return orderList.size
    }

    internal inner class LikesViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var lnly: CardView
        var vehicleImg: ImageView

        var orderId_txt: TextView
        var capacityTextView: TextView

        var date_txt: TextView

        var vehicle_number_txt: TextView
        var statusTxt: TextView
        var buyerComTextView:TextView
        var statusLnLy:LinearLayout


        init {

            lnly = view.findViewById<CardView>(R.id.lnly)
            vehicleImg = view.findViewById<ImageView>(R.id.vehicleImg)

            orderId_txt = view.findViewById(R.id.orderId_txt)


            capacityTextView = view.findViewById(R.id.capacityTextView)
            date_txt = view.findViewById(R.id.date_txt)

            vehicle_number_txt = view.findViewById(R.id.vehicle_number_txt)
            statusTxt = view.findViewById(R.id.statusTxt)
            buyerComTextView  = view.findViewById(R.id.buyerComTextView)
            statusLnLy = view.findViewById(R.id.statusLnLy)

        }
    }


    interface OnLoadMoreListener {

        fun onLoadMore()
    }

    fun setOnLoadMoreListener(mOnLoadMoreListener: OnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener
    }

    fun setLoaded() {
        isLoading = false
    }


    fun setListener(listener: orderlistInterface) {

        mListener = listener
    }

    interface orderlistInterface {

        fun onVehicleListRowClick(user: OrderDetailObj)
    }
}