package com.conmixtracking.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.conmixtracking.R
import com.conmixtracking.databinding.LocationSpinnerRowBinding


/**
 * Created by Hitesh Patel on 10,May,2021
 */
class SelectTimeAdapter(ctx: Context, moods: ArrayList<String>?) :
    ArrayAdapter<String>(ctx, 0, moods!!) {


    private var _binding: LocationSpinnerRowBinding?= null

    override fun getView(position: Int, recycledView: View?, parent: ViewGroup): View {
        return this.createView(position, recycledView, parent)
    }

    override fun getDropDownView(position: Int, recycledView: View?, parent: ViewGroup): View {
        return this.createView(position, recycledView, parent)
    }

    private fun createView(position: Int, recycledView: View?, parent: ViewGroup): View {
        val stateModel = getItem(position)
        _binding = LocationSpinnerRowBinding.inflate(LayoutInflater.from(context),parent,false)
         val view = _binding!!.root
        _binding?.titleSpinnerItem?.text = stateModel?.toString()
        return view
    }
}