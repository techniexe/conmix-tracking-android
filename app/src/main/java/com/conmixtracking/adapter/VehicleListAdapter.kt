package com.conmixtracking.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.conmixtracking.R
import com.conmixtracking.data.TMListObjRes
import com.conmixtracking.utils.Utils


/**
 * Created by Hitesh Patel on 07,May,2021
 */
class VehicleListAdapter(val context: Context, val vehicleList: ArrayList<TMListObjRes>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val ROW_TYPE = 1
    private val LOAD_TYPE = 2
    var isLoading = false
    var mOnLoadMoreListener: OnLoadMoreListener? = null

    var mListener: vehiclelistInterface? = null

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is LikesViewHolder) {

            val vData = vehicleList[position]


            Utils.setImageUsingGlide(context, vData.TM_image_url ?: "", holder.vehicleImg)

            //Utils().GlidewithLoadImage(context,vData.vehicle_image_url ?: "", holder.vehicleImg)

            holder.truck_range_txt.text = context.getString(R.string.vehicle_list_fix_title)
            /*holder.capacityTextView.text = vData.vehicle_sub_category?.sub_category_name+" - ("+vData?.vehicle_sub_category?.min_load_capacity+" MT to "+
                    vData?.vehicle_sub_category?.max_load_capacity+" MT)"*/


           /* holder.capacityTextView.text = Utils.setMTTextview(vData.TM_sub_category?.sub_category_name,
                vData?.TM_sub_category?.load_capacity,vData?.TM_sub_category?.load_capacity)*/

            holder.capacityTextView.text = vData.TM_category?.category_name

            holder.vehicle_number_txt.text = vData.TM_rc_number

            /* val string1 = vData.created_at
             val Date1 = ISO8601Utils.parse(string1, ParsePosition(0))
             val posFormat = SimpleDateFormat("dd MMM yyyy")
             val oldPosDate = posFormat.format(Date1)

             holder.date_txt.text = oldPosDate*/
            /* if(!vData?.is_active){
                 holder.status_txt.text = "Not Avilable"
                 holder.status_txt.setTextColor(ContextCompat.getColor(context, R.color.vehicle_status_not_avible))
             }else{
                 holder.status_txt.text = "Avilable"
                 holder.status_txt.setTextColor(ContextCompat.getColor(context, R.color.rounded_green_button))
             }
 */

            holder.lnly.setOnClickListener {
                mListener?.onVehicleListRowClick(vData)
            }


        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


        val view = LayoutInflater.from(parent.context).inflate(R.layout.vehicle_list_row, parent, false)
        return LikesViewHolder(view)
    }

    override fun getItemViewType(position: Int): Int {


        return LOAD_TYPE
    }

    override fun getItemCount(): Int {
        return vehicleList.size
    }



    internal inner class LikesViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var lnly: CardView
        var vehicleImg: ImageView

        var truck_range_txt: TextView
        var capacityTextView: TextView



        var vehicle_number_txt: TextView
        var status_txt: TextView


        init {
            lnly = view.findViewById<CardView>(R.id.lnly)
            vehicleImg = view.findViewById<ImageView>(R.id.vehicleImg)
            truck_range_txt = view.findViewById(R.id.truck_range_txt)
            capacityTextView = view.findViewById(R.id.capacityTextView)
            vehicle_number_txt = view.findViewById(R.id.vehicle_number_txt)
            status_txt = view.findViewById(R.id.status_txt)
        }
    }


    interface OnLoadMoreListener {

        fun onLoadMore()
    }

    fun setOnLoadMoreListener(mOnLoadMoreListener: OnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener
    }

    fun setLoaded() {
        isLoading = false
    }

    fun setListener(listener: vehiclelistInterface) {
        mListener = listener
    }

    interface vehiclelistInterface {

        fun onVehicleListRowClick(user: TMListObjRes)
    }
}