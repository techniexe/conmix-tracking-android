package com.conmixtracking.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.conmixtracking.R
import com.conmixtracking.data.CPListObjRes
import com.conmixtracking.utils.Utils


/**
 * Created by Hitesh Patel on 27,July,2021(vData.concrete_pump_capacity?:0.0).toString() + " Cu.Mtr"
 */
class CPListAdapter(val context: Context, val cpList: ArrayList<CPListObjRes>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val ROW_TYPE = 1

    var mListener: cplistInterface? = null

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is LikesViewHolder) {

            val vData = cpList[position]

            Utils.setImageUsingGlide(context, vData.concrete_pump_image_url ?: "", holder.vehicleImg)
            holder.truck_range_txt.text = vData.concrete_pump_category?.category_name
            val str = vData?.company_name+" - "+vData?.concrete_pump_model
            holder.capacityTextView.text = str
            holder.vehicle_number_txt.text = (vData.concrete_pump_capacity?:0.0).toInt().toString() + " Cu.Mtr"

            holder.lnly.setOnClickListener {
                mListener?.onCPListRowClick(vData)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


        val view = LayoutInflater.from(parent.context).inflate(R.layout.vehicle_list_row, parent, false)
        return LikesViewHolder(view)
    }

    override fun getItemViewType(position: Int): Int {


        return ROW_TYPE
    }

    override fun getItemCount(): Int {
        return cpList.size
    }



    internal inner class LikesViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var lnly: CardView
        var vehicleImg: ImageView
        var truck_range_txt: TextView
        var capacityTextView: TextView
        var vehicle_number_txt: TextView
        var status_txt: TextView


        init {
            lnly = view.findViewById<CardView>(R.id.lnly)
            vehicleImg = view.findViewById<ImageView>(R.id.vehicleImg)
            truck_range_txt = view.findViewById(R.id.truck_range_txt)
            capacityTextView = view.findViewById(R.id.capacityTextView)
            vehicle_number_txt = view.findViewById(R.id.vehicle_number_txt)
            status_txt = view.findViewById(R.id.status_txt)
        }
    }




    fun setListener(listener: cplistInterface) {
        mListener = listener
    }

    interface cplistInterface {

        fun onCPListRowClick(user: CPListObjRes)
    }
}