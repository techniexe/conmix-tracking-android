package com.conmixtracking.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


/**
 * Created by Hitesh Patel on 06,May,2021
 */


@Parcelize
data class OrderDeatilRes(val order: OrderDetailObj) : Parcelable

@Parcelize
data class OrderDeatilListRes(val order: ArrayList<OrderDetailObj>) : Parcelable

@Parcelize
data class OrderDetailObj(
    val _id: String?,
    var quantity: Double?,
    var delivered_quantity:Double?,
    var pickup_quantity:Double?,
    var remaining_quantity:Double?,
    var event_status:String?,
    var pickedup_at:String?,
    var created_at: String?,
    var challan_image_url:String?,
    var royalty_pass_image_url:String?,
    var way_slip_image_url:String?,
    var supplier_pickup_signature:String?,
    var driver_pickup_signature:String?,
    var buyer_drop_signature:String?,
    var driver_drop_signature:String?,
    var TM_info: TMInfoObj?,
    var driver1_info:TMDriver1Info?,
    var TM_category_info: TMCategoryInfoObj?,
    var TM_sub_category_info: TMSubCatInfoObj?,
    var buyer_info: BuyerInfoObj?,
    var vendor_info: SupplierInfoObj?,
    var vendor_order_info:vendor_order_info_obj?,
    var pickup_address_info: PickUpAddressinfoObj?,
    var delivery_address_info: DeliveryAddressinfoObj?,
    var order_info:order_Info_obj?,
    var order_item_info:order_item_info_obj?,
    var royality_quantity:Double?=0.0,
    var invoice_image_url:String?,
    var CP_info:CPInfoObj?,
    var CP_category_info:CPCatInfoObj?,
    var supplier_info:SupplierInfoObj?,
    var operator_info:operatorInfoObj?,
    var invoice_url:String?

) : Parcelable


@Parcelize
data class operatorInfoObj(var operator_name:String?,var operator_mobile_number:String?):Parcelable

@Parcelize
data class CPInfoObj(var _id:String?,var concrete_pump_category_id:String?,var company_name:String?,var concrete_pump_model:String?,
                     var manufacture_year:String?,var pipe_connection:String?,var concrete_pump_capacity:String?,var is_Operator:Boolean?,
                     var is_helper:Boolean?,var transportation_charge:Double?,var concrete_pump_price:Double?,var concrete_pump_image_url:String?):Parcelable

@Parcelize
data class CPCatInfoObj(var category_name:String?,var is_active:Boolean?):Parcelable

@Parcelize
data class TMInfoObj(
    val _id: String?,
    val TM_rc_number: String?,
    val TM_category_id: String?,
    val TM_sub_category_id: String?,
    val TM_image_url: String?
) : Parcelable

@Parcelize
data class TMDriver1Info(
    val driver_name:String?,
    val driver_mobile_number:String?
):Parcelable

@Parcelize
data class TMCategoryInfoObj(
    val _id: String?,
    val category_name: String?
) : Parcelable

@Parcelize
data class TMSubCatInfoObj(
    val _id: String?,
    val sub_category_name: String?,
    val load_capacity: Double?
) : Parcelable



@Parcelize
data class BuyerInfoObj(val _id: String?,
                        val full_name: String?,
                        val mobile_number: String?,var company_name:String?) :Parcelable


@Parcelize
data class SupplierInfoObj(val _id: String?,
                           val full_name: String?,
                           val mobile_number: String?,
                           var company_name:String?) : Parcelable


@Parcelize
data class vendor_order_info_obj(
    var _id:String?,
    var quantity:Double?,
    var created_at:String?):Parcelable

@Parcelize
data class order_item_info_obj(var _id:String?):Parcelable

@Parcelize
data class order_Info_obj(var _id:String?):Parcelable


@Parcelize
data class PickUpAddressinfoObj(
    val _id: String?,
    val line1: String?,
    val line2: String?,
    val pincode: Int?,
    val city_name: String?,
    val state_name: String?,
    val pickup_location: pickupLocationRes?
) : Parcelable


@Parcelize
data class DeliveryAddressinfoObj(
    val _id: String?,
    val line1: String?,
    val line2: String?,
    val pincode: Int?,
    val city_name: String?,
    val state_name: String?,
    val delivery_location: pickupLocationRes?
) : Parcelable

@Parcelize
data class UpdateQtyOBJ(var pickup_quantity:Double?,var royality_quantity:Double?,var order_status:String?,var reject_reason:String?):Parcelable

@Parcelize
data class DelayReasonObj(var reasonForDelay:String?,var delayTime:String?):Parcelable


@Parcelize
data class UploadTmInvoiceObj(var track_id:String?,var order_id:String?,var TM_id:String?):Parcelable