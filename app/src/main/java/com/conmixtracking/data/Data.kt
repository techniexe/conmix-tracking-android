package com.conmixtracking.data


/**
 * Created by Hitesh Patel on 06,May,2021
 */
data class Data<out T>(val data: T)
