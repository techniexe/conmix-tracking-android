package com.conmixtracking.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


/**
 * Created by Hitesh Patel on 06,May,2021
 */
@Parcelize
data class LoginReqModel(val password: String) : Parcelable

@Parcelize
data class LoginResponse(val customToken: String) : Parcelable


@Parcelize
data class ErrorRes(val error: ErrorModel) : Parcelable

@Parcelize
data class ErrorModel(val message: String, val email_exists: Boolean, val full_name: String, val facebook_id: String) : Parcelable