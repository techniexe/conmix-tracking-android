package com.conmixtracking.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


/**
 * Created by Hitesh Patel on 06,May,2021
 */
@Parcelize
data class SessionResponse(val token: String) : Parcelable
