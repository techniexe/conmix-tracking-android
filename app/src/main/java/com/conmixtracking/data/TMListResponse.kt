package com.conmixtracking.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


/**
 * Created by Hitesh Patel on 06,May,2021
 */
@Parcelize
data class TMListResponse(val data: ArrayList<TMListObjRes>):Parcelable

@Parcelize
data class SpecificTMRes(val data: TMListObjRes):Parcelable

@Parcelize
data class TMListObjRes(
    val _id: String,
    val TM_category_id: String?,
    val TM_sub_category_id: String?,
    val TM_rc_number: String?,
    val per_Cu_mtr_km: Int?,
    val pickup_location: pickupLocationRes?,
    val line1: String?,
    val line2: String?,
    val pincode: Int? = 0,
    val city_name: String?,
    val state_name: String?,
    val delivery_range: Int?,
    val min_trip_price: Int?,
    val manufacturer_name: String?,
    val manufacture_year: Int?,
    val TM_model: String?,
    val is_active: Boolean?,
    val is_gps_enabled: Boolean?,
    val is_insurance_active: Boolean?,
    val rc_book_image_url: String?,
    val insurance_image_url: String?,
    val TM_image_url: String?,
    val user_id: String?,
    val TM_category: tmCategoryRes?,
    val TM_sub_category: tmSubCategoryRes?,
    val created_at: String?,
    val driver1_info: Driver1_info?,
    val driver2_info: Driver2_info?,
    val state_id:String?,
    val calculated_trip_price:Double?

) : Parcelable

@Parcelize
data class pickupLocationRes(val type: String?, val coordinates: Array<Double>?) : Parcelable


@Parcelize
data class tmCategoryRes(val category_name: String?) : Parcelable

@Parcelize
data class tmSubCategoryRes(
    val sub_category_name: String?,
    val load_capacity: String?

) : Parcelable

@Parcelize
data class Driver1_info(val driver_name: String?, val driver_mobile_number: String?) : Parcelable


@Parcelize
data class Driver2_info(val driver_name: String?, val driver_mobile_number: String?) : Parcelable


// CP Response

@Parcelize
data class  CPListResponse(val data: ArrayList<CPListObjRes>):Parcelable

@Parcelize
data class CPListObjRes(var _id:String?,
                        var company_name:String?,
                        var concrete_pump_model:String?,
                        var manufacture_year:Int?,
                        var pipe_connection:Double?,
                        var concrete_pump_capacity:Double?,
                        var is_Operator:Boolean?,
                        var operator_id:String?,
                        var is_helper:Boolean?,
                        var transportation_charge:Double?,
                        var concrete_pump_price:Double?,
                        var concrete_pump_category_id:String?,
                        var concrete_pump_image_url:String?,
                        var created_at:String?,
                        var concrete_pump_category:ConcreatePumpCatObj?,
                        var operator_info:CPOperatorInfoObj?,
                        var serial_number:String?):Parcelable


@Parcelize
data class ConcreatePumpCatObj(var is_active:Boolean?, var category_name:String?):Parcelable

@Parcelize
data class CPOperatorInfoObj(var _id:String?, var operator_name:String?, var operator_mobile_number:String?):Parcelable

