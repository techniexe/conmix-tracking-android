package com.conmixtracking.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


/**
 * Created by Hitesh Patel on 06,May,2021
 */
@Parcelize
data class SessionRequest(val accessToken: String, val registrationToken: String): Parcelable

