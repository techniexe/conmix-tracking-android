package com.conmixtracking

import android.app.Application
import android.content.Context
import android.os.StrictMode
import android.provider.Settings
import com.downloader.PRDownloader
import com.downloader.PRDownloaderConfig
import com.facebook.drawee.backends.pipeline.Fresco
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import java.util.logging.Logger.global


/**
 * Created by Hitesh Patel on 06,May,2021
 */
class ConmixTrackingApp: Application() {

    var globalVehicle:String? = null
    override fun onLowMemory() {
        super.onLowMemory()
        System.gc()
    }

    override fun onTerminate() {
        super.onTerminate()
    }

    override fun onTrimMemory(level: Int) {
        super.onTrimMemory(level)
    }

    override fun onCreate() {
        super.onCreate()
       // instance = this
        val config = ImageLoaderConfiguration.Builder(this)
            .build()
        ImageLoader.getInstance().init(config)



        Fresco.initialize(this)
        instance = this



        val configd = PRDownloaderConfig.newBuilder()
            .setDatabaseEnabled(true)
            .build()
        PRDownloader.initialize(this, configd)
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())

    }

    companion object {
        var instance: ConmixTrackingApp? = null
            private set
    }
}