package com.conmixtracking.ViewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData

import com.conmixtracking.Repository.SessionRepositary
import com.conmixtracking.data.SessionRequest
import com.conmixtracking.services.api.ApiInterface
import com.conmixtracking.services.api.Resource
import kotlinx.coroutines.Dispatchers


/**
 * Created by Hitesh Patel on 06,May,2021
 */
class SessionViewModel(private val sessionRepositary: SessionRepositary) : ViewModel() {

    fun getSession(sessionRequest: SessionRequest) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = sessionRepositary.getSessionToken(sessionRequest)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }
    class ViewModelFactory(private val apiInterface: ApiInterface) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(SessionViewModel::class.java)) {
                return SessionViewModel(SessionRepositary(apiInterface)) as T
            }
            throw IllegalArgumentException("Unknown class name")
        }

    }
}