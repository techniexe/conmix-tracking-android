package com.conmixtracking.ViewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.conmixtracking.Repository.LoginRepositary
import com.conmixtracking.data.LoginReqModel
import com.conmixtracking.services.api.ApiInterface
import com.conmixtracking.services.api.Resource
import kotlinx.coroutines.Dispatchers


/**
 * Created by Hitesh Patel on 06,May,2021
 */
class LoginViewModel (private val loginRepository: LoginRepositary) : ViewModel() {


    fun loginWithIdentifier(userName: String, loginReqModel: LoginReqModel)= liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = loginRepository.loginWithIdentifier(userName,loginReqModel)))
        }catch (exception:Exception){

        }
    }

    fun getOtpCode(mobilenumber: String?)= liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = loginRepository.getOtpCode(mobilenumber)))
        }catch (exception:Exception){

        }
    }


    fun postMobileCode(mobile_number: String?,code: String?)= liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = loginRepository.postMobileCode(mobile_number,code)))
        }catch (exception:Exception){

        }
    }

    class ViewModelFactory(private val apiInterface: ApiInterface) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {
                return LoginViewModel(LoginRepositary(apiInterface)) as T
            }
            throw IllegalArgumentException("Unknown class name")
        }

    }


}