package com.conmixtracking.ViewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.conmixtracking.Repository.CPListRepository
import com.conmixtracking.Repository.TMListRepository
import com.conmixtracking.services.api.ApiInterface
import com.conmixtracking.services.api.Resource
import kotlinx.coroutines.Dispatchers


/**
 * Created by Hitesh Patel on 27,July,2021
 */
class CPListViewModel(private val cpListRepository: CPListRepository) : ViewModel() {

    fun getCPListData() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(
                Resource.success(
                    data = cpListRepository.getCPListData()
                )
            )
        } catch (exception: Exception) {

        }
    }


    class ViewModelFactory(private val apiInterface: ApiInterface) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(CPListViewModel::class.java)) {
                return CPListViewModel(CPListRepository(apiInterface)) as T
            }
            throw IllegalArgumentException("Unknown class name")
        }

    }

}