package com.conmixtracking.ViewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.conmixtracking.Repository.TMListRepository
import com.conmixtracking.services.api.ApiInterface
import com.conmixtracking.services.api.Resource
import kotlinx.coroutines.Dispatchers


/**
 * Created by Hitesh Patel on 06,May,2021
 */
class TMListViewModel(private val tmListRepository: TMListRepository) : ViewModel() {


    fun getTMListData(
        is_active: Boolean?,
        TM_category_id: String?,
        TM_sub_category_id: String?,
        is_gps_enabled: Boolean?,
        is_insurance_active: Boolean?,
        before: String?,
        after: String?,
        limit_count: Int?
    ) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(
                Resource.success(
                    data = tmListRepository.getTMListData(
                        is_active,
                        TM_category_id,
                        TM_sub_category_id,
                        is_gps_enabled,
                        is_insurance_active,
                        before,
                        after,
                        limit_count
                    )
                )
            )
        } catch (exception: Exception) {

        }
    }

    fun getSpecificTMData(id: String?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(
                Resource.success(
                    data = tmListRepository.getSpecificTMData(
                        id
                    )
                )
            )
        } catch (exception: Exception) {

        }
    }

    class ViewModelFactory(private val apiInterface: ApiInterface) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(TMListViewModel::class.java)) {
                return TMListViewModel(TMListRepository(apiInterface)) as T
            }
            throw IllegalArgumentException("Unknown class name")
        }

    }


}