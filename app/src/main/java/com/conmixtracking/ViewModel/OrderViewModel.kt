package com.conmixtracking.ViewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.conmixtracking.Repository.OrderRepository
import com.conmixtracking.data.DelayReasonObj
import com.conmixtracking.data.UpdateQtyOBJ
import com.conmixtracking.data.UploadTmInvoiceObj
import com.conmixtracking.services.api.ApiInterface
import com.conmixtracking.services.api.Resource
import kotlinx.coroutines.Dispatchers
import okhttp3.MultipartBody


/**
 * Created by Hitesh Patel on 06,May,2021
 */
class OrderViewModel(private val orderRepository: OrderRepository) : ViewModel() {



    fun getOrderDetailListData(id: String?,current_date:String?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(
                Resource.success(
                    data = orderRepository.getOrderDetailListData(
                        id,current_date
                    )
                )
            )
        } catch (exception: Exception) {

        }
    }
    fun getOrderDetailListCPData(id: String?,current_date:String?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(
                Resource.success(
                    data = orderRepository.getOrderDetailCPListData(
                        id,current_date
                    )
                )
            )
        } catch (exception: Exception) {

        }
    }

    fun getOrdeIdDetailData(order_id:String?,track_id:String?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(
                Resource.success(
                    data = orderRepository.getOrdeIdDetailData(
                        order_id,track_id
                    )
                )
            )
        } catch (exception: Exception) {

        }
    }

    fun getOrdeIdCPDetailData(order_id:String?,track_id:String?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(
                Resource.success(
                    data = orderRepository.getOrdeIdCPDetailData(
                        order_id,track_id
                    )
                )
            )
        } catch (exception: Exception) {

        }
    }

    fun updateQtyData(track_id:String?,tmId:String?,order_id:String?,uQb: UpdateQtyOBJ?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(
                Resource.success( data = orderRepository.updateQtyData( track_id,tmId,order_id,uQb))
            )
        } catch (exception: Exception) {

        }
    }

    fun updateQtyStatusCPData(track_id:String?,cpId:String?,order_id:String?,uQb: UpdateQtyOBJ?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(
                Resource.success( data = orderRepository.updateQtyStatusCPData( track_id,cpId,order_id,uQb))
            )
        } catch (exception: Exception) {

        }
    }

    fun confirmPickupOrder(track_id:String?,tmId:String?,order_id:String?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(
                Resource.success( data = orderRepository.confirmPickupOrder( track_id,tmId,order_id))
            )
        } catch (exception: Exception) {

        }
    }

    fun confirmPickupCPOrder(track_id:String?,cpId:String?,order_id:String?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(
                Resource.success( data = orderRepository.confirmPickupCPOrder( track_id,cpId,order_id))
            )
        } catch (exception: Exception) {

        }
    }

    fun uploadDocumentOrder(track_id:String?, tmId:String?, order_id:String?, royalty_pass_image: MultipartBody.Part?,
                            way_slip_image: MultipartBody.Part?,
                            challan_image: MultipartBody.Part?,
                            invoice_image: MultipartBody.Part?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(
                Resource.success( data = orderRepository.uploadDocumentOrder( track_id, tmId, order_id,  royalty_pass_image,
                    way_slip_image,
                    challan_image,
                    invoice_image))
            )
        } catch (exception: Exception) {

        }
    }

    fun uploadDocumentCPOrder(track_id:String?, cpId:String?, order_id:String?, royalty_pass_image: MultipartBody.Part?,
                            way_slip_image: MultipartBody.Part?,
                            challan_image: MultipartBody.Part?,
                            invoice_image: MultipartBody.Part?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(
                Resource.success( data = orderRepository.uploadDocumentCPOrder( track_id, cpId, order_id, royalty_pass_image,
                    way_slip_image,
                    challan_image,
                    invoice_image))
            )
        } catch (exception: Exception) {

        }
    }

    fun uploadSignSupplierOrder(track_id:String?, tmId:String?, order_id:String?,supplier_pickup_signature: MultipartBody.Part?,
                                driver_pickup_signature: MultipartBody.Part?,
                                buyer_drop_signature: MultipartBody.Part?,
                                driver_drop_signature: MultipartBody.Part?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(
                Resource.success( data = orderRepository.uploadSignSupplierOrder( track_id, tmId, order_id,supplier_pickup_signature,driver_pickup_signature,buyer_drop_signature,driver_drop_signature))
            )
        } catch (exception: Exception) {

        }
    }

    fun uploadSignSupplierCPOrder(track_id:String?, cpId:String?, order_id:String?,supplier_pickup_signature: MultipartBody.Part?,
                                driver_pickup_signature: MultipartBody.Part?,
                                buyer_drop_signature: MultipartBody.Part?,
                                driver_drop_signature: MultipartBody.Part?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(
                Resource.success( data = orderRepository.uploadSignSupplierCPOrder( track_id, cpId, order_id,supplier_pickup_signature,driver_pickup_signature,buyer_drop_signature,driver_drop_signature))
            )
        } catch (exception: Exception) {

        }
    }

    fun postDelayReasonOrder(tmId:String?,orderId:String?,track_id:String?,dROBJ: DelayReasonObj?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(
                Resource.success( data = orderRepository.postDelayReasonOrder( tmId,orderId,track_id,dROBJ))
            )
        } catch (exception: Exception) {

        }
    }

    fun postDelayCPReasonOrder(cpId:String?,orderId:String?,track_id:String?,dROBJ: DelayReasonObj?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(
                Resource.success( data = orderRepository.postDelayReasonCPOrder( cpId,orderId,track_id,dROBJ))
            )
        } catch (exception: Exception) {

        }
    }

    fun deliverSuceessCall(tmId: String?,
                           orderId: String?,
                           track_id: String?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(
                Resource.success( data = orderRepository.deliverSuceessCall( tmId,orderId,track_id))
            )
        } catch (exception: Exception) {

        }
    }
    fun deliverSuceessCPCall(cpId: String?,
                           orderId: String?,
                           track_id: String?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(
                Resource.success( data = orderRepository.deliverSuceessCPCall( cpId,orderId,track_id))
            )
        } catch (exception: Exception) {

        }
    }

    fun getCPDeliverCheck(orderId: String?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(
                Resource.success( data = orderRepository.getCPDeliverCheck(orderId))
            )
        } catch (exception: Exception) {

        }
    }

    fun uploadTmInvoice(uploadTmInvoiceObj: UploadTmInvoiceObj?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(
                Resource.success( data = orderRepository.uploadTmInvoice(uploadTmInvoiceObj))
            )
        } catch (exception: Exception) {

        }
    }

    fun uploadVendorInvoice(uploadTmInvoiceObj: UploadTmInvoiceObj?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(
                Resource.success( data = orderRepository.uploadVendorInvoice(uploadTmInvoiceObj))
            )
        } catch (exception: Exception) {

        }
    }



    class ViewModelFactory(private val apiInterface: ApiInterface) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(OrderViewModel::class.java)) {
                return OrderViewModel(OrderRepository(apiInterface)) as T
            }
            throw IllegalArgumentException("Unknown class name")
        }

    }


}