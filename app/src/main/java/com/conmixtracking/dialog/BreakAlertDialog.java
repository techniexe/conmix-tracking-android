package com.conmixtracking.dialog;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.conmixtracking.R;

/**
 * Created by Hitesh Patel on 07,May,2021
 */
public class BreakAlertDialog extends DialogFragment {
    ClickListener mListener;
    private String mTitle;
    private String mMsg, mPostiveText, mNegativeText, mUserImage;

    @SuppressLint("ValidFragment")
    public BreakAlertDialog() {

    }

    @SuppressLint("ValidFragment")
    public BreakAlertDialog(ClickListener dialogListener, String title, String positiveBtn, String negativeBtn) {

        mListener = dialogListener;
        mTitle = title;
        mPostiveText = positiveBtn;
        mNegativeText = negativeBtn;

    }

    public void setDialogListener(ClickListener dialogListener) {
        mListener = dialogListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, 0);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.break_dialog, container, false);
        TextView title = (TextView) v.findViewById(R.id.titleAlertDialog);
        Button doneButton = (Button) v.findViewById(R.id.postiveBtnAlertDialog);
        Button cancelButton = (Button) v.findViewById(R.id.neagtiveBtnAlertDialog);


        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onDoneClicked();
                dismiss();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onCancelClicked();
                dismiss();
            }
        });

        title.setText(mTitle);
        doneButton.setText(mPostiveText);
        cancelButton.setText(mNegativeText);

        if (mNegativeText == null || mNegativeText.isEmpty()) {
            cancelButton.setVisibility(View.GONE);
        }

        getDialog().getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        return v;
    }


    public interface ClickListener {
        void onDoneClicked();
        void onCancelClicked();
    }
}