package com.conmixtracking.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.fragment.app.DialogFragment
import com.conmixtracking.R
import com.conmixtracking.utils.SharedPrefUtils


/**
 * Created by Hitesh Patel on 07,May,2021
 */
class SelectLanguageDialog : DialogFragment(), CompoundButton.OnCheckedChangeListener {
    private var mListner: DialogToFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, 0)

    }

    interface DialogToFragment {
        fun sortDialogDismiss()
        fun sortDialogSave(sortType: String)
    }

    fun setListener(mListener: DialogToFragment) {
        this.mListner = mListener


    }

    override fun onResume() {
        super.onResume()
        val window = dialog!!.window
        window?.setGravity(Gravity.CENTER)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        dialog!!.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCanceledOnTouchOutside(true)
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        mListner!!.sortDialogDismiss()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)
        val inflater = requireActivity().layoutInflater
        val view = inflater.inflate(R.layout.selectlanguage_dailog, null)
        builder.setView(view)
        val hindi_radio = view.findViewById<RadioButton>(R.id.hindi_radio) as RadioButton
        val english_radio = view.findViewById<RadioButton>(R.id.english_radio) as RadioButton
        val imgClose = view.findViewById<ImageView>(R.id.imgClose) as ImageView
        val btnSubmit = view.findViewById<Button>(R.id.btnApply) as Button
        val radioGroupbtn = view.findViewById<RadioGroup>(R.id.radioGroup) as RadioGroup


        val lan = SharedPrefUtils.getInstance(requireContext()).getStringFromSharedPref(SharedPrefUtils.LANGUAGE_KEY)

        if(lan == "hi"){
           hindi_radio.isChecked = true
        }else{
            english_radio.isChecked = true
        }


        english_radio.setOnCheckedChangeListener(this);
        hindi_radio.setOnCheckedChangeListener(this);


        imgClose.setOnClickListener {
            dismiss()
            mListner!!.sortDialogDismiss()
        }

        btnSubmit.setOnClickListener {
            // mListner!!.sortDialogSave(selectLan)

            val rb: RadioButton =  radioGroupbtn.findViewById(radioGroupbtn.getCheckedRadioButtonId()) as RadioButton
            mListner!!.sortDialogSave(rb.text.toString())
            dismiss()
        }



        return builder.create()
    }

    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
        when (buttonView?.id) {
            R.id.english_radio -> {


            }
            R.id.hindi_radio -> {


            }

        }

    }
}